(in-package :tz-time.jni)

(defstruct (tz-time::timezone (:print-object print-timezone))
  ref)

(defun tz-time::timezone-name (timezone)
  (cl+j:jtol (#]toString (timezone-ref timezone))))

(defun tz-time::gettz (name)
  (make-timezone :ref (cl+j::jmethod "java.time.ZoneId" "of" (cl+j:ltoj name))))

(defun tz-time::tz-timestamp-timezone-name (tz-timestamp)
  (cl+j:jtol
   (#]toString
    (#]getZone (tz-timestamp-ref tz-timestamp)))))

(defun print-timezone (timezone stream)
  (format stream "<TZ ~A>" (tz-time::timezone-name timezone)))

(defparameter tz-time::*timezone* :utc)

(defstruct (tz-time::tz-timestamp (:print-object print-tz-timestamp))
  ref)

(defun print-tz-timestamp (tz-timestamp stream)
  (write-string "<TZ-TIMESTAMP " stream)
  (tz-time::format-timestring tz-timestamp stream)
  (write-string ">" stream))

(defun tz-time::now (&key (tz tz-time::*timezone*))
  (let ((timezone (if (keywordp tz)
                      (ecase tz
                        (:utc (tz-time::gettz "UTC")))
                      (progn (check-type tz tz-time::timezone)
                             tz))))
    (make-tz-timestamp :ref
                       (cl+j:jmethod "java.time.ZonedDateTime" "now"
                                   (timezone-ref timezone)))))

(defun get-formatter (format)
  (or (and (not (keywordp format))
           format)
      (ecase format
        (:basic-iso-date #?java.time.format.DateTimeFormatter.BASIC_ISO_DATE)
        (:iso-date #?java.time.format.DateTimeFormatter.ISO_DATE)
        (:iso-date-time #?java.time.format.DateTimeFormatter.ISO_DATE_TIME)
        (:iso-instant #?java.time.format.DateTimeFormatter.ISO_INSTANT)
        (:iso-local-date #?java.time.format.DateTimeFormatter.ISO_LOCAL_DATE)
        (:iso-local-date-time #?java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        (:iso-local-time #?java.time.format.DateTimeFormatter.ISO_LOCAL_TIME)
        (:iso-offset-date #?java.time.format.DateTimeFormatter.ISO_OFFSET_DATE)
        (:iso-offset-date-time #?java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        (:iso-offset-time #?java.time.format.DateTimeFormatter.ISO_OFFSET_TIME)
        (:iso-ordinal-date #?java.time.format.DateTimeFormatter.ISO_ORDINAL_DATE)
        (:iso-time #?java.time.format.DateTimeFormatter.ISO_TIME)
        (:iso-week-date #?java.time.format.DateTimeFormatter.ISO_WEEK_DATE)
        (:iso-zoned-date-time #?java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME)
        (:rfc-1123-date-time #?java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME))))

(defun tz-time::format-timestring (timestamp &optional destination &key format)
  (if (not destination)
      (with-output-to-string (stream)
        (if (not format)
            (princ (cl+j:jtol (#]toString (tz-timestamp-ref timestamp))) stream)
            (let ((formatter (get-formatter format)))
              (princ
               (cl+j:jtol
                (#]format (tz-timestamp-ref timestamp)
                          formatter))
               stream))))
      ; else
      (if (not format)
            (princ (cl+j:jtol (#]toString (tz-timestamp-ref timestamp))) destination)
            (let ((formatter (get-formatter format)))
              (princ
               (cl+j:jtol
                (#]format (tz-timestamp-ref timestamp)
                          formatter))
               destination)))))      

(defun tz-time::decode-tz-timestamp (timestamp)
  "Returns the decoded time as multiple values: nsec, ss, mm, hh, day, month, year, day-of-week, timezone"
  (check-type timestamp tz-time::tz-timestamp)
  (with-slots (ref) timestamp
	(values (cl+j:jtol (#]getNano ref))
			(cl+j:jtol (#]getSecond ref))
			(cl+j:jtol (#]getMinute ref))
			(cl+j:jtol (#]getHour ref))
            (cl+j:jtol (#]getDayOfMonth ref))
			(cl+j:jtol (#]getValue (#]getMonth ref)))
			(cl+j:jtol (#]getYear ref))
			(cl+j:jtol (#]getValue (#]getDayOfWeek ref)))
			(make-timezone :ref (#]getZone ref)))))

(defun tz-time::timestamp-timezone (timestamp)
  (check-type timestamp tz-time::tz-timestamp)
  (with-slots (ref) timestamp
    (make-timezone :ref (#]getZone ref))))

(defun tz-time::encode-tz-timestamp (year month dayOfMonth hour minute second nanoOfSecond tz)
  (make-tz-timestamp :ref
					 (cl+j:jmethod "java.time.ZonedDateTime" "of"
                                   (cl+j:jprim-short year)
                                   (cl+j:jprim-short month)
                                   (cl+j:jprim-short dayofmonth)
                                   (cl+j:jprim-short hour)
                                   (cl+j:jprim-short minute)
                                   (cl+j:jprim-short second)
                                   ;; We are not interested in nano precission. We pass zero instead
                                   ;(cl+j:jprim-short nanoofsecond)
                                   (cl+j:jprim-short 0)
                                   (timezone-ref tz))))

(defun tz-time::tz-timestamp-difference (t1 t2)
  (#]getSeconds
   (cl+j:jmethod "java.time.Duration"
                 "between"
                 (tz-timestamp-ref t1)
                 (tz-timestamp-ref t2))))

(defun tz-time::tz-timestamp-format-localized (timestamp locale)
  "Example: (tz-time::tz-timestamp-format-localized (now) \"fr\")"
  
  (let ((formatter (cl+j:jmethod "java.time.format.DateTimeFormatter"
                                 "ofLocalizedDateTime"
                                 #?java.time.format.FormatStyle.FULL)))
	(let ((localized-formatter
           (#]withLocale formatter
                         (cl+j:jnew "java.util.Locale" (cl+j:ltoj locale)))))
	  (cl+j:jtol (#]format (tz-timestamp-ref timestamp)
                           localized-formatter)))))

(defun tz-time::tz-timestamp-to-timezone (timestamp tz)
  (make-tz-timestamp :ref
					 (#]withZoneSameInstant 
								 (tz-timestamp-ref timestamp)
								 (timezone-ref tz))))

;; (foil::define-java-condition tz-time::datetime-error (foil::java-error)
;;   ()
;;   (:java-exception "java.time.DateTimeException"))

;; (foil::define-java-condition tz-time::datetime-parse-error (tz-time::datetime-error)
;; 	()
;;   (:java-exception "java.time.format.DateTimeParseException"))

(defun tz-time::parse-tz-timestring (string &optional (error-p t) format)
  (flet ((parse-timestring ()
           (if format
               (let ((formatter (get-formatter format)))
                 (make-tz-timestamp :ref
                                    (cl+j:jmethod "java.time.ZonedDateTime"
                                                  "parse"
                                                  (cl+j:ltoj string)
                                                  formatter)))
               (make-tz-timestamp :ref
                                  (cl+j:jmethod "java.time.ZonedDateTime"
                                                "parse"
                                                (cl+j:ltoj string))))))	
    (if error-p
        (parse-timestring)
                                        ; else
        (handler-case
            (parse-timestring)
          (tz-time::datetime-parse-error ()
            (return-from tz-time::parse-tz-timestring nil))))))

(defun tz-time::plus-nanos (tz-time nanos)
  (make-tz-timestamp :ref
                     (#]plusNanos (tz-timestamp-ref tz-time)
                                  (cl+j:jprim-long nanos))))

(defun tz-time::plus-seconds (tz-time seconds)
  (make-tz-timestamp :ref
                     (#]plusSeconds (tz-timestamp-ref tz-time)
                                    (cl+j:jprim-short seconds))))

(defun tz-time::plus-minutes (tz-time minutes)
  (make-tz-timestamp :ref
                     (#]plusMinutes (tz-timestamp-ref tz-time)
                                    (cl+j:jprim-short minutes))))

(defun tz-time::plus-days (tz-time days)
  (make-tz-timestamp :ref
                     (#]plusDays (tz-timestamp-ref tz-time)
                                 (cl+j:jprim-short days))))

(defun tz-time::plus-weeks (tz-time weeks)
  (make-tz-timestamp :ref
                     (#]plusWeeks (tz-timestamp-ref tz-time)
                                  (cl+j:jprim-short weeks))))

(defun tz-time::plus-months (tz-time months)
  (make-tz-timestamp :ref
                     (#]plusMonths (tz-timestamp-ref tz-time)
                                   (cl+j:jprim-short months))))

(defun tz-time::plus-years (tz-time years)
  (make-tz-timestamp :ref
                     (#]plusYears (tz-timestamp-ref tz-time)
                                  (cl+j:jprim-short years))))

(defun tz-time::minus-nanos (tz-time nanos)
  (make-tz-timestamp :ref
                     (#]minusNanos (tz-timestamp-ref tz-time)
                                  (cl+j:jprim-long nanos))))

(defun tz-time::minus-seconds (tz-time seconds)
  (make-tz-timestamp :ref
                     (#]minusSeconds (tz-timestamp-ref tz-time)
                                    (cl+j:jprim-short seconds))))

(defun tz-time::minus-minutes (tz-time minutes)
  (make-tz-timestamp :ref
                     (#]minusMinutes (tz-timestamp-ref tz-time)
                                    (cl+j:jprim-short minutes))))

(defun tz-time::minus-days (tz-time days)
  (make-tz-timestamp :ref
                     (#]minusDays (tz-timestamp-ref tz-time)
                                 (cl+j:jprim-short days))))

(defun tz-time::minus-weeks (tz-time weeks)
  (make-tz-timestamp :ref
                     (#]minusWeeks (tz-timestamp-ref tz-time)
                                  (cl+j:jprim-short weeks))))

(defun tz-time::minus-months (tz-time months)
  (make-tz-timestamp :ref
                     (#]minusMonths (tz-timestamp-ref tz-time)
                                   (cl+j:jprim-short months))))

(defun tz-time::minus-years (tz-time years)
  (make-tz-timestamp :ref
                     (#]minusYears (tz-timestamp-ref tz-time)
                                  (cl+j:jprim-short years))))

;; Comparison

(defun tz-time::is-before (t1 t2)
  (eql
   (#]isBefore
    (tz-timestamp-ref t1)
    (tz-timestamp-ref t2))
   1))

(defun tz-time::is-after (t1 t2)
  (eql
   (#]isAfter
    (tz-timestamp-ref t1)
    (tz-timestamp-ref t2))
   1))

(defun tz-time::is-equal (t1 t2)
  (eql
   (#]isEqual
    (tz-timestamp-ref t1)
    (tz-timestamp-ref t2))
   1))
