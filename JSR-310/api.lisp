(in-package :tz-time.jsr310)

(defstruct (tz-time::timezone (:print-object print-timezone))
  ref)

(defun tz-time::timezone-name (timezone)
  (foil:to-string (timezone-ref timezone)))

(defun tz-time::gettz (name)
  (make-timezone :ref (|java.time|::zoneid.of name)))

(defun tz-time::tz-timestamp-timezone-name (tz-timestamp)
  (foil:to-string
   (|java.time|:zoneddatetime.get-zone (tz-timestamp-ref tz-timestamp))))

(defun print-timezone (timezone stream)
  (format stream "<TZ ~A>" (tz-time::timezone-name timezone)))

(defparameter tz-time::*timezone* :utc)

(defstruct (tz-time::tz-timestamp (:print-object print-tz-timestamp))
  ref)

(defun print-tz-timestamp (tz-timestamp stream)
  (write-string "<TZ-TIMESTAMP " stream)
  (tz-time::format-timestring tz-timestamp stream)
  (write-string ">" stream))

(defun tz-time::now (&key (tz tz-time::*timezone*))
  (let ((timezone (if (keywordp tz)
                      (ecase tz
                        (:utc (tz-time::gettz "UTC")))
                      (progn (check-type tz tz-time::timezone)
                             tz))))
    (make-tz-timestamp :ref
                       (|java.time|::zoneddatetime.now
                                   (timezone-ref timezone)))))

(defun get-formatter (format)
  (or (and (not (keywordp format))
           (|java.time.format|:datetimeformatter.of-pattern format))
      (ecase format
        (:basic-iso-date (|java.time.format|:datetimeformatter.+basic_iso_date+))
        (:iso-date (|java.time.format|:datetimeformatter.+iso_date+))
        (:iso-date-time (|java.time.format|:datetimeformatter.+iso_date_time+))
        (:iso-instant (|java.time.format|:datetimeformatter.+iso_instant+))
        (:iso-local-date (|java.time.format|:datetimeformatter.+iso_local_date+))
        (:iso-local-date-time (|java.time.format|:datetimeformatter.+iso_local_date_time+))
        (:iso-local-time (|java.time.format|:datetimeformatter.+iso_local_time+))
        (:iso-offset-date (|java.time.format|:datetimeformatter.+iso_offset_date+))
        (:iso-offset-date-time (|java.time.format|:datetimeformatter.+iso_offset_date_time+))
        (:iso-offset-time (|java.time.format|:datetimeformatter.+iso_offset_time+))
        (:iso-ordinal-date (|java.time.format|:datetimeformatter.+iso_ordinal_date+))
        (:iso-time (|java.time.format|:datetimeformatter.+iso_time+))
        (:iso-week-date (|java.time.format|:datetimeformatter.+iso_week_date+))
        (:iso-zoned-date-time (|java.time.format|:datetimeformatter.+iso_zoned_date_time+))
        (:rfc-1123-date-time (|java.time.format|:datetimeformatter.+rfc_1123_date_time+)))))

(defun tz-time::format-timestring (timestamp &optional stream &key format)
  (if (not format)
	  (princ (foil:to-string (tz-timestamp-ref timestamp)) stream)
	  (let ((formatter (get-formatter format)))
		(princ
		 (|java.time|:zoneddatetime.format (tz-timestamp-ref timestamp)
					 formatter) stream))))

(defun tz-time::decode-tz-timestamp (timestamp)
  "Returns the decoded time as multiple values: nsec, ss, mm, hh, day, month, year, day-of-week, timezone"
  (check-type timestamp tz-time::tz-timestamp)
  (with-slots (ref) timestamp
	(values (|java.time|::zoneddatetime.nano-prop ref)
			(|java.time|::zoneddatetime.second-prop ref)
			(|java.time|::zoneddatetime.minute-prop ref)
			(|java.time|::zoneddatetime.hour-prop ref)
            (|java.time|::zoneddatetime.day-of-month-prop ref)
			(|java.time|::zoneddatetime.month-value-prop ref)
			(|java.time|::zoneddatetime.year-prop ref)
			(|java.time|::dayofweek.get-value
						(|java.time|::zoneddatetime.day-of-week-prop ref))
			(make-timezone :ref (|java.time|::zoneddatetime.zone-prop ref)))))

(defun tz-time::second-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::zoneddatetime.second-prop ref)))

(defun tz-time::minute-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::zoneddatetime.minute-prop ref)))

(defun tz-time::hour-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::zoneddatetime.hour-prop ref)))

(defun tz-time::day-of-month-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::zoneddatetime.day-of-month-prop ref)))

(defun tz-time::month-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::zoneddatetime.month-value-prop ref)))

(defun tz-time::year-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::zoneddatetime.year-prop ref)))

(defun tz-time::day-of-week-of (timestamp)
  (with-slots (ref) timestamp
    (|java.time|::dayofweek.get-value
                (|java.time|::zoneddatetime.day-of-week-prop ref))))

(defun tz-time::timestamp-timezone (timestamp)
  (check-type timestamp tz-time::tz-timestamp)
  (with-slots (ref) timestamp
    (make-timezone :ref (|java.time|::zoneddatetime.zone-prop ref))))

(defun tz-time::encode-tz-timestamp (year month dayOfMonth hour minute second nanoOfSecond tz)
  (make-tz-timestamp :ref
					 (|java.time|::zoneddatetime.of year month dayofmonth
								 hour minute second nanoofsecond
								 (timezone-ref tz))))

(defun tz-time::tz-timestamp-difference (t1 t2)
  (|java.time|::duration.get-seconds
			  (|java.time|::duration.between
						  (tz-timestamp-ref t1)
						  (tz-timestamp-ref t2))))

(defun tz-time::tz-timestamp-format-localized (timestamp locale &optional (format :full))
  "Example: (tz-time::tz-timestamp-format-localized (now) \"fr\")"

  (let ((formatter (|java.time.format|::datetimeformatter.of-localized-date-time
                                      (ecase format
                                        (:full (|java.time.format|::formatstyle.+full+))
                                        (:long (|java.time.format|::formatstyle.+long+))
                                        (:medium (|java.time.format|::formatstyle.+medium+))
                                        (:short (|java.time.format|::formatstyle.+short+))))))
	(let ((localized-formatter (|java.time.format|::datetimeformatter.with-locale
												  formatter
												  (|java.util|:locale.new locale))))
	  (|java.time|::zoneddatetime.format
				  (tz-timestamp-ref timestamp)
				  localized-formatter))))

(defun tz-time::tz-timestamp-to-timezone (timestamp tz)
  (make-tz-timestamp :ref
					 (|java.time|::zoneddatetime.with-zone-same-instant
								 (tz-timestamp-ref timestamp)
								 (timezone-ref tz))))

(foil::define-java-condition tz-time::datetime-error (foil::java-error)
  ()
  (:java-exception "java.time.DateTimeException"))

(foil::define-java-condition tz-time::datetime-parse-error (tz-time::datetime-error)
	()
  (:java-exception "java.time.format.DateTimeParseException"))

(defun tz-time::parse-tz-timestring (string &optional (error-p t) format)
  (flet ((parse-timestring ()
           (if format
               (let ((formatter (get-formatter format)))
                 (make-tz-timestamp :ref
                                    (|java.time|::zoneddatetime.parse string formatter)))
               (make-tz-timestamp :ref
                                  (|java.time|::zoneddatetime.parse string)))))
                 
		
    (if error-p
        (parse-timestring)
                                        ; else
        (handler-case
            (parse-timestring)
          (tz-time::datetime-parse-error ()
            (return-from tz-time::parse-tz-timestring nil))))))

(defmacro def-diff-operation (operation)
  `(defun ,(intern (string operation) :tz-time)
	   (tz-time x)
	 (make-tz-timestamp :ref
						(,(intern (format nil "ZONEDDATETIME.~A" operation) :|java.time|)
						  (tz-timestamp-ref tz-time)
						  (foil:box :long x)))))

(def-diff-operation plus-nanos)
(def-diff-operation plus-seconds)
(def-diff-operation plus-minutes)
(def-diff-operation plus-days)
(def-diff-operation plus-weeks)
(def-diff-operation plus-months)
(def-diff-operation plus-years)
(def-diff-operation minus-nanos)
(def-diff-operation minus-seconds)
(def-diff-operation minus-minutes)
(def-diff-operation minus-days)
(def-diff-operation minus-weeks)
(def-diff-operation minus-months)
(def-diff-operation minus-years)

;; Comparison

(defun tz-time::is-before (t1 t2)
  (|java.time|::zoneddatetime.is-before
              (tz-timestamp-ref t1)
              (tz-timestamp-ref t2)))

(defun tz-time::is-after (t1 t2)
  (|java.time|::zoneddatetime.is-after
              (tz-timestamp-ref t1)
              (tz-timestamp-ref t2)))

(defun tz-time::is-equal (t1 t2)
  (|java.time|::zoneddatetime.is-equal
              (tz-timestamp-ref t1)
              (tz-timestamp-ref t2)))
