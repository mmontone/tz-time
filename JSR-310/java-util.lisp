(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "java.io")
(FOIL:ENSURE-PACKAGE "java.util")
)(DEFCONSTANT |java.util|::LOCALE. '|java.util|::|Locale|)
(DEFCLASS |java.util|::LOCALE.
          (|java.lang|::CLONEABLE. |java.io|::SERIALIZABLE.
           |java.lang|::OBJECT.)
          NIL)
(DEFUN |java.util|::LOCALE.NEW (&REST FOIL::ARGS)
  "public java.util.Locale(java.lang.String)
public java.util.Locale(java.lang.String,java.lang.String)
public java.util.Locale(java.lang.String,java.lang.String,java.lang.String)
"
  (FOIL::CALL-CTOR '|java.util|::|Locale| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|Locale|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::LOCALE.NEW FOIL::ARGS))
(DEFUN |java.util|::LOCALE.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Locale.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "equals"
                          '|java.util|::LOCALE.EQUALS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "toString"
                          '|java.util|::LOCALE.TO-STRING FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Locale.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "hashCode"
                          '|java.util|::LOCALE.HASH-CODE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.CLONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.Locale.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "clone"
                          '|java.util|::LOCALE.CLONE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-LANGUAGE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getLanguage()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getLanguage"
                          '|java.util|::LOCALE.GET-LANGUAGE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-DEFAULT (&REST FOIL::ARGS)
  "public static java.util.Locale java.util.Locale.getDefault()
public static java.util.Locale java.util.Locale.getDefault(java.util.Locale$Category)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getDefault"
                          '|java.util|::LOCALE.GET-DEFAULT NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.LOOKUP (&REST FOIL::ARGS)
  "public static java.util.Locale java.util.Locale.lookup(java.util.List,java.util.Collection)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "lookup"
                          '|java.util|::LOCALE.LOOKUP NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.FILTER (&REST FOIL::ARGS)
  "public static java.util.List java.util.Locale.filter(java.util.List,java.util.Collection,java.util.Locale$FilteringMode)
public static java.util.List java.util.Locale.filter(java.util.List,java.util.Collection)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "filter"
                          '|java.util|::LOCALE.FILTER NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.SET-DEFAULT (&REST FOIL::ARGS)
  "public static synchronized void java.util.Locale.setDefault(java.util.Locale)
public static synchronized void java.util.Locale.setDefault(java.util.Locale$Category,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "setDefault"
                          '|java.util|::LOCALE.SET-DEFAULT NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-AVAILABLE-LOCALES (&REST FOIL::ARGS)
  "public static java.util.Locale[] java.util.Locale.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getAvailableLocales"
                          '|java.util|::LOCALE.GET-AVAILABLE-LOCALES NIL
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-ISO-COUNTRIES (&REST FOIL::ARGS)
  "public static java.lang.String[] java.util.Locale.getISOCountries()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getISOCountries"
                          '|java.util|::LOCALE.GET-ISO-COUNTRIES NIL
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-ISO-LANGUAGES (&REST FOIL::ARGS)
  "public static java.lang.String[] java.util.Locale.getISOLanguages()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getISOLanguages"
                          '|java.util|::LOCALE.GET-ISO-LANGUAGES NIL
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-SCRIPT (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getScript()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getScript"
                          '|java.util|::LOCALE.GET-SCRIPT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-COUNTRY (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getCountry()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getCountry"
                          '|java.util|::LOCALE.GET-COUNTRY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-VARIANT (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getVariant()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getVariant"
                          '|java.util|::LOCALE.GET-VARIANT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.HAS-EXTENSIONS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Locale.hasExtensions()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "hasExtensions"
                          '|java.util|::LOCALE.HAS-EXTENSIONS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.STRIP-EXTENSIONS (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale java.util.Locale.stripExtensions()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "stripExtensions"
                          '|java.util|::LOCALE.STRIP-EXTENSIONS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-EXTENSION (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getExtension(char)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getExtension"
                          '|java.util|::LOCALE.GET-EXTENSION FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-EXTENSION-KEYS (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set java.util.Locale.getExtensionKeys()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getExtensionKeys"
                          '|java.util|::LOCALE.GET-EXTENSION-KEYS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-UNICODE-LOCALE-ATTRIBUTES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set java.util.Locale.getUnicodeLocaleAttributes()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getUnicodeLocaleAttributes"
                          '|java.util|::LOCALE.GET-UNICODE-LOCALE-ATTRIBUTES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-UNICODE-LOCALE-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getUnicodeLocaleType(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getUnicodeLocaleType"
                          '|java.util|::LOCALE.GET-UNICODE-LOCALE-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-UNICODE-LOCALE-KEYS
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set java.util.Locale.getUnicodeLocaleKeys()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getUnicodeLocaleKeys"
                          '|java.util|::LOCALE.GET-UNICODE-LOCALE-KEYS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.TO-LANGUAGE-TAG (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.toLanguageTag()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "toLanguageTag"
                          '|java.util|::LOCALE.TO-LANGUAGE-TAG FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.FOR-LANGUAGE-TAG (&REST FOIL::ARGS)
  "public static java.util.Locale java.util.Locale.forLanguageTag(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "forLanguageTag"
                          '|java.util|::LOCALE.FOR-LANGUAGE-TAG NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-ISO3-LANGUAGE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getISO3Language() throws java.util.MissingResourceException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getISO3Language"
                          '|java.util|::LOCALE.GET-ISO3-LANGUAGE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-ISO3-COUNTRY (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getISO3Country() throws java.util.MissingResourceException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getISO3Country"
                          '|java.util|::LOCALE.GET-ISO3-COUNTRY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-DISPLAY-LANGUAGE (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayLanguage()
public java.lang.String java.util.Locale.getDisplayLanguage(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getDisplayLanguage"
                          '|java.util|::LOCALE.GET-DISPLAY-LANGUAGE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-DISPLAY-SCRIPT (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getDisplayScript()
public java.lang.String java.util.Locale.getDisplayScript(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getDisplayScript"
                          '|java.util|::LOCALE.GET-DISPLAY-SCRIPT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-DISPLAY-COUNTRY (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayCountry()
public java.lang.String java.util.Locale.getDisplayCountry(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getDisplayCountry"
                          '|java.util|::LOCALE.GET-DISPLAY-COUNTRY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-DISPLAY-VARIANT (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getDisplayVariant(java.util.Locale)
public final java.lang.String java.util.Locale.getDisplayVariant()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getDisplayVariant"
                          '|java.util|::LOCALE.GET-DISPLAY-VARIANT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-DISPLAY-NAME (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayName()
public java.lang.String java.util.Locale.getDisplayName(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getDisplayName"
                          '|java.util|::LOCALE.GET-DISPLAY-NAME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.FILTER-TAGS (&REST FOIL::ARGS)
  "public static java.util.List java.util.Locale.filterTags(java.util.List,java.util.Collection,java.util.Locale$FilteringMode)
public static java.util.List java.util.Locale.filterTags(java.util.List,java.util.Collection)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "filterTags"
                          '|java.util|::LOCALE.FILTER-TAGS NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.LOOKUP-TAG (&REST FOIL::ARGS)
  "public static java.lang.String java.util.Locale.lookupTag(java.util.List,java.util.Collection)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "lookupTag"
                          '|java.util|::LOCALE.LOOKUP-TAG NIL FOIL::ARGS))
(DEFUN |java.util|::LOCALE.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "wait"
                          '|java.util|::LOCALE.WAIT FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "getClass"
                          '|java.util|::LOCALE.GET-CLASS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "notify"
                          '|java.util|::LOCALE.NOTIFY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Locale| "notifyAll"
                          '|java.util|::LOCALE.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::LOCALE.+ENGLISH+ ()
  "public static final java.util.Locale java.util.Locale.ENGLISH"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ENGLISH"
                    '|java.util|::LOCALE.ENGLISH* NIL))
(DEFUN (SETF |java.util|::LOCALE.+ENGLISH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ENGLISH"
                    '|java.util|::LOCALE.ENGLISH* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+FRENCH+ ()
  "public static final java.util.Locale java.util.Locale.FRENCH"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "FRENCH"
                    '|java.util|::LOCALE.FRENCH* NIL))
(DEFUN (SETF |java.util|::LOCALE.+FRENCH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "FRENCH"
                    '|java.util|::LOCALE.FRENCH* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+GERMAN+ ()
  "public static final java.util.Locale java.util.Locale.GERMAN"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "GERMAN"
                    '|java.util|::LOCALE.GERMAN* NIL))
(DEFUN (SETF |java.util|::LOCALE.+GERMAN+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "GERMAN"
                    '|java.util|::LOCALE.GERMAN* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+ITALIAN+ ()
  "public static final java.util.Locale java.util.Locale.ITALIAN"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ITALIAN"
                    '|java.util|::LOCALE.ITALIAN* NIL))
(DEFUN (SETF |java.util|::LOCALE.+ITALIAN+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ITALIAN"
                    '|java.util|::LOCALE.ITALIAN* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+JAPANESE+ ()
  "public static final java.util.Locale java.util.Locale.JAPANESE"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "JAPANESE"
                    '|java.util|::LOCALE.JAPANESE* NIL))
(DEFUN (SETF |java.util|::LOCALE.+JAPANESE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "JAPANESE"
                    '|java.util|::LOCALE.JAPANESE* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+KOREAN+ ()
  "public static final java.util.Locale java.util.Locale.KOREAN"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "KOREAN"
                    '|java.util|::LOCALE.KOREAN* NIL))
(DEFUN (SETF |java.util|::LOCALE.+KOREAN+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "KOREAN"
                    '|java.util|::LOCALE.KOREAN* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+CHINESE+ ()
  "public static final java.util.Locale java.util.Locale.CHINESE"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CHINESE"
                    '|java.util|::LOCALE.CHINESE* NIL))
(DEFUN (SETF |java.util|::LOCALE.+CHINESE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CHINESE"
                    '|java.util|::LOCALE.CHINESE* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+SIMPLIFIED_CHINESE+ ()
  "public static final java.util.Locale java.util.Locale.SIMPLIFIED_CHINESE"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "SIMPLIFIED_CHINESE"
                    '|java.util|::LOCALE.SIMPLIFIED_CHINESE* NIL))
(DEFUN (SETF |java.util|::LOCALE.+SIMPLIFIED_CHINESE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "SIMPLIFIED_CHINESE"
                    '|java.util|::LOCALE.SIMPLIFIED_CHINESE* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+TRADITIONAL_CHINESE+ ()
  "public static final java.util.Locale java.util.Locale.TRADITIONAL_CHINESE"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "TRADITIONAL_CHINESE"
                    '|java.util|::LOCALE.TRADITIONAL_CHINESE* NIL))
(DEFUN (SETF |java.util|::LOCALE.+TRADITIONAL_CHINESE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "TRADITIONAL_CHINESE"
                    '|java.util|::LOCALE.TRADITIONAL_CHINESE* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+FRANCE+ ()
  "public static final java.util.Locale java.util.Locale.FRANCE"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "FRANCE"
                    '|java.util|::LOCALE.FRANCE* NIL))
(DEFUN (SETF |java.util|::LOCALE.+FRANCE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "FRANCE"
                    '|java.util|::LOCALE.FRANCE* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+GERMANY+ ()
  "public static final java.util.Locale java.util.Locale.GERMANY"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "GERMANY"
                    '|java.util|::LOCALE.GERMANY* NIL))
(DEFUN (SETF |java.util|::LOCALE.+GERMANY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "GERMANY"
                    '|java.util|::LOCALE.GERMANY* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+ITALY+ ()
  "public static final java.util.Locale java.util.Locale.ITALY"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ITALY" '|java.util|::LOCALE.ITALY*
                    NIL))
(DEFUN (SETF |java.util|::LOCALE.+ITALY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ITALY" '|java.util|::LOCALE.ITALY*
                    NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+JAPAN+ ()
  "public static final java.util.Locale java.util.Locale.JAPAN"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "JAPAN" '|java.util|::LOCALE.JAPAN*
                    NIL))
(DEFUN (SETF |java.util|::LOCALE.+JAPAN+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "JAPAN" '|java.util|::LOCALE.JAPAN*
                    NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+KOREA+ ()
  "public static final java.util.Locale java.util.Locale.KOREA"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "KOREA" '|java.util|::LOCALE.KOREA*
                    NIL))
(DEFUN (SETF |java.util|::LOCALE.+KOREA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "KOREA" '|java.util|::LOCALE.KOREA*
                    NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+CHINA+ ()
  "public static final java.util.Locale java.util.Locale.CHINA"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CHINA" '|java.util|::LOCALE.CHINA*
                    NIL))
(DEFUN (SETF |java.util|::LOCALE.+CHINA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CHINA" '|java.util|::LOCALE.CHINA*
                    NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+PRC+ ()
  "public static final java.util.Locale java.util.Locale.PRC"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "PRC" '|java.util|::LOCALE.PRC* NIL))
(DEFUN (SETF |java.util|::LOCALE.+PRC+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "PRC" '|java.util|::LOCALE.PRC* NIL
                    FOIL::VAL))
(DEFUN |java.util|::LOCALE.+TAIWAN+ ()
  "public static final java.util.Locale java.util.Locale.TAIWAN"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "TAIWAN"
                    '|java.util|::LOCALE.TAIWAN* NIL))
(DEFUN (SETF |java.util|::LOCALE.+TAIWAN+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "TAIWAN"
                    '|java.util|::LOCALE.TAIWAN* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+UK+ ()
  "public static final java.util.Locale java.util.Locale.UK"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "UK" '|java.util|::LOCALE.UK* NIL))
(DEFUN (SETF |java.util|::LOCALE.+UK+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "UK" '|java.util|::LOCALE.UK* NIL
                    FOIL::VAL))
(DEFUN |java.util|::LOCALE.+US+ ()
  "public static final java.util.Locale java.util.Locale.US"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "US" '|java.util|::LOCALE.US* NIL))
(DEFUN (SETF |java.util|::LOCALE.+US+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "US" '|java.util|::LOCALE.US* NIL
                    FOIL::VAL))
(DEFUN |java.util|::LOCALE.+CANADA+ ()
  "public static final java.util.Locale java.util.Locale.CANADA"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CANADA"
                    '|java.util|::LOCALE.CANADA* NIL))
(DEFUN (SETF |java.util|::LOCALE.+CANADA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CANADA"
                    '|java.util|::LOCALE.CANADA* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+CANADA_FRENCH+ ()
  "public static final java.util.Locale java.util.Locale.CANADA_FRENCH"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CANADA_FRENCH"
                    '|java.util|::LOCALE.CANADA_FRENCH* NIL))
(DEFUN (SETF |java.util|::LOCALE.+CANADA_FRENCH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "CANADA_FRENCH"
                    '|java.util|::LOCALE.CANADA_FRENCH* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+ROOT+ ()
  "public static final java.util.Locale java.util.Locale.ROOT"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ROOT" '|java.util|::LOCALE.ROOT*
                    NIL))
(DEFUN (SETF |java.util|::LOCALE.+ROOT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "ROOT" '|java.util|::LOCALE.ROOT*
                    NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+PRIVATE_USE_EXTENSION+ ()
  "public static final char java.util.Locale.PRIVATE_USE_EXTENSION"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "PRIVATE_USE_EXTENSION"
                    '|java.util|::LOCALE.PRIVATE_USE_EXTENSION* NIL))
(DEFUN (SETF |java.util|::LOCALE.+PRIVATE_USE_EXTENSION+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "PRIVATE_USE_EXTENSION"
                    '|java.util|::LOCALE.PRIVATE_USE_EXTENSION* NIL FOIL::VAL))
(DEFUN |java.util|::LOCALE.+UNICODE_LOCALE_EXTENSION+ ()
  "public static final char java.util.Locale.UNICODE_LOCALE_EXTENSION"
  (FOIL::CALL-FIELD '|java.util|::|Locale| "UNICODE_LOCALE_EXTENSION"
                    '|java.util|::LOCALE.UNICODE_LOCALE_EXTENSION* NIL))
(DEFUN (SETF |java.util|::LOCALE.+UNICODE_LOCALE_EXTENSION+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Locale| "UNICODE_LOCALE_EXTENSION"
                    '|java.util|::LOCALE.UNICODE_LOCALE_EXTENSION* NIL
                    FOIL::VAL))
(DEFUN |java.util|::LOCALE.ISO3-COUNTRY-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getISO3Country() throws java.util.MissingResourceException
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "ISO3Country"
                       '|java.util|::LOCALE.ISO3COUNTRY-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.ISO3-LANGUAGE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getISO3Language() throws java.util.MissingResourceException
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "ISO3Language"
                       '|java.util|::LOCALE.ISO3LANGUAGE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "class"
                       '|java.util|::LOCALE.CLASS-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.COUNTRY-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getCountry()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "country"
                       '|java.util|::LOCALE.COUNTRY-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.DISPLAY-COUNTRY-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayCountry()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "displayCountry"
                       '|java.util|::LOCALE.DISPLAYCOUNTRY-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.DISPLAY-LANGUAGE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayLanguage()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "displayLanguage"
                       '|java.util|::LOCALE.DISPLAYLANGUAGE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.DISPLAY-NAME-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayName()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "displayName"
                       '|java.util|::LOCALE.DISPLAYNAME-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.DISPLAY-SCRIPT-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getDisplayScript()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "displayScript"
                       '|java.util|::LOCALE.DISPLAYSCRIPT-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.DISPLAY-VARIANT-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.Locale.getDisplayVariant()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "displayVariant"
                       '|java.util|::LOCALE.DISPLAYVARIANT-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.EXTENSION-KEYS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set java.util.Locale.getExtensionKeys()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "extensionKeys"
                       '|java.util|::LOCALE.EXTENSIONKEYS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.LANGUAGE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getLanguage()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "language"
                       '|java.util|::LOCALE.LANGUAGE-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.SCRIPT-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getScript()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "script"
                       '|java.util|::LOCALE.SCRIPT-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.UNICODE-LOCALE-ATTRIBUTES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set java.util.Locale.getUnicodeLocaleAttributes()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "unicodeLocaleAttributes"
                       '|java.util|::LOCALE.UNICODELOCALEATTRIBUTES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::LOCALE.UNICODE-LOCALE-KEYS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set java.util.Locale.getUnicodeLocaleKeys()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "unicodeLocaleKeys"
                       '|java.util|::LOCALE.UNICODELOCALEKEYS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::LOCALE.VARIANT-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Locale.getVariant()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Locale| "variant"
                       '|java.util|::LOCALE.VARIANT-GET FOIL::THIS FOIL::ARGS))
(eval-when (:load-toplevel)(export '(|java.util|::LOCALE.VARIANT-PROP
                                     |java.util|::LOCALE.UNICODE-LOCALE-KEYS-PROP
                                     |java.util|::LOCALE.UNICODE-LOCALE-ATTRIBUTES-PROP
                                     |java.util|::LOCALE.SCRIPT-PROP
                                     |java.util|::LOCALE.LANGUAGE-PROP
                                     |java.util|::LOCALE.EXTENSION-KEYS-PROP
                                     |java.util|::LOCALE.DISPLAY-VARIANT-PROP
                                     |java.util|::LOCALE.DISPLAY-SCRIPT-PROP
                                     |java.util|::LOCALE.DISPLAY-NAME-PROP
                                     |java.util|::LOCALE.DISPLAY-LANGUAGE-PROP
                                     |java.util|::LOCALE.DISPLAY-COUNTRY-PROP
                                     |java.util|::LOCALE.COUNTRY-PROP
                                     |java.util|::LOCALE.CLASS-PROP
                                     |java.util|::LOCALE.ISO3-LANGUAGE-PROP
                                     |java.util|::LOCALE.ISO3-COUNTRY-PROP
                                     |java.util|::LOCALE.+UNICODE_LOCALE_EXTENSION+
                                     |java.util|::LOCALE.+PRIVATE_USE_EXTENSION+
                                     |java.util|::LOCALE.+ROOT+
                                     |java.util|::LOCALE.+CANADA_FRENCH+
                                     |java.util|::LOCALE.+CANADA+
                                     |java.util|::LOCALE.+US+
                                     |java.util|::LOCALE.+UK+
                                     |java.util|::LOCALE.+TAIWAN+
                                     |java.util|::LOCALE.+PRC+
                                     |java.util|::LOCALE.+CHINA+
                                     |java.util|::LOCALE.+KOREA+
                                     |java.util|::LOCALE.+JAPAN+
                                     |java.util|::LOCALE.+ITALY+
                                     |java.util|::LOCALE.+GERMANY+
                                     |java.util|::LOCALE.+FRANCE+
                                     |java.util|::LOCALE.+TRADITIONAL_CHINESE+
                                     |java.util|::LOCALE.+SIMPLIFIED_CHINESE+
                                     |java.util|::LOCALE.+CHINESE+
                                     |java.util|::LOCALE.+KOREAN+
                                     |java.util|::LOCALE.+JAPANESE+
                                     |java.util|::LOCALE.+ITALIAN+
                                     |java.util|::LOCALE.+GERMAN+
                                     |java.util|::LOCALE.+FRENCH+
                                     |java.util|::LOCALE.+ENGLISH+
                                     |java.util|::LOCALE.NOTIFY-ALL
                                     |java.util|::LOCALE.NOTIFY
                                     |java.util|::LOCALE.GET-CLASS
                                     |java.util|::LOCALE.WAIT
                                     |java.util|::LOCALE.LOOKUP-TAG
                                     |java.util|::LOCALE.FILTER-TAGS
                                     |java.util|::LOCALE.GET-DISPLAY-NAME
                                     |java.util|::LOCALE.GET-DISPLAY-VARIANT
                                     |java.util|::LOCALE.GET-DISPLAY-COUNTRY
                                     |java.util|::LOCALE.GET-DISPLAY-SCRIPT
                                     |java.util|::LOCALE.GET-DISPLAY-LANGUAGE
                                     |java.util|::LOCALE.GET-ISO3-COUNTRY
                                     |java.util|::LOCALE.GET-ISO3-LANGUAGE
                                     |java.util|::LOCALE.FOR-LANGUAGE-TAG
                                     |java.util|::LOCALE.TO-LANGUAGE-TAG
                                     |java.util|::LOCALE.GET-UNICODE-LOCALE-KEYS
                                     |java.util|::LOCALE.GET-UNICODE-LOCALE-TYPE
                                     |java.util|::LOCALE.GET-UNICODE-LOCALE-ATTRIBUTES
                                     |java.util|::LOCALE.GET-EXTENSION-KEYS
                                     |java.util|::LOCALE.GET-EXTENSION
                                     |java.util|::LOCALE.STRIP-EXTENSIONS
                                     |java.util|::LOCALE.HAS-EXTENSIONS
                                     |java.util|::LOCALE.GET-VARIANT
                                     |java.util|::LOCALE.GET-COUNTRY
                                     |java.util|::LOCALE.GET-SCRIPT
                                     |java.util|::LOCALE.GET-ISO-LANGUAGES
                                     |java.util|::LOCALE.GET-ISO-COUNTRIES
                                     |java.util|::LOCALE.GET-AVAILABLE-LOCALES
                                     |java.util|::LOCALE.SET-DEFAULT
                                     |java.util|::LOCALE.FILTER
                                     |java.util|::LOCALE.LOOKUP
                                     |java.util|::LOCALE.GET-DEFAULT
                                     |java.util|::LOCALE.GET-LANGUAGE
                                     |java.util|::LOCALE.CLONE
                                     |java.util|::LOCALE.HASH-CODE
                                     |java.util|::LOCALE.TO-STRING
                                     |java.util|::LOCALE.EQUALS
                                     |java.util|::LOCALE.NEW
                                     |java.util|::LOCALE.) "java.util"))
