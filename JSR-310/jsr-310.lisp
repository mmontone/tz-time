(in-package :tz-time.jsr310)

(setf foil:*fvm* (foil:make-foreign-vm))

(defparameter +jre-jar+ "/usr/lib/jvm/java-8-oracle/jre/lib/rt.jar")

(defun generate-java-wrappers ()
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-jsr310 "JSR-310/java-time.lisp")
   (foil:get-library-classnames +jre-jar+  "java/time"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-jsr310 "JSR-310/java-util.lisp")
										;(foil:get-library-classnames +jre-jar+  "java/util")
   (list "java.util.Locale")))
