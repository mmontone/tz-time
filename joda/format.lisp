(in-package :tz-time)

;; Formatting

;; From local-time library:

(defparameter +iso-8601-date-format+
  '((:year 4) #\- (:month 2) #\- (:day 2)))

(defparameter +iso-8601-time-format+
  '((:hour 2) #\: (:min 2) #\: (:sec 2) #\. (:usec 6)))

(defparameter +iso-8601-format+
  ;; 2008-11-18T02:32:00.586931+01:00
  (append +iso-8601-date-format+ (list #\T) +iso-8601-time-format+ (list :gmt-offset-or-z)))

(defparameter +rfc3339-format+ +iso-8601-format+)

(defparameter +rfc3339-format/date-only+
  '((:year 4) #\- (:month 2) #\- (:day 2)))

(defparameter +asctime-format+
  '(:short-weekday #\space :short-month #\space (:day 2 #\space) #\space
    (:hour 2) #\: (:min 2) #\: (:sec 2) #\space
    (:year 4)))

(defparameter +rfc-1123-format+
  ;; Sun, 06 Nov 1994 08:49:37 GMT
  '(:short-weekday ", " (:day 2) #\space :short-month #\space (:year 4) #\space
    (:hour 2) #\: (:min 2) #\: (:sec 2) #\space :gmt-offset-hhmm)
  "See the RFC 1123 for the details about the possible values of the timezone field.")

(defparameter +iso-week-date-format+
  ;; 2009-W53-5
  '((:iso-week-year 4) #\- #\W (:iso-week-number 2) #\- (:iso-week-day 1)))

(defvar +tz-timestamp-full-format+
  (append +rfc3339-format+ (list #\space #\@ #\space :tz-name)))

(defun ordinalize (day)
  "Return an ordinal string representing the position of DAY in a sequence (1st, 2nd, 3rd, 4th, etc)."
  (declare (type (integer 1 31) day))
  (format nil "~d~a" day
          (if (<= 11 day 13)
              "th"
              (case (mod day 10)
                (1 "st")
                (2 "nd")
                (3 "rd")
                (t "th")))))

(defun %construct-timestring (timestamp format timezone)
  "Constructs a string representing TIMESTAMP given the FORMAT of the string and the TIMEZONE.  See the documentation of FORMAT-TIMESTRING for the structure of FORMAT."
  (declare (type timestamp tz-timestamp)
           (optimize (speed 3)))
  (multiple-value-bind (nsec sec minute hour day month year weekday daylight-p offset abbrev)
      (decode-timestamp timestamp :timezone timezone)
    (declare (ignore daylight-p))
    (multiple-value-bind (iso-year iso-week iso-weekday)
        (%timestamp-decode-iso-week timestamp)
      (let ((*print-pretty* nil)
            (*print-circle* nil))
        (with-output-to-string (result nil)
          (dolist (fmt format)
            (cond
              ((member fmt '(:gmt-offset :gmt-offset-or-z :gmt-offset-hhmm))
               (multiple-value-bind (offset-hours offset-secs)
                   (floor offset +seconds-per-hour+)
                 (declare (fixnum offset-hours offset-secs))
                 (if (and (eql fmt :gmt-offset-or-z) (zerop offset))
                     (princ #\Z result)
                     (format result "~c~2,'0d~:[:~;~]~2,'0d"
                             (if (minusp offset-hours) #\- #\+)
                             (abs offset-hours)
                             (eql fmt :gmt-offset-hhmm)
                             (truncate (abs offset-secs)
                                       +seconds-per-minute+)))))
              ((eql fmt :short-year)
               (princ (mod year 100) result))
              ((eql fmt :long-month)
               (princ (aref +month-names+ month) result))
              ((eql fmt :short-month)
               (princ (aref +short-month-names+ month) result))
              ((eql fmt :long-weekday)
               (princ (aref +day-names+ weekday) result))
              ((eql fmt :short-weekday)
               (princ (aref +short-day-names+ weekday) result))
              ((eql fmt :minimal-weekday)
               (princ (aref +minimal-day-names+ weekday) result))
              ((eql fmt :timezone)
               (princ abbrev result))
              ((eql fmt :ampm)
               (princ (if (< hour 12) "am" "pm") result))
              ((eql fmt :ordinal-day)
               (princ (ordinalize day) result))
              ((or (stringp fmt) (characterp fmt))
               (princ fmt result))
              (t
               (let ((val (ecase (if (consp fmt) (car fmt) fmt)
                            (:nsec nsec)
                            (:usec (floor nsec 1000))
                            (:msec (floor nsec 1000000))
                            (:sec sec)
                            (:min minute)
                            (:hour hour)
                            (:hour12 (1+ (mod (1- hour) 12)))
                            (:day day)
                            (:weekday weekday)
                            (:month month)
                            (:year year)
                            (:iso-week-year iso-year)
                            (:iso-week-number iso-week)
                            (:iso-week-day iso-weekday))))
                 (cond
                   ((atom fmt)
                    (princ val result))
                   ((minusp val)
                    (format result "-~v,vd"
                            (second fmt)
                            (or (third fmt) #\0)
                            (abs val)))
                   (t
                    (format result "~v,vd"
                            (second fmt)
                            (or (third fmt) #\0)
                            val))))))))))))


(defun format-timestring (destination timestamp &key
                          (format +iso-8601-format+)
                          (timezone *default-timezone*))
  "Constructs a string representation of TIMESTAMP according to FORMAT and returns it.  If destination is T, the string is written to *standard-output*.  If destination is a stream, the string is written to the stream.

FORMAT is a list containing one or more of strings, characters, and keywords. Strings and characters are output literally, while keywords are replaced by the values here:

  :YEAR              *year
  :MONTH             *numeric month
  :DAY               *day of month
  :HOUR              *hour
  :MIN               *minutes
  :SEC               *seconds
  :WEEKDAY           *numeric day of week starting from index 0, which means Sunday
  :MSEC              *milliseconds
  :USEC              *microseconds
  :NSEC              *nanoseconds
  :ISO-WEEK-YEAR     *year for ISO week date (can be different from regular calendar year)
  :ISO-WEEK-NUMBER   *ISO week number (i.e. 1 through 53)
  :ISO-WEEK-DAY      *ISO compatible weekday number (monday=1, sunday=7)
  :LONG-WEEKDAY      long form of weekday (e.g. Sunday, Monday)
  :SHORT-WEEKDAY     short form of weekday (e.g. Sun, Mon)
  :MINIMAL-WEEKDAY   minimal form of weekday (e.g. Su, Mo)
  :LONG-MONTH        long form of month (e.g. January, February)
  :SHORT-MONTH       short form of month (e.g. Jan, Feb)
  :HOUR12            *hour on a 12-hour clock
  :AMPM              am/pm marker in lowercase
  :GMT-OFFSET        the gmt-offset of the time, in +00:00 form
  :GMT-OFFSET-OR-Z   like :GMT-OFFSET, but is Z when UTC
  :GMT-OFFSET-HHMM   like :GMT-OFFSET, but in +0000 form
  :TIMEZONE          timezone abbrevation for the time
  :TZ-NAME           the timezone name

Elements marked by * can be placed in a list in the form: \(:keyword padding &optional \(padchar #\0))

The string representation of the value will be padded with the padchar.

You can see examples in +ISO-8601-FORMAT+, +ASCTIME-FORMAT+, and +RFC-1123-FORMAT+."
  (declare (type (or boolean stream) destination))
  (let ((result (%construct-timestring timestamp format timezone)))
    (when destination
      (write-string result (if (eq t destination) *standard-output* destination)))
    result))

(defun format-rfc1123-timestring (destination timestamp &key
                                  (timezone *default-timezone*))
  (format-timestring destination timestamp
                     :format +rfc-1123-format+
                     :timezone timezone))

(defun to-rfc1123-timestring (timestamp)
  (format-rfc1123-timestring nil timestamp))

(defun format-rfc3339-timestring (destination timestamp &key
                                  omit-date-part
                                  omit-time-part
                                  (omit-timezone-part omit-time-part)
                                  (use-zulu t)
                                  (timezone *default-timezone*))
  "Formats a timestring in the RFC 3339 format, a restricted form of the ISO-8601 timestring specification for Internet timestamps."
  (let ((rfc3339-format
         (if (and use-zulu
                  (not omit-date-part)
                  (not omit-time-part)
                  (not omit-timezone-part))
             +rfc3339-format+ ; micro optimization
             (append
              (unless omit-date-part
                '((:year 4) #\-
                  (:month 2) #\-
                  (:day 2)))
              (unless (or omit-date-part
                          omit-time-part)
                '(#\T))
              (unless omit-time-part
                '((:hour 2) #\:
                  (:min 2) #\:
                  (:sec 2) #\.
                  (:usec 6)))
              (unless omit-timezone-part
                (if use-zulu
                    '(:gmt-offset-or-z)
                    '(:gmt-offset)))))))
    (format-timestring destination timestamp :format rfc3339-format :timezone timezone)))

(defun to-rfc3339-timestring (timestamp)
  (format-rfc3339-timestring nil timestamp))
