(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE "java.io")
(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "org.joda.time")
(FOIL:ENSURE-PACKAGE "org.joda.time.base")
)(DEFCONSTANT |org.joda.time.base|::ABSTRACTDATETIME.
   '|org.joda.time.base|::|AbstractDateTime|)
(DEFCLASS |org.joda.time.base|::ABSTRACTDATETIME.
          (|org.joda.time.base|::ABSTRACTINSTANT.
           |org.joda.time|::READABLEDATETIME.)
          NIL)
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS-OF-SECOND
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfSecond()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getMillisOfSecond"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS-OF-SECOND
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-CALENDAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar org.joda.time.base.AbstractDateTime.toCalendar(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "toCalendar"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-CALENDAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getEra()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "getEra"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-CENTURY-OF-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getCenturyOfEra()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getCenturyOfEra"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-CENTURY-OF-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR-OF-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfEra()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getYearOfEra"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR-OF-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR-OF-CENTURY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfCentury()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getYearOfCentury"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR-OF-CENTURY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "getYear"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekyear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getWeekyear"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-MONTH-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMonthOfYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getMonthOfYear"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-MONTH-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getDayOfYear"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-MONTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfMonth()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getDayOfMonth"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-MONTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfWeek()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getDayOfWeek"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-HOUR-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getHourOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getHourOfDay"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-HOUR-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-MINUTE-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getMinuteOfDay"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-MINUTE-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-MINUTE-OF-HOUR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfHour()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getMinuteOfHour"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-MINUTE-OF-HOUR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-SECOND-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getSecondOfDay"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-SECOND-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-SECOND-OF-MINUTE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfMinute()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getSecondOfMinute"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-SECOND-OF-MINUTE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getMillisOfDay"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-WEEK-OF-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekOfWeekyear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getWeekOfWeekyear"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-WEEK-OF-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-GREGORIAN-CALENDAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.GregorianCalendar org.joda.time.base.AbstractDateTime.toGregorianCalendar()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "toGregorianCalendar"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-GREGORIAN-CALENDAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.get(org.joda.time.DateTimeField)
public int org.joda.time.base.AbstractDateTime.get(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "get"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractInstant.toString(org.joda.time.format.DateTimeFormatter)
public java.lang.String org.joda.time.base.AbstractDateTime.toString()
public java.lang.String org.joda.time.base.AbstractDateTime.toString(java.lang.String,java.util.Locale) throws java.lang.IllegalArgumentException
public java.lang.String org.joda.time.base.AbstractDateTime.toString(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "toString"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.base.AbstractInstant.getZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "getZone"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-DATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Date org.joda.time.base.AbstractInstant.toDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "toDate"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime(org.joda.time.Chronology)
public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime(org.joda.time.DateTimeZone)
public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "toDateTime"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-BEFORE-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBeforeNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "isBeforeNow"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-BEFORE-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-EQUAL-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqualNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "isEqualNow"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-EQUAL-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-MUTABLE-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime(org.joda.time.DateTimeZone)
public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime(org.joda.time.Chronology)
public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "toMutableDateTime"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-MUTABLE-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-INSTANT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Instant org.joda.time.base.AbstractInstant.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "toInstant"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-INSTANT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-MUTABLE-DATE-TIME-+ISO+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTimeISO()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "toMutableDateTimeISO"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-MUTABLE-DATE-TIME-+ISO+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-AFTER-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfterNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "isAfterNow"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-AFTER-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfter(long)
public boolean org.joda.time.base.AbstractInstant.isAfter(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "isAfter"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBefore(long)
public boolean org.joda.time.base.AbstractInstant.isBefore(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "isBefore"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.TO-DATE-TIME-+ISO+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTimeISO()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "toDateTimeISO"
                          '|org.joda.time.base|::ABSTRACTDATETIME.TO-DATE-TIME-+ISO+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqual(long)
public boolean org.joda.time.base.AbstractInstant.isEqual(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "isEqual"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "equals"
                          '|org.joda.time.base|::ABSTRACTDATETIME.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "hashCode"
                          '|org.joda.time.base|::ABSTRACTDATETIME.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.compareTo(org.joda.time.ReadableInstant)
public int org.joda.time.base.AbstractInstant.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "compareTo"
                          '|org.joda.time.base|::ABSTRACTDATETIME.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isSupported(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "isSupported"
                          '|org.joda.time.base|::ABSTRACTDATETIME.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "wait"
                          '|org.joda.time.base|::ABSTRACTDATETIME.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "getClass"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "notify"
                          '|org.joda.time.base|::ABSTRACTDATETIME.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "notifyAll"
                          '|org.joda.time.base|::ABSTRACTDATETIME.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.ReadableInstant.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime|
                          "getChronology"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.ReadableInstant.getMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDateTime| "getMillis"
                          '|org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.AFTER-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfterNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "afterNow"
                       '|org.joda.time.base|::ABSTRACTDATETIME.AFTERNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.BEFORE-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBeforeNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "beforeNow"
                       '|org.joda.time.base|::ABSTRACTDATETIME.BEFORENOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.CENTURY-OF-ERA-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getCenturyOfEra()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "centuryOfEra"
                       '|org.joda.time.base|::ABSTRACTDATETIME.CENTURYOFERA-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "class"
                       '|org.joda.time.base|::ABSTRACTDATETIME.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.DAY-OF-MONTH-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfMonth()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "dayOfMonth"
                       '|org.joda.time.base|::ABSTRACTDATETIME.DAYOFMONTH-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.DAY-OF-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfWeek()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "dayOfWeek"
                       '|org.joda.time.base|::ABSTRACTDATETIME.DAYOFWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.DAY-OF-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "dayOfYear"
                       '|org.joda.time.base|::ABSTRACTDATETIME.DAYOFYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.EQUAL-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqualNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "equalNow"
                       '|org.joda.time.base|::ABSTRACTDATETIME.EQUALNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.ERA-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getEra()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "era"
                       '|org.joda.time.base|::ABSTRACTDATETIME.ERA-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.HOUR-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getHourOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "hourOfDay"
                       '|org.joda.time.base|::ABSTRACTDATETIME.HOUROFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.MILLIS-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "millisOfDay"
                       '|org.joda.time.base|::ABSTRACTDATETIME.MILLISOFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.MILLIS-OF-SECOND-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfSecond()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime|
                       "millisOfSecond"
                       '|org.joda.time.base|::ABSTRACTDATETIME.MILLISOFSECOND-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.MINUTE-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "minuteOfDay"
                       '|org.joda.time.base|::ABSTRACTDATETIME.MINUTEOFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.MINUTE-OF-HOUR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfHour()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "minuteOfHour"
                       '|org.joda.time.base|::ABSTRACTDATETIME.MINUTEOFHOUR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.MONTH-OF-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMonthOfYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "monthOfYear"
                       '|org.joda.time.base|::ABSTRACTDATETIME.MONTHOFYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.SECOND-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "secondOfDay"
                       '|org.joda.time.base|::ABSTRACTDATETIME.SECONDOFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.SECOND-OF-MINUTE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfMinute()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime|
                       "secondOfMinute"
                       '|org.joda.time.base|::ABSTRACTDATETIME.SECONDOFMINUTE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.WEEK-OF-WEEKYEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekOfWeekyear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime|
                       "weekOfWeekyear"
                       '|org.joda.time.base|::ABSTRACTDATETIME.WEEKOFWEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.WEEKYEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekyear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "weekyear"
                       '|org.joda.time.base|::ABSTRACTDATETIME.WEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "year"
                       '|org.joda.time.base|::ABSTRACTDATETIME.YEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.YEAR-OF-CENTURY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfCentury()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime|
                       "yearOfCentury"
                       '|org.joda.time.base|::ABSTRACTDATETIME.YEAROFCENTURY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.YEAR-OF-ERA-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfEra()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "yearOfEra"
                       '|org.joda.time.base|::ABSTRACTDATETIME.YEAROFERA-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDATETIME.ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.base.AbstractInstant.getZone()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDateTime| "zone"
                       '|org.joda.time.base|::ABSTRACTDATETIME.ZONE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::ABSTRACTDURATION.
  '|org.joda.time.base|::|AbstractDuration|)
(DEFCLASS |org.joda.time.base|::ABSTRACTDURATION.
          (|org.joda.time|::READABLEDURATION. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.TO-DURATION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Duration org.joda.time.base.AbstractDuration.toDuration()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration|
                          "toDuration"
                          '|org.joda.time.base|::ABSTRACTDURATION.TO-DURATION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.TO-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.AbstractDuration.toPeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "toPeriod"
                          '|org.joda.time.base|::ABSTRACTDURATION.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.IS-LONGER-THAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.isLongerThan(org.joda.time.ReadableDuration)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration|
                          "isLongerThan"
                          '|org.joda.time.base|::ABSTRACTDURATION.IS-LONGER-THAN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.IS-SHORTER-THAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.isShorterThan(org.joda.time.ReadableDuration)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration|
                          "isShorterThan"
                          '|org.joda.time.base|::ABSTRACTDURATION.IS-SHORTER-THAN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.isEqual(org.joda.time.ReadableDuration)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "isEqual"
                          '|org.joda.time.base|::ABSTRACTDURATION.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "equals"
                          '|org.joda.time.base|::ABSTRACTDURATION.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractDuration.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "toString"
                          '|org.joda.time.base|::ABSTRACTDURATION.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDuration.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "hashCode"
                          '|org.joda.time.base|::ABSTRACTDURATION.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDuration.compareTo(org.joda.time.ReadableDuration)
public int org.joda.time.base.AbstractDuration.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "compareTo"
                          '|org.joda.time.base|::ABSTRACTDURATION.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "wait"
                          '|org.joda.time.base|::ABSTRACTDURATION.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "getClass"
                          '|org.joda.time.base|::ABSTRACTDURATION.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "notify"
                          '|org.joda.time.base|::ABSTRACTDURATION.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "notifyAll"
                          '|org.joda.time.base|::ABSTRACTDURATION.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.ReadableDuration.getMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractDuration| "getMillis"
                          '|org.joda.time.base|::ABSTRACTDURATION.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTDURATION.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractDuration| "class"
                       '|org.joda.time.base|::ABSTRACTDURATION.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::ABSTRACTINSTANT.
  '|org.joda.time.base|::|AbstractInstant|)
(DEFCLASS |org.joda.time.base|::ABSTRACTINSTANT.
          (|org.joda.time|::READABLEINSTANT. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.GET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.base.AbstractInstant.getZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "getZone"
                          '|org.joda.time.base|::ABSTRACTINSTANT.GET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-DATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Date org.joda.time.base.AbstractInstant.toDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "toDate"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime(org.joda.time.Chronology)
public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime(org.joda.time.DateTimeZone)
public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "toDateTime"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-BEFORE-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBeforeNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant|
                          "isBeforeNow"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-BEFORE-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-EQUAL-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqualNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "isEqualNow"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-EQUAL-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-MUTABLE-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime(org.joda.time.DateTimeZone)
public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime(org.joda.time.Chronology)
public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant|
                          "toMutableDateTime"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-MUTABLE-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-INSTANT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Instant org.joda.time.base.AbstractInstant.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "toInstant"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-INSTANT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-MUTABLE-DATE-TIME-+ISO+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTimeISO()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant|
                          "toMutableDateTimeISO"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-MUTABLE-DATE-TIME-+ISO+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-AFTER-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfterNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "isAfterNow"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-AFTER-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfter(long)
public boolean org.joda.time.base.AbstractInstant.isAfter(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "isAfter"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBefore(long)
public boolean org.joda.time.base.AbstractInstant.isBefore(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "isBefore"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-DATE-TIME-+ISO+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTimeISO()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant|
                          "toDateTimeISO"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-DATE-TIME-+ISO+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqual(long)
public boolean org.joda.time.base.AbstractInstant.isEqual(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "isEqual"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.get(org.joda.time.DateTimeField)
public int org.joda.time.base.AbstractInstant.get(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "get"
                          '|org.joda.time.base|::ABSTRACTINSTANT.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "equals"
                          '|org.joda.time.base|::ABSTRACTINSTANT.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractInstant.toString(org.joda.time.format.DateTimeFormatter)
public java.lang.String org.joda.time.base.AbstractInstant.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "toString"
                          '|org.joda.time.base|::ABSTRACTINSTANT.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "hashCode"
                          '|org.joda.time.base|::ABSTRACTINSTANT.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.compareTo(org.joda.time.ReadableInstant)
public int org.joda.time.base.AbstractInstant.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "compareTo"
                          '|org.joda.time.base|::ABSTRACTINSTANT.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isSupported(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant|
                          "isSupported"
                          '|org.joda.time.base|::ABSTRACTINSTANT.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "wait"
                          '|org.joda.time.base|::ABSTRACTINSTANT.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "getClass"
                          '|org.joda.time.base|::ABSTRACTINSTANT.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "notify"
                          '|org.joda.time.base|::ABSTRACTINSTANT.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "notifyAll"
                          '|org.joda.time.base|::ABSTRACTINSTANT.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.ReadableInstant.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant|
                          "getChronology"
                          '|org.joda.time.base|::ABSTRACTINSTANT.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.ReadableInstant.getMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInstant| "getMillis"
                          '|org.joda.time.base|::ABSTRACTINSTANT.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.AFTER-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfterNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInstant| "afterNow"
                       '|org.joda.time.base|::ABSTRACTINSTANT.AFTERNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.BEFORE-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBeforeNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInstant| "beforeNow"
                       '|org.joda.time.base|::ABSTRACTINSTANT.BEFORENOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInstant| "class"
                       '|org.joda.time.base|::ABSTRACTINSTANT.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.EQUAL-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqualNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInstant| "equalNow"
                       '|org.joda.time.base|::ABSTRACTINSTANT.EQUALNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINSTANT.ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.base.AbstractInstant.getZone()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInstant| "zone"
                       '|org.joda.time.base|::ABSTRACTINSTANT.ZONE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::ABSTRACTINTERVAL.
  '|org.joda.time.base|::|AbstractInterval|)
(DEFCLASS |org.joda.time.base|::ABSTRACTINTERVAL.
          (|org.joda.time|::READABLEINTERVAL. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.IS-BEFORE-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isBeforeNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "isBeforeNow"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.IS-BEFORE-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.IS-AFTER-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isAfterNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "isAfterNow"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.IS-AFTER-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.IS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isAfter(org.joda.time.ReadableInstant)
public boolean org.joda.time.base.AbstractInterval.isAfter(org.joda.time.ReadableInterval)
public boolean org.joda.time.base.AbstractInterval.isAfter(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "isAfter"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isBefore(org.joda.time.ReadableInterval)
public boolean org.joda.time.base.AbstractInterval.isBefore(long)
public boolean org.joda.time.base.AbstractInterval.isBefore(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "isBefore"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.TO-DURATION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Duration org.joda.time.base.AbstractInterval.toDuration()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "toDuration"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.TO-DURATION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.TO-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.AbstractInterval.toPeriod(org.joda.time.PeriodType)
public org.joda.time.Period org.joda.time.base.AbstractInterval.toPeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "toPeriod"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.GET-START
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getStart()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "getStart"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.GET-START
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.GET-END
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getEnd()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "getEnd"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.GET-END
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.CONTAINS-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.containsNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "containsNow"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.CONTAINS-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.OVERLAPS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.overlaps(org.joda.time.ReadableInterval)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "overlaps"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.OVERLAPS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.TO-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Interval org.joda.time.base.AbstractInterval.toInterval()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "toInterval"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.TO-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.TO-MUTABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableInterval org.joda.time.base.AbstractInterval.toMutableInterval()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "toMutableInterval"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.TO-MUTABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.TO-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.AbstractInterval.toDurationMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "toDurationMillis"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.TO-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isEqual(org.joda.time.ReadableInterval)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "isEqual"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "equals"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractInterval.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "toString"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInterval.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "hashCode"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.CONTAINS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.contains(org.joda.time.ReadableInterval)
public boolean org.joda.time.base.AbstractInterval.contains(org.joda.time.ReadableInstant)
public boolean org.joda.time.base.AbstractInterval.contains(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "contains"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.CONTAINS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "wait"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "getClass"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "notify"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval| "notifyAll"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.ReadableInterval.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "getChronology"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.GET-START-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.ReadableInterval.getStartMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "getStartMillis"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.GET-START-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.GET-END-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.ReadableInterval.getEndMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractInterval|
                          "getEndMillis"
                          '|org.joda.time.base|::ABSTRACTINTERVAL.GET-END-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.AFTER-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isAfterNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInterval| "afterNow"
                       '|org.joda.time.base|::ABSTRACTINTERVAL.AFTERNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.BEFORE-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isBeforeNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInterval| "beforeNow"
                       '|org.joda.time.base|::ABSTRACTINTERVAL.BEFORENOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInterval| "class"
                       '|org.joda.time.base|::ABSTRACTINTERVAL.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.END-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getEnd()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInterval| "end"
                       '|org.joda.time.base|::ABSTRACTINTERVAL.END-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTINTERVAL.START-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getStart()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractInterval| "start"
                       '|org.joda.time.base|::ABSTRACTINTERVAL.START-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::ABSTRACTPARTIAL.
  '|org.joda.time.base|::|AbstractPartial|)
(DEFCLASS |org.joda.time.base|::ABSTRACTPARTIAL.
          (|org.joda.time|::READABLEPARTIAL. |java.lang|::COMPARABLE.
           |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.TO-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractPartial.toDateTime(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "toDateTime"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.TO-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.IS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isAfter(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "isAfter"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isBefore(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "isBefore"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPartial.getValues()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "getValues"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD-TYPES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType[] org.joda.time.base.AbstractPartial.getFieldTypes()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial|
                          "getFieldTypes"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD-TYPES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isEqual(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "isEqual"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.get(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "get"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "equals"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
public java.lang.String org.joda.time.base.AbstractPartial.toString(org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "toString"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "hashCode"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.compareTo(java.lang.Object)
public int org.joda.time.base.AbstractPartial.compareTo(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "compareTo"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.INDEX-OF
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.indexOf(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "indexOf"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.INDEX-OF
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELDS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField[] org.joda.time.base.AbstractPartial.getFields()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "getFields"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELDS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField org.joda.time.base.AbstractPartial.getField(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "getField"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.base.AbstractPartial.getFieldType(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial|
                          "getFieldType"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isSupported(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial|
                          "isSupported"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "wait"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "getClass"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "notify"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "notifyAll"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.ReadablePartial.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial|
                          "getChronology"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.ReadablePartial.getValue(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "getValue"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.SIZE (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.ReadablePartial.size()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPartial| "size"
                          '|org.joda.time.base|::ABSTRACTPARTIAL.SIZE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPartial| "class"
                       '|org.joda.time.base|::ABSTRACTPARTIAL.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.FIELD-TYPES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType[] org.joda.time.base.AbstractPartial.getFieldTypes()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPartial| "fieldTypes"
                       '|org.joda.time.base|::ABSTRACTPARTIAL.FIELDTYPES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.FIELDS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField[] org.joda.time.base.AbstractPartial.getFields()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPartial| "fields"
                       '|org.joda.time.base|::ABSTRACTPARTIAL.FIELDS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPARTIAL.VALUES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPartial.getValues()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPartial| "values"
                       '|org.joda.time.base|::ABSTRACTPARTIAL.VALUES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::ABSTRACTPERIOD.
  '|org.joda.time.base|::|AbstractPeriod|)
(DEFCLASS |org.joda.time.base|::ABSTRACTPERIOD.
          (|org.joda.time|::READABLEPERIOD. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.TO-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.AbstractPeriod.toPeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "toPeriod"
                          '|org.joda.time.base|::ABSTRACTPERIOD.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPeriod.getValues()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "getValues"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.TO-MUTABLE-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutablePeriod org.joda.time.base.AbstractPeriod.toMutablePeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod|
                          "toMutablePeriod"
                          '|org.joda.time.base|::ABSTRACTPERIOD.TO-MUTABLE-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET-FIELD-TYPES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType[] org.joda.time.base.AbstractPeriod.getFieldTypes()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod|
                          "getFieldTypes"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET-FIELD-TYPES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.get(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "get"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPeriod.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "equals"
                          '|org.joda.time.base|::ABSTRACTPERIOD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractPeriod.toString(org.joda.time.format.PeriodFormatter)
public java.lang.String org.joda.time.base.AbstractPeriod.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "toString"
                          '|org.joda.time.base|::ABSTRACTPERIOD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "hashCode"
                          '|org.joda.time.base|::ABSTRACTPERIOD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.INDEX-OF
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.indexOf(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "indexOf"
                          '|org.joda.time.base|::ABSTRACTPERIOD.INDEX-OF
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.SIZE (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.size()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "size"
                          '|org.joda.time.base|::ABSTRACTPERIOD.SIZE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType org.joda.time.base.AbstractPeriod.getFieldType(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod|
                          "getFieldType"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET-FIELD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPeriod.isSupported(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "isSupported"
                          '|org.joda.time.base|::ABSTRACTPERIOD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "wait"
                          '|org.joda.time.base|::ABSTRACTPERIOD.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "getClass"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "notify"
                          '|org.joda.time.base|::ABSTRACTPERIOD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "notifyAll"
                          '|org.joda.time.base|::ABSTRACTPERIOD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.PeriodType org.joda.time.ReadablePeriod.getPeriodType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod|
                          "getPeriodType"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.ReadablePeriod.getValue(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|AbstractPeriod| "getValue"
                          '|org.joda.time.base|::ABSTRACTPERIOD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPeriod| "class"
                       '|org.joda.time.base|::ABSTRACTPERIOD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.FIELD-TYPES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType[] org.joda.time.base.AbstractPeriod.getFieldTypes()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPeriod| "fieldTypes"
                       '|org.joda.time.base|::ABSTRACTPERIOD.FIELDTYPES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::ABSTRACTPERIOD.VALUES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPeriod.getValues()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|AbstractPeriod| "values"
                       '|org.joda.time.base|::ABSTRACTPERIOD.VALUES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASEDATETIME.
  '|org.joda.time.base|::|BaseDateTime|)
(DEFCLASS |org.joda.time.base|::BASEDATETIME.
          (|org.joda.time.base|::ABSTRACTDATETIME.
           |org.joda.time|::READABLEDATETIME. |java.io|::SERIALIZABLE.)
          NIL)
(DEFUN |org.joda.time.base|::BASEDATETIME.NEW (&REST FOIL::ARGS)
  "public org.joda.time.base.BaseDateTime(int,int,int,int,int,int,int,org.joda.time.Chronology)
public org.joda.time.base.BaseDateTime(int,int,int,int,int,int,int,org.joda.time.DateTimeZone)
public org.joda.time.base.BaseDateTime()
public org.joda.time.base.BaseDateTime(long,org.joda.time.DateTimeZone)
public org.joda.time.base.BaseDateTime(long)
public org.joda.time.base.BaseDateTime(org.joda.time.Chronology)
public org.joda.time.base.BaseDateTime(org.joda.time.DateTimeZone)
public org.joda.time.base.BaseDateTime(int,int,int,int,int,int,int)
public org.joda.time.base.BaseDateTime(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.base.BaseDateTime(java.lang.Object,org.joda.time.DateTimeZone)
public org.joda.time.base.BaseDateTime(long,org.joda.time.Chronology)
"
  (FOIL::CALL-CTOR '|org.joda.time.base|::|BaseDateTime| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.base|::|BaseDateTime|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.base|::BASEDATETIME.NEW FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.base.BaseDateTime.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getChronology"
                          '|org.joda.time.base|::BASEDATETIME.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseDateTime.getMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getMillis"
                          '|org.joda.time.base|::BASEDATETIME.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-MILLIS-OF-SECOND
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfSecond()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getMillisOfSecond"
                          '|org.joda.time.base|::BASEDATETIME.GET-MILLIS-OF-SECOND
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-CALENDAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar org.joda.time.base.AbstractDateTime.toCalendar(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "toCalendar"
                          '|org.joda.time.base|::BASEDATETIME.TO-CALENDAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-ERA (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getEra()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getEra"
                          '|org.joda.time.base|::BASEDATETIME.GET-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-CENTURY-OF-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getCenturyOfEra()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getCenturyOfEra"
                          '|org.joda.time.base|::BASEDATETIME.GET-CENTURY-OF-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-YEAR-OF-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfEra()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getYearOfEra"
                          '|org.joda.time.base|::BASEDATETIME.GET-YEAR-OF-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-YEAR-OF-CENTURY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfCentury()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getYearOfCentury"
                          '|org.joda.time.base|::BASEDATETIME.GET-YEAR-OF-CENTURY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getYear"
                          '|org.joda.time.base|::BASEDATETIME.GET-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekyear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getWeekyear"
                          '|org.joda.time.base|::BASEDATETIME.GET-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-MONTH-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMonthOfYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getMonthOfYear"
                          '|org.joda.time.base|::BASEDATETIME.GET-MONTH-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-DAY-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getDayOfYear"
                          '|org.joda.time.base|::BASEDATETIME.GET-DAY-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-DAY-OF-MONTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfMonth()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getDayOfMonth"
                          '|org.joda.time.base|::BASEDATETIME.GET-DAY-OF-MONTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfWeek()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getDayOfWeek"
                          '|org.joda.time.base|::BASEDATETIME.GET-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-HOUR-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getHourOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getHourOfDay"
                          '|org.joda.time.base|::BASEDATETIME.GET-HOUR-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-MINUTE-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getMinuteOfDay"
                          '|org.joda.time.base|::BASEDATETIME.GET-MINUTE-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-MINUTE-OF-HOUR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfHour()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getMinuteOfHour"
                          '|org.joda.time.base|::BASEDATETIME.GET-MINUTE-OF-HOUR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-SECOND-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getSecondOfDay"
                          '|org.joda.time.base|::BASEDATETIME.GET-SECOND-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-SECOND-OF-MINUTE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfMinute()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getSecondOfMinute"
                          '|org.joda.time.base|::BASEDATETIME.GET-SECOND-OF-MINUTE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-MILLIS-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getMillisOfDay"
                          '|org.joda.time.base|::BASEDATETIME.GET-MILLIS-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-WEEK-OF-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekOfWeekyear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "getWeekOfWeekyear"
                          '|org.joda.time.base|::BASEDATETIME.GET-WEEK-OF-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-GREGORIAN-CALENDAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.GregorianCalendar org.joda.time.base.AbstractDateTime.toGregorianCalendar()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "toGregorianCalendar"
                          '|org.joda.time.base|::BASEDATETIME.TO-GREGORIAN-CALENDAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.get(org.joda.time.DateTimeField)
public int org.joda.time.base.AbstractDateTime.get(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "get"
                          '|org.joda.time.base|::BASEDATETIME.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractInstant.toString(org.joda.time.format.DateTimeFormatter)
public java.lang.String org.joda.time.base.AbstractDateTime.toString()
public java.lang.String org.joda.time.base.AbstractDateTime.toString(java.lang.String,java.util.Locale) throws java.lang.IllegalArgumentException
public java.lang.String org.joda.time.base.AbstractDateTime.toString(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "toString"
                          '|org.joda.time.base|::BASEDATETIME.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.base.AbstractInstant.getZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getZone"
                          '|org.joda.time.base|::BASEDATETIME.GET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-DATE (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Date org.joda.time.base.AbstractInstant.toDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "toDate"
                          '|org.joda.time.base|::BASEDATETIME.TO-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime(org.joda.time.Chronology)
public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime(org.joda.time.DateTimeZone)
public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "toDateTime"
                          '|org.joda.time.base|::BASEDATETIME.TO-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-BEFORE-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBeforeNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isBeforeNow"
                          '|org.joda.time.base|::BASEDATETIME.IS-BEFORE-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-EQUAL-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqualNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isEqualNow"
                          '|org.joda.time.base|::BASEDATETIME.IS-EQUAL-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-MUTABLE-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime(org.joda.time.DateTimeZone)
public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime(org.joda.time.Chronology)
public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "toMutableDateTime"
                          '|org.joda.time.base|::BASEDATETIME.TO-MUTABLE-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-INSTANT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Instant org.joda.time.base.AbstractInstant.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "toInstant"
                          '|org.joda.time.base|::BASEDATETIME.TO-INSTANT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-MUTABLE-DATE-TIME-+ISO+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.base.AbstractInstant.toMutableDateTimeISO()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime|
                          "toMutableDateTimeISO"
                          '|org.joda.time.base|::BASEDATETIME.TO-MUTABLE-DATE-TIME-+ISO+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-AFTER-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfterNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isAfterNow"
                          '|org.joda.time.base|::BASEDATETIME.IS-AFTER-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfter(long)
public boolean org.joda.time.base.AbstractInstant.isAfter(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isAfter"
                          '|org.joda.time.base|::BASEDATETIME.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBefore(long)
public boolean org.joda.time.base.AbstractInstant.isBefore(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isBefore"
                          '|org.joda.time.base|::BASEDATETIME.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.TO-DATE-TIME-+ISO+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInstant.toDateTimeISO()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "toDateTimeISO"
                          '|org.joda.time.base|::BASEDATETIME.TO-DATE-TIME-+ISO+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqual(long)
public boolean org.joda.time.base.AbstractInstant.isEqual(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isEqual"
                          '|org.joda.time.base|::BASEDATETIME.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "equals"
                          '|org.joda.time.base|::BASEDATETIME.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "hashCode"
                          '|org.joda.time.base|::BASEDATETIME.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInstant.compareTo(org.joda.time.ReadableInstant)
public int org.joda.time.base.AbstractInstant.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "compareTo"
                          '|org.joda.time.base|::BASEDATETIME.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isSupported(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "isSupported"
                          '|org.joda.time.base|::BASEDATETIME.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "wait"
                          '|org.joda.time.base|::BASEDATETIME.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "getClass"
                          '|org.joda.time.base|::BASEDATETIME.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "notify"
                          '|org.joda.time.base|::BASEDATETIME.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDateTime| "notifyAll"
                          '|org.joda.time.base|::BASEDATETIME.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.AFTER-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isAfterNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "afterNow"
                       '|org.joda.time.base|::BASEDATETIME.AFTERNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.BEFORE-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isBeforeNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "beforeNow"
                       '|org.joda.time.base|::BASEDATETIME.BEFORENOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.CENTURY-OF-ERA-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getCenturyOfEra()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "centuryOfEra"
                       '|org.joda.time.base|::BASEDATETIME.CENTURYOFERA-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.CHRONOLOGY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.base.BaseDateTime.getChronology()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "chronology"
                       '|org.joda.time.base|::BASEDATETIME.CHRONOLOGY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "class"
                       '|org.joda.time.base|::BASEDATETIME.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.DAY-OF-MONTH-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfMonth()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "dayOfMonth"
                       '|org.joda.time.base|::BASEDATETIME.DAYOFMONTH-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.DAY-OF-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfWeek()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "dayOfWeek"
                       '|org.joda.time.base|::BASEDATETIME.DAYOFWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.DAY-OF-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getDayOfYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "dayOfYear"
                       '|org.joda.time.base|::BASEDATETIME.DAYOFYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.EQUAL-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInstant.isEqualNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "equalNow"
                       '|org.joda.time.base|::BASEDATETIME.EQUALNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.ERA-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getEra()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "era"
                       '|org.joda.time.base|::BASEDATETIME.ERA-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.HOUR-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getHourOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "hourOfDay"
                       '|org.joda.time.base|::BASEDATETIME.HOUROFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseDateTime.getMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "millis"
                       '|org.joda.time.base|::BASEDATETIME.MILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.MILLIS-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "millisOfDay"
                       '|org.joda.time.base|::BASEDATETIME.MILLISOFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.MILLIS-OF-SECOND-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMillisOfSecond()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "millisOfSecond"
                       '|org.joda.time.base|::BASEDATETIME.MILLISOFSECOND-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.MINUTE-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "minuteOfDay"
                       '|org.joda.time.base|::BASEDATETIME.MINUTEOFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.MINUTE-OF-HOUR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMinuteOfHour()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "minuteOfHour"
                       '|org.joda.time.base|::BASEDATETIME.MINUTEOFHOUR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.MONTH-OF-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getMonthOfYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "monthOfYear"
                       '|org.joda.time.base|::BASEDATETIME.MONTHOFYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.SECOND-OF-DAY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfDay()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "secondOfDay"
                       '|org.joda.time.base|::BASEDATETIME.SECONDOFDAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.SECOND-OF-MINUTE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getSecondOfMinute()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "secondOfMinute"
                       '|org.joda.time.base|::BASEDATETIME.SECONDOFMINUTE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.WEEK-OF-WEEKYEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekOfWeekyear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "weekOfWeekyear"
                       '|org.joda.time.base|::BASEDATETIME.WEEKOFWEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.WEEKYEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getWeekyear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "weekyear"
                       '|org.joda.time.base|::BASEDATETIME.WEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "year"
                       '|org.joda.time.base|::BASEDATETIME.YEAR-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.YEAR-OF-CENTURY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfCentury()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "yearOfCentury"
                       '|org.joda.time.base|::BASEDATETIME.YEAROFCENTURY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.YEAR-OF-ERA-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDateTime.getYearOfEra()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "yearOfEra"
                       '|org.joda.time.base|::BASEDATETIME.YEAROFERA-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDATETIME.ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.base.AbstractInstant.getZone()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDateTime| "zone"
                       '|org.joda.time.base|::BASEDATETIME.ZONE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASEDURATION.
  '|org.joda.time.base|::|BaseDuration|)
(DEFCLASS |org.joda.time.base|::BASEDURATION.
          (|org.joda.time.base|::ABSTRACTDURATION.
           |org.joda.time|::READABLEDURATION. |java.io|::SERIALIZABLE.)
          NIL)
(DEFUN |org.joda.time.base|::BASEDURATION.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseDuration.getMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "getMillis"
                          '|org.joda.time.base|::BASEDURATION.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.AbstractDuration.toPeriod()
public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriod(org.joda.time.PeriodType,org.joda.time.Chronology)
public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriod(org.joda.time.PeriodType)
public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriod(org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "toPeriod"
                          '|org.joda.time.base|::BASEDURATION.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-PERIOD-FROM
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriodFrom(org.joda.time.ReadableInstant,org.joda.time.PeriodType)
public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriodFrom(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "toPeriodFrom"
                          '|org.joda.time.base|::BASEDURATION.TO-PERIOD-FROM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-PERIOD-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriodTo(org.joda.time.ReadableInstant,org.joda.time.PeriodType)
public org.joda.time.Period org.joda.time.base.BaseDuration.toPeriodTo(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "toPeriodTo"
                          '|org.joda.time.base|::BASEDURATION.TO-PERIOD-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-INTERVAL-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Interval org.joda.time.base.BaseDuration.toIntervalTo(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "toIntervalTo"
                          '|org.joda.time.base|::BASEDURATION.TO-INTERVAL-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-INTERVAL-FROM
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Interval org.joda.time.base.BaseDuration.toIntervalFrom(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration|
                          "toIntervalFrom"
                          '|org.joda.time.base|::BASEDURATION.TO-INTERVAL-FROM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-DURATION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Duration org.joda.time.base.AbstractDuration.toDuration()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "toDuration"
                          '|org.joda.time.base|::BASEDURATION.TO-DURATION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.IS-LONGER-THAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.isLongerThan(org.joda.time.ReadableDuration)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "isLongerThan"
                          '|org.joda.time.base|::BASEDURATION.IS-LONGER-THAN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.IS-SHORTER-THAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.isShorterThan(org.joda.time.ReadableDuration)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "isShorterThan"
                          '|org.joda.time.base|::BASEDURATION.IS-SHORTER-THAN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.isEqual(org.joda.time.ReadableDuration)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "isEqual"
                          '|org.joda.time.base|::BASEDURATION.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractDuration.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "equals"
                          '|org.joda.time.base|::BASEDURATION.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractDuration.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "toString"
                          '|org.joda.time.base|::BASEDURATION.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDuration.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "hashCode"
                          '|org.joda.time.base|::BASEDURATION.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractDuration.compareTo(org.joda.time.ReadableDuration)
public int org.joda.time.base.AbstractDuration.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "compareTo"
                          '|org.joda.time.base|::BASEDURATION.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "wait"
                          '|org.joda.time.base|::BASEDURATION.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "getClass"
                          '|org.joda.time.base|::BASEDURATION.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "notify"
                          '|org.joda.time.base|::BASEDURATION.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseDuration| "notifyAll"
                          '|org.joda.time.base|::BASEDURATION.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDuration| "class"
                       '|org.joda.time.base|::BASEDURATION.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEDURATION.MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseDuration.getMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseDuration| "millis"
                       '|org.joda.time.base|::BASEDURATION.MILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASEINTERVAL.
  '|org.joda.time.base|::|BaseInterval|)
(DEFCLASS |org.joda.time.base|::BASEINTERVAL.
          (|org.joda.time.base|::ABSTRACTINTERVAL.
           |org.joda.time|::READABLEINTERVAL. |java.io|::SERIALIZABLE.)
          NIL)
(DEFUN |org.joda.time.base|::BASEINTERVAL.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.base.BaseInterval.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "getChronology"
                          '|org.joda.time.base|::BASEINTERVAL.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.GET-START-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseInterval.getStartMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval|
                          "getStartMillis"
                          '|org.joda.time.base|::BASEINTERVAL.GET-START-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.GET-END-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseInterval.getEndMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "getEndMillis"
                          '|org.joda.time.base|::BASEINTERVAL.GET-END-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.IS-BEFORE-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isBeforeNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "isBeforeNow"
                          '|org.joda.time.base|::BASEINTERVAL.IS-BEFORE-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.IS-AFTER-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isAfterNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "isAfterNow"
                          '|org.joda.time.base|::BASEINTERVAL.IS-AFTER-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.IS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isAfter(org.joda.time.ReadableInstant)
public boolean org.joda.time.base.AbstractInterval.isAfter(org.joda.time.ReadableInterval)
public boolean org.joda.time.base.AbstractInterval.isAfter(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "isAfter"
                          '|org.joda.time.base|::BASEINTERVAL.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isBefore(org.joda.time.ReadableInterval)
public boolean org.joda.time.base.AbstractInterval.isBefore(long)
public boolean org.joda.time.base.AbstractInterval.isBefore(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "isBefore"
                          '|org.joda.time.base|::BASEINTERVAL.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.TO-DURATION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Duration org.joda.time.base.AbstractInterval.toDuration()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "toDuration"
                          '|org.joda.time.base|::BASEINTERVAL.TO-DURATION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.TO-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.AbstractInterval.toPeriod(org.joda.time.PeriodType)
public org.joda.time.Period org.joda.time.base.AbstractInterval.toPeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "toPeriod"
                          '|org.joda.time.base|::BASEINTERVAL.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.GET-START
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getStart()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "getStart"
                          '|org.joda.time.base|::BASEINTERVAL.GET-START
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.GET-END (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getEnd()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "getEnd"
                          '|org.joda.time.base|::BASEINTERVAL.GET-END
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.CONTAINS-NOW
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.containsNow()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "containsNow"
                          '|org.joda.time.base|::BASEINTERVAL.CONTAINS-NOW
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.OVERLAPS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.overlaps(org.joda.time.ReadableInterval)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "overlaps"
                          '|org.joda.time.base|::BASEINTERVAL.OVERLAPS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.TO-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Interval org.joda.time.base.AbstractInterval.toInterval()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "toInterval"
                          '|org.joda.time.base|::BASEINTERVAL.TO-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.TO-MUTABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableInterval org.joda.time.base.AbstractInterval.toMutableInterval()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval|
                          "toMutableInterval"
                          '|org.joda.time.base|::BASEINTERVAL.TO-MUTABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.TO-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.AbstractInterval.toDurationMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval|
                          "toDurationMillis"
                          '|org.joda.time.base|::BASEINTERVAL.TO-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.IS-EQUAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isEqual(org.joda.time.ReadableInterval)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "isEqual"
                          '|org.joda.time.base|::BASEINTERVAL.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "equals"
                          '|org.joda.time.base|::BASEINTERVAL.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractInterval.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "toString"
                          '|org.joda.time.base|::BASEINTERVAL.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractInterval.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "hashCode"
                          '|org.joda.time.base|::BASEINTERVAL.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.CONTAINS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.contains(org.joda.time.ReadableInterval)
public boolean org.joda.time.base.AbstractInterval.contains(org.joda.time.ReadableInstant)
public boolean org.joda.time.base.AbstractInterval.contains(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "contains"
                          '|org.joda.time.base|::BASEINTERVAL.CONTAINS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "wait"
                          '|org.joda.time.base|::BASEINTERVAL.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "getClass"
                          '|org.joda.time.base|::BASEINTERVAL.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "notify"
                          '|org.joda.time.base|::BASEINTERVAL.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseInterval| "notifyAll"
                          '|org.joda.time.base|::BASEINTERVAL.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.AFTER-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isAfterNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "afterNow"
                       '|org.joda.time.base|::BASEINTERVAL.AFTERNOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.BEFORE-NOW-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractInterval.isBeforeNow()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "beforeNow"
                       '|org.joda.time.base|::BASEINTERVAL.BEFORENOW-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.CHRONOLOGY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.base.BaseInterval.getChronology()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "chronology"
                       '|org.joda.time.base|::BASEINTERVAL.CHRONOLOGY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "class"
                       '|org.joda.time.base|::BASEINTERVAL.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.END-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getEnd()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "end"
                       '|org.joda.time.base|::BASEINTERVAL.END-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.END-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseInterval.getEndMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "endMillis"
                       '|org.joda.time.base|::BASEINTERVAL.ENDMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.START-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractInterval.getStart()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "start"
                       '|org.joda.time.base|::BASEINTERVAL.START-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEINTERVAL.START-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.base.BaseInterval.getStartMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseInterval| "startMillis"
                       '|org.joda.time.base|::BASEINTERVAL.STARTMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASELOCAL.
  '|org.joda.time.base|::|BaseLocal|)
(DEFCLASS |org.joda.time.base|::BASELOCAL.
          (|org.joda.time.base|::ABSTRACTPARTIAL.) NIL)
(DEFUN |org.joda.time.base|::BASELOCAL.TO-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractPartial.toDateTime(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "toDateTime"
                          '|org.joda.time.base|::BASELOCAL.TO-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.IS-AFTER (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isAfter(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "isAfter"
                          '|org.joda.time.base|::BASELOCAL.IS-AFTER FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.IS-BEFORE (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isBefore(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "isBefore"
                          '|org.joda.time.base|::BASELOCAL.IS-BEFORE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-VALUES (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPartial.getValues()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getValues"
                          '|org.joda.time.base|::BASELOCAL.GET-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-FIELD-TYPES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType[] org.joda.time.base.AbstractPartial.getFieldTypes()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getFieldTypes"
                          '|org.joda.time.base|::BASELOCAL.GET-FIELD-TYPES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.IS-EQUAL (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isEqual(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "isEqual"
                          '|org.joda.time.base|::BASELOCAL.IS-EQUAL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.get(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "get"
                          '|org.joda.time.base|::BASELOCAL.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "equals"
                          '|org.joda.time.base|::BASELOCAL.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
public java.lang.String org.joda.time.base.AbstractPartial.toString(org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "toString"
                          '|org.joda.time.base|::BASELOCAL.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "hashCode"
                          '|org.joda.time.base|::BASELOCAL.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.COMPARE-TO (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.compareTo(java.lang.Object)
public int org.joda.time.base.AbstractPartial.compareTo(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "compareTo"
                          '|org.joda.time.base|::BASELOCAL.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.INDEX-OF (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.indexOf(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "indexOf"
                          '|org.joda.time.base|::BASELOCAL.INDEX-OF FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-FIELDS (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField[] org.joda.time.base.AbstractPartial.getFields()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getFields"
                          '|org.joda.time.base|::BASELOCAL.GET-FIELDS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-FIELD (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField org.joda.time.base.AbstractPartial.getField(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getField"
                          '|org.joda.time.base|::BASELOCAL.GET-FIELD FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.base.AbstractPartial.getFieldType(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getFieldType"
                          '|org.joda.time.base|::BASELOCAL.GET-FIELD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isSupported(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "isSupported"
                          '|org.joda.time.base|::BASELOCAL.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "wait"
                          '|org.joda.time.base|::BASELOCAL.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getClass"
                          '|org.joda.time.base|::BASELOCAL.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "notify"
                          '|org.joda.time.base|::BASELOCAL.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "notifyAll"
                          '|org.joda.time.base|::BASELOCAL.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.ReadablePartial.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getChronology"
                          '|org.joda.time.base|::BASELOCAL.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.GET-VALUE (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.ReadablePartial.getValue(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "getValue"
                          '|org.joda.time.base|::BASELOCAL.GET-VALUE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.SIZE (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.ReadablePartial.size()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseLocal| "size"
                          '|org.joda.time.base|::BASELOCAL.SIZE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseLocal| "class"
                       '|org.joda.time.base|::BASELOCAL.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.FIELD-TYPES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType[] org.joda.time.base.AbstractPartial.getFieldTypes()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseLocal| "fieldTypes"
                       '|org.joda.time.base|::BASELOCAL.FIELDTYPES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.FIELDS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField[] org.joda.time.base.AbstractPartial.getFields()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseLocal| "fields"
                       '|org.joda.time.base|::BASELOCAL.FIELDS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASELOCAL.VALUES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPartial.getValues()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseLocal| "values"
                       '|org.joda.time.base|::BASELOCAL.VALUES-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASEPARTIAL.
  '|org.joda.time.base|::|BasePartial|)
(DEFCLASS |org.joda.time.base|::BASEPARTIAL.
          (|org.joda.time.base|::ABSTRACTPARTIAL.
           |org.joda.time|::READABLEPARTIAL. |java.io|::SERIALIZABLE.)
          NIL)
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.base.BasePartial.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getChronology"
                          '|org.joda.time.base|::BASEPARTIAL.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.BasePartial.getValues()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getValues"
                          '|org.joda.time.base|::BASEPARTIAL.GET-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
public java.lang.String org.joda.time.base.AbstractPartial.toString(org.joda.time.format.DateTimeFormatter)
public java.lang.String org.joda.time.base.BasePartial.toString(java.lang.String,java.util.Locale) throws java.lang.IllegalArgumentException
public java.lang.String org.joda.time.base.BasePartial.toString(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "toString"
                          '|org.joda.time.base|::BASEPARTIAL.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BasePartial.getValue(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getValue"
                          '|org.joda.time.base|::BASEPARTIAL.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.TO-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.base.AbstractPartial.toDateTime(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "toDateTime"
                          '|org.joda.time.base|::BASEPARTIAL.TO-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.IS-AFTER (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isAfter(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "isAfter"
                          '|org.joda.time.base|::BASEPARTIAL.IS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.IS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isBefore(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "isBefore"
                          '|org.joda.time.base|::BASEPARTIAL.IS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-FIELD-TYPES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType[] org.joda.time.base.AbstractPartial.getFieldTypes()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getFieldTypes"
                          '|org.joda.time.base|::BASEPARTIAL.GET-FIELD-TYPES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.IS-EQUAL (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isEqual(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "isEqual"
                          '|org.joda.time.base|::BASEPARTIAL.IS-EQUAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.get(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "get"
                          '|org.joda.time.base|::BASEPARTIAL.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "equals"
                          '|org.joda.time.base|::BASEPARTIAL.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "hashCode"
                          '|org.joda.time.base|::BASEPARTIAL.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.compareTo(java.lang.Object)
public int org.joda.time.base.AbstractPartial.compareTo(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "compareTo"
                          '|org.joda.time.base|::BASEPARTIAL.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.INDEX-OF (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPartial.indexOf(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "indexOf"
                          '|org.joda.time.base|::BASEPARTIAL.INDEX-OF
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-FIELDS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField[] org.joda.time.base.AbstractPartial.getFields()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getFields"
                          '|org.joda.time.base|::BASEPARTIAL.GET-FIELDS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField org.joda.time.base.AbstractPartial.getField(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getField"
                          '|org.joda.time.base|::BASEPARTIAL.GET-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.base.AbstractPartial.getFieldType(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getFieldType"
                          '|org.joda.time.base|::BASEPARTIAL.GET-FIELD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPartial.isSupported(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "isSupported"
                          '|org.joda.time.base|::BASEPARTIAL.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "wait"
                          '|org.joda.time.base|::BASEPARTIAL.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "getClass"
                          '|org.joda.time.base|::BASEPARTIAL.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "notify"
                          '|org.joda.time.base|::BASEPARTIAL.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "notifyAll"
                          '|org.joda.time.base|::BASEPARTIAL.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.SIZE (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.ReadablePartial.size()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePartial| "size"
                          '|org.joda.time.base|::BASEPARTIAL.SIZE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.CHRONOLOGY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.base.BasePartial.getChronology()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePartial| "chronology"
                       '|org.joda.time.base|::BASEPARTIAL.CHRONOLOGY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePartial| "class"
                       '|org.joda.time.base|::BASEPARTIAL.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.FIELD-TYPES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType[] org.joda.time.base.AbstractPartial.getFieldTypes()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePartial| "fieldTypes"
                       '|org.joda.time.base|::BASEPARTIAL.FIELDTYPES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.FIELDS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeField[] org.joda.time.base.AbstractPartial.getFields()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePartial| "fields"
                       '|org.joda.time.base|::BASEPARTIAL.FIELDS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPARTIAL.VALUES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.BasePartial.getValues()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePartial| "values"
                       '|org.joda.time.base|::BASEPARTIAL.VALUES-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASEPERIOD.
  '|org.joda.time.base|::|BasePeriod|)
(DEFCLASS |org.joda.time.base|::BASEPERIOD.
          (|org.joda.time.base|::ABSTRACTPERIOD.
           |org.joda.time|::READABLEPERIOD. |java.io|::SERIALIZABLE.)
          NIL)
(DEFUN |org.joda.time.base|::BASEPERIOD.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.base.BasePeriod.getPeriodType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "getPeriodType"
                          '|org.joda.time.base|::BASEPERIOD.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.TO-DURATION-FROM
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Duration org.joda.time.base.BasePeriod.toDurationFrom(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "toDurationFrom"
                          '|org.joda.time.base|::BASEPERIOD.TO-DURATION-FROM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.TO-DURATION-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Duration org.joda.time.base.BasePeriod.toDurationTo(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "toDurationTo"
                          '|org.joda.time.base|::BASEPERIOD.TO-DURATION-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.GET-VALUE (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BasePeriod.getValue(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "getValue"
                          '|org.joda.time.base|::BASEPERIOD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.TO-PERIOD (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.AbstractPeriod.toPeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "toPeriod"
                          '|org.joda.time.base|::BASEPERIOD.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.GET-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPeriod.getValues()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "getValues"
                          '|org.joda.time.base|::BASEPERIOD.GET-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.TO-MUTABLE-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutablePeriod org.joda.time.base.AbstractPeriod.toMutablePeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "toMutablePeriod"
                          '|org.joda.time.base|::BASEPERIOD.TO-MUTABLE-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.GET-FIELD-TYPES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType[] org.joda.time.base.AbstractPeriod.getFieldTypes()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "getFieldTypes"
                          '|org.joda.time.base|::BASEPERIOD.GET-FIELD-TYPES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.get(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "get"
                          '|org.joda.time.base|::BASEPERIOD.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPeriod.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "equals"
                          '|org.joda.time.base|::BASEPERIOD.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.base.AbstractPeriod.toString(org.joda.time.format.PeriodFormatter)
public java.lang.String org.joda.time.base.AbstractPeriod.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "toString"
                          '|org.joda.time.base|::BASEPERIOD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "hashCode"
                          '|org.joda.time.base|::BASEPERIOD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.INDEX-OF (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.indexOf(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "indexOf"
                          '|org.joda.time.base|::BASEPERIOD.INDEX-OF FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.SIZE (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.AbstractPeriod.size()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "size"
                          '|org.joda.time.base|::BASEPERIOD.SIZE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType org.joda.time.base.AbstractPeriod.getFieldType(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "getFieldType"
                          '|org.joda.time.base|::BASEPERIOD.GET-FIELD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.AbstractPeriod.isSupported(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "isSupported"
                          '|org.joda.time.base|::BASEPERIOD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "wait"
                          '|org.joda.time.base|::BASEPERIOD.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "getClass"
                          '|org.joda.time.base|::BASEPERIOD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "notify"
                          '|org.joda.time.base|::BASEPERIOD.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BasePeriod| "notifyAll"
                          '|org.joda.time.base|::BASEPERIOD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePeriod| "class"
                       '|org.joda.time.base|::BASEPERIOD.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.FIELD-TYPES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType[] org.joda.time.base.AbstractPeriod.getFieldTypes()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePeriod| "fieldTypes"
                       '|org.joda.time.base|::BASEPERIOD.FIELDTYPES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.PERIOD-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.base.BasePeriod.getPeriodType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePeriod| "periodType"
                       '|org.joda.time.base|::BASEPERIOD.PERIODTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASEPERIOD.VALUES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.base.AbstractPeriod.getValues()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BasePeriod| "values"
                       '|org.joda.time.base|::BASEPERIOD.VALUES-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.base|::BASESINGLEFIELDPERIOD.
  '|org.joda.time.base|::|BaseSingleFieldPeriod|)
(DEFCLASS |org.joda.time.base|::BASESINGLEFIELDPERIOD.
          (|org.joda.time|::READABLEPERIOD. |java.lang|::COMPARABLE.
           |java.io|::SERIALIZABLE. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.base.BaseSingleFieldPeriod.toPeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "toPeriod"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.PeriodType org.joda.time.base.BaseSingleFieldPeriod.getPeriodType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "getPeriodType"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-MUTABLE-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutablePeriod org.joda.time.base.BaseSingleFieldPeriod.toMutablePeriod()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "toMutablePeriod"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-MUTABLE-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BaseSingleFieldPeriod.get(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod| "get"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.BaseSingleFieldPeriod.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "equals"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BaseSingleFieldPeriod.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "hashCode"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BaseSingleFieldPeriod.compareTo(java.lang.Object)
public int org.joda.time.base.BaseSingleFieldPeriod.compareTo(org.joda.time.base.BaseSingleFieldPeriod)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "compareTo"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BaseSingleFieldPeriod.getValue(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "getValue"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.SIZE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.base.BaseSingleFieldPeriod.size()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod| "size"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.SIZE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationFieldType org.joda.time.base.BaseSingleFieldPeriod.getFieldType()
public org.joda.time.DurationFieldType org.joda.time.base.BaseSingleFieldPeriod.getFieldType(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "getFieldType"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-FIELD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.base.BaseSingleFieldPeriod.isSupported(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "isSupported"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod| "wait"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "toString"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "getClass"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "notify"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.base|::|BaseSingleFieldPeriod|
                          "notifyAll"
                          '|org.joda.time.base|::BASESINGLEFIELDPERIOD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseSingleFieldPeriod| "class"
                       '|org.joda.time.base|::BASESINGLEFIELDPERIOD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.FIELD-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationFieldType org.joda.time.base.BaseSingleFieldPeriod.getFieldType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseSingleFieldPeriod|
                       "fieldType"
                       '|org.joda.time.base|::BASESINGLEFIELDPERIOD.FIELDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.base|::BASESINGLEFIELDPERIOD.PERIOD-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.PeriodType org.joda.time.base.BaseSingleFieldPeriod.getPeriodType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.base|::|BaseSingleFieldPeriod|
                       "periodType"
                       '|org.joda.time.base|::BASESINGLEFIELDPERIOD.PERIODTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(eval-when (:load-toplevel)(export '(|org.joda.time.base|::BASESINGLEFIELDPERIOD.VALUE-PROP
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.PERIOD-TYPE-PROP
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.FIELD-TYPE-PROP
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.CLASS-PROP
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.NOTIFY-ALL
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.NOTIFY
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-CLASS
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-STRING
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.WAIT
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.IS-SUPPORTED
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-FIELD-TYPE
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.SIZE
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-VALUE
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.COMPARE-TO
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.HASH-CODE
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.EQUALS
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-MUTABLE-PERIOD
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.GET-PERIOD-TYPE
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.TO-PERIOD
                                     |org.joda.time.base|::BASESINGLEFIELDPERIOD.
                                     |org.joda.time.base|::BASEPERIOD.VALUES-PROP
                                     |org.joda.time.base|::BASEPERIOD.VALUE-PROP
                                     |org.joda.time.base|::BASEPERIOD.PERIOD-TYPE-PROP
                                     |org.joda.time.base|::BASEPERIOD.FIELD-TYPES-PROP
                                     |org.joda.time.base|::BASEPERIOD.FIELD-TYPE-PROP
                                     |org.joda.time.base|::BASEPERIOD.CLASS-PROP
                                     |org.joda.time.base|::BASEPERIOD.NOTIFY-ALL
                                     |org.joda.time.base|::BASEPERIOD.NOTIFY
                                     |org.joda.time.base|::BASEPERIOD.GET-CLASS
                                     |org.joda.time.base|::BASEPERIOD.WAIT
                                     |org.joda.time.base|::BASEPERIOD.IS-SUPPORTED
                                     |org.joda.time.base|::BASEPERIOD.GET-FIELD-TYPE
                                     |org.joda.time.base|::BASEPERIOD.SIZE
                                     |org.joda.time.base|::BASEPERIOD.INDEX-OF
                                     |org.joda.time.base|::BASEPERIOD.HASH-CODE
                                     |org.joda.time.base|::BASEPERIOD.TO-STRING
                                     |org.joda.time.base|::BASEPERIOD.EQUALS
                                     |org.joda.time.base|::BASEPERIOD.GET
                                     |org.joda.time.base|::BASEPERIOD.GET-FIELD-TYPES
                                     |org.joda.time.base|::BASEPERIOD.TO-MUTABLE-PERIOD
                                     |org.joda.time.base|::BASEPERIOD.GET-VALUES
                                     |org.joda.time.base|::BASEPERIOD.TO-PERIOD
                                     |org.joda.time.base|::BASEPERIOD.GET-VALUE
                                     |org.joda.time.base|::BASEPERIOD.TO-DURATION-TO
                                     |org.joda.time.base|::BASEPERIOD.TO-DURATION-FROM
                                     |org.joda.time.base|::BASEPERIOD.GET-PERIOD-TYPE
                                     |org.joda.time.base|::BASEPERIOD.
                                     |org.joda.time.base|::BASEPARTIAL.VALUES-PROP
                                     |org.joda.time.base|::BASEPARTIAL.VALUE-PROP
                                     |org.joda.time.base|::BASEPARTIAL.FIELDS-PROP
                                     |org.joda.time.base|::BASEPARTIAL.FIELD-TYPES-PROP
                                     |org.joda.time.base|::BASEPARTIAL.FIELD-TYPE-PROP
                                     |org.joda.time.base|::BASEPARTIAL.FIELD-PROP
                                     |org.joda.time.base|::BASEPARTIAL.CLASS-PROP
                                     |org.joda.time.base|::BASEPARTIAL.CHRONOLOGY-PROP
                                     |org.joda.time.base|::BASEPARTIAL.SIZE
                                     |org.joda.time.base|::BASEPARTIAL.NOTIFY-ALL
                                     |org.joda.time.base|::BASEPARTIAL.NOTIFY
                                     |org.joda.time.base|::BASEPARTIAL.GET-CLASS
                                     |org.joda.time.base|::BASEPARTIAL.WAIT
                                     |org.joda.time.base|::BASEPARTIAL.IS-SUPPORTED
                                     |org.joda.time.base|::BASEPARTIAL.GET-FIELD-TYPE
                                     |org.joda.time.base|::BASEPARTIAL.GET-FIELD
                                     |org.joda.time.base|::BASEPARTIAL.GET-FIELDS
                                     |org.joda.time.base|::BASEPARTIAL.INDEX-OF
                                     |org.joda.time.base|::BASEPARTIAL.COMPARE-TO
                                     |org.joda.time.base|::BASEPARTIAL.HASH-CODE
                                     |org.joda.time.base|::BASEPARTIAL.EQUALS
                                     |org.joda.time.base|::BASEPARTIAL.GET
                                     |org.joda.time.base|::BASEPARTIAL.IS-EQUAL
                                     |org.joda.time.base|::BASEPARTIAL.GET-FIELD-TYPES
                                     |org.joda.time.base|::BASEPARTIAL.IS-BEFORE
                                     |org.joda.time.base|::BASEPARTIAL.IS-AFTER
                                     |org.joda.time.base|::BASEPARTIAL.TO-DATE-TIME
                                     |org.joda.time.base|::BASEPARTIAL.GET-VALUE
                                     |org.joda.time.base|::BASEPARTIAL.TO-STRING
                                     |org.joda.time.base|::BASEPARTIAL.GET-VALUES
                                     |org.joda.time.base|::BASEPARTIAL.GET-CHRONOLOGY
                                     |org.joda.time.base|::BASEPARTIAL.
                                     |org.joda.time.base|::BASELOCAL.VALUES-PROP
                                     |org.joda.time.base|::BASELOCAL.FIELDS-PROP
                                     |org.joda.time.base|::BASELOCAL.FIELD-TYPES-PROP
                                     |org.joda.time.base|::BASELOCAL.FIELD-TYPE-PROP
                                     |org.joda.time.base|::BASELOCAL.FIELD-PROP
                                     |org.joda.time.base|::BASELOCAL.CLASS-PROP
                                     |org.joda.time.base|::BASELOCAL.SIZE
                                     |org.joda.time.base|::BASELOCAL.GET-VALUE
                                     |org.joda.time.base|::BASELOCAL.GET-CHRONOLOGY
                                     |org.joda.time.base|::BASELOCAL.NOTIFY-ALL
                                     |org.joda.time.base|::BASELOCAL.NOTIFY
                                     |org.joda.time.base|::BASELOCAL.GET-CLASS
                                     |org.joda.time.base|::BASELOCAL.WAIT
                                     |org.joda.time.base|::BASELOCAL.IS-SUPPORTED
                                     |org.joda.time.base|::BASELOCAL.GET-FIELD-TYPE
                                     |org.joda.time.base|::BASELOCAL.GET-FIELD
                                     |org.joda.time.base|::BASELOCAL.GET-FIELDS
                                     |org.joda.time.base|::BASELOCAL.INDEX-OF
                                     |org.joda.time.base|::BASELOCAL.COMPARE-TO
                                     |org.joda.time.base|::BASELOCAL.HASH-CODE
                                     |org.joda.time.base|::BASELOCAL.TO-STRING
                                     |org.joda.time.base|::BASELOCAL.EQUALS
                                     |org.joda.time.base|::BASELOCAL.GET
                                     |org.joda.time.base|::BASELOCAL.IS-EQUAL
                                     |org.joda.time.base|::BASELOCAL.GET-FIELD-TYPES
                                     |org.joda.time.base|::BASELOCAL.GET-VALUES
                                     |org.joda.time.base|::BASELOCAL.IS-BEFORE
                                     |org.joda.time.base|::BASELOCAL.IS-AFTER
                                     |org.joda.time.base|::BASELOCAL.TO-DATE-TIME
                                     |org.joda.time.base|::BASELOCAL.
                                     |org.joda.time.base|::BASEINTERVAL.START-MILLIS-PROP
                                     |org.joda.time.base|::BASEINTERVAL.START-PROP
                                     |org.joda.time.base|::BASEINTERVAL.END-MILLIS-PROP
                                     |org.joda.time.base|::BASEINTERVAL.END-PROP
                                     |org.joda.time.base|::BASEINTERVAL.CLASS-PROP
                                     |org.joda.time.base|::BASEINTERVAL.CHRONOLOGY-PROP
                                     |org.joda.time.base|::BASEINTERVAL.BEFORE-NOW-PROP
                                     |org.joda.time.base|::BASEINTERVAL.AFTER-NOW-PROP
                                     |org.joda.time.base|::BASEINTERVAL.NOTIFY-ALL
                                     |org.joda.time.base|::BASEINTERVAL.NOTIFY
                                     |org.joda.time.base|::BASEINTERVAL.GET-CLASS
                                     |org.joda.time.base|::BASEINTERVAL.WAIT
                                     |org.joda.time.base|::BASEINTERVAL.CONTAINS
                                     |org.joda.time.base|::BASEINTERVAL.HASH-CODE
                                     |org.joda.time.base|::BASEINTERVAL.TO-STRING
                                     |org.joda.time.base|::BASEINTERVAL.EQUALS
                                     |org.joda.time.base|::BASEINTERVAL.IS-EQUAL
                                     |org.joda.time.base|::BASEINTERVAL.TO-DURATION-MILLIS
                                     |org.joda.time.base|::BASEINTERVAL.TO-MUTABLE-INTERVAL
                                     |org.joda.time.base|::BASEINTERVAL.TO-INTERVAL
                                     |org.joda.time.base|::BASEINTERVAL.OVERLAPS
                                     |org.joda.time.base|::BASEINTERVAL.CONTAINS-NOW
                                     |org.joda.time.base|::BASEINTERVAL.GET-END
                                     |org.joda.time.base|::BASEINTERVAL.GET-START
                                     |org.joda.time.base|::BASEINTERVAL.TO-PERIOD
                                     |org.joda.time.base|::BASEINTERVAL.TO-DURATION
                                     |org.joda.time.base|::BASEINTERVAL.IS-BEFORE
                                     |org.joda.time.base|::BASEINTERVAL.IS-AFTER
                                     |org.joda.time.base|::BASEINTERVAL.IS-AFTER-NOW
                                     |org.joda.time.base|::BASEINTERVAL.IS-BEFORE-NOW
                                     |org.joda.time.base|::BASEINTERVAL.GET-END-MILLIS
                                     |org.joda.time.base|::BASEINTERVAL.GET-START-MILLIS
                                     |org.joda.time.base|::BASEINTERVAL.GET-CHRONOLOGY
                                     |org.joda.time.base|::BASEINTERVAL.
                                     |org.joda.time.base|::BASEDURATION.MILLIS-PROP
                                     |org.joda.time.base|::BASEDURATION.CLASS-PROP
                                     |org.joda.time.base|::BASEDURATION.NOTIFY-ALL
                                     |org.joda.time.base|::BASEDURATION.NOTIFY
                                     |org.joda.time.base|::BASEDURATION.GET-CLASS
                                     |org.joda.time.base|::BASEDURATION.WAIT
                                     |org.joda.time.base|::BASEDURATION.COMPARE-TO
                                     |org.joda.time.base|::BASEDURATION.HASH-CODE
                                     |org.joda.time.base|::BASEDURATION.TO-STRING
                                     |org.joda.time.base|::BASEDURATION.EQUALS
                                     |org.joda.time.base|::BASEDURATION.IS-EQUAL
                                     |org.joda.time.base|::BASEDURATION.IS-SHORTER-THAN
                                     |org.joda.time.base|::BASEDURATION.IS-LONGER-THAN
                                     |org.joda.time.base|::BASEDURATION.TO-DURATION
                                     |org.joda.time.base|::BASEDURATION.TO-INTERVAL-FROM
                                     |org.joda.time.base|::BASEDURATION.TO-INTERVAL-TO
                                     |org.joda.time.base|::BASEDURATION.TO-PERIOD-TO
                                     |org.joda.time.base|::BASEDURATION.TO-PERIOD-FROM
                                     |org.joda.time.base|::BASEDURATION.TO-PERIOD
                                     |org.joda.time.base|::BASEDURATION.GET-MILLIS
                                     |org.joda.time.base|::BASEDURATION.
                                     |org.joda.time.base|::BASEDATETIME.ZONE-PROP
                                     |org.joda.time.base|::BASEDATETIME.YEAR-OF-ERA-PROP
                                     |org.joda.time.base|::BASEDATETIME.YEAR-OF-CENTURY-PROP
                                     |org.joda.time.base|::BASEDATETIME.YEAR-PROP
                                     |org.joda.time.base|::BASEDATETIME.WEEKYEAR-PROP
                                     |org.joda.time.base|::BASEDATETIME.WEEK-OF-WEEKYEAR-PROP
                                     |org.joda.time.base|::BASEDATETIME.SECOND-OF-MINUTE-PROP
                                     |org.joda.time.base|::BASEDATETIME.SECOND-OF-DAY-PROP
                                     |org.joda.time.base|::BASEDATETIME.MONTH-OF-YEAR-PROP
                                     |org.joda.time.base|::BASEDATETIME.MINUTE-OF-HOUR-PROP
                                     |org.joda.time.base|::BASEDATETIME.MINUTE-OF-DAY-PROP
                                     |org.joda.time.base|::BASEDATETIME.MILLIS-OF-SECOND-PROP
                                     |org.joda.time.base|::BASEDATETIME.MILLIS-OF-DAY-PROP
                                     |org.joda.time.base|::BASEDATETIME.MILLIS-PROP
                                     |org.joda.time.base|::BASEDATETIME.HOUR-OF-DAY-PROP
                                     |org.joda.time.base|::BASEDATETIME.ERA-PROP
                                     |org.joda.time.base|::BASEDATETIME.EQUAL-NOW-PROP
                                     |org.joda.time.base|::BASEDATETIME.DAY-OF-YEAR-PROP
                                     |org.joda.time.base|::BASEDATETIME.DAY-OF-WEEK-PROP
                                     |org.joda.time.base|::BASEDATETIME.DAY-OF-MONTH-PROP
                                     |org.joda.time.base|::BASEDATETIME.CLASS-PROP
                                     |org.joda.time.base|::BASEDATETIME.CHRONOLOGY-PROP
                                     |org.joda.time.base|::BASEDATETIME.CENTURY-OF-ERA-PROP
                                     |org.joda.time.base|::BASEDATETIME.BEFORE-NOW-PROP
                                     |org.joda.time.base|::BASEDATETIME.AFTER-NOW-PROP
                                     |org.joda.time.base|::BASEDATETIME.NOTIFY-ALL
                                     |org.joda.time.base|::BASEDATETIME.NOTIFY
                                     |org.joda.time.base|::BASEDATETIME.GET-CLASS
                                     |org.joda.time.base|::BASEDATETIME.WAIT
                                     |org.joda.time.base|::BASEDATETIME.IS-SUPPORTED
                                     |org.joda.time.base|::BASEDATETIME.COMPARE-TO
                                     |org.joda.time.base|::BASEDATETIME.HASH-CODE
                                     |org.joda.time.base|::BASEDATETIME.EQUALS
                                     |org.joda.time.base|::BASEDATETIME.IS-EQUAL
                                     |org.joda.time.base|::BASEDATETIME.TO-DATE-TIME-+ISO+
                                     |org.joda.time.base|::BASEDATETIME.IS-BEFORE
                                     |org.joda.time.base|::BASEDATETIME.IS-AFTER
                                     |org.joda.time.base|::BASEDATETIME.IS-AFTER-NOW
                                     |org.joda.time.base|::BASEDATETIME.TO-MUTABLE-DATE-TIME-+ISO+
                                     |org.joda.time.base|::BASEDATETIME.TO-INSTANT
                                     |org.joda.time.base|::BASEDATETIME.TO-MUTABLE-DATE-TIME
                                     |org.joda.time.base|::BASEDATETIME.IS-EQUAL-NOW
                                     |org.joda.time.base|::BASEDATETIME.IS-BEFORE-NOW
                                     |org.joda.time.base|::BASEDATETIME.TO-DATE-TIME
                                     |org.joda.time.base|::BASEDATETIME.TO-DATE
                                     |org.joda.time.base|::BASEDATETIME.GET-ZONE
                                     |org.joda.time.base|::BASEDATETIME.TO-STRING
                                     |org.joda.time.base|::BASEDATETIME.GET
                                     |org.joda.time.base|::BASEDATETIME.TO-GREGORIAN-CALENDAR
                                     |org.joda.time.base|::BASEDATETIME.GET-WEEK-OF-WEEKYEAR
                                     |org.joda.time.base|::BASEDATETIME.GET-MILLIS-OF-DAY
                                     |org.joda.time.base|::BASEDATETIME.GET-SECOND-OF-MINUTE
                                     |org.joda.time.base|::BASEDATETIME.GET-SECOND-OF-DAY
                                     |org.joda.time.base|::BASEDATETIME.GET-MINUTE-OF-HOUR
                                     |org.joda.time.base|::BASEDATETIME.GET-MINUTE-OF-DAY
                                     |org.joda.time.base|::BASEDATETIME.GET-HOUR-OF-DAY
                                     |org.joda.time.base|::BASEDATETIME.GET-DAY-OF-WEEK
                                     |org.joda.time.base|::BASEDATETIME.GET-DAY-OF-MONTH
                                     |org.joda.time.base|::BASEDATETIME.GET-DAY-OF-YEAR
                                     |org.joda.time.base|::BASEDATETIME.GET-MONTH-OF-YEAR
                                     |org.joda.time.base|::BASEDATETIME.GET-WEEKYEAR
                                     |org.joda.time.base|::BASEDATETIME.GET-YEAR
                                     |org.joda.time.base|::BASEDATETIME.GET-YEAR-OF-CENTURY
                                     |org.joda.time.base|::BASEDATETIME.GET-YEAR-OF-ERA
                                     |org.joda.time.base|::BASEDATETIME.GET-CENTURY-OF-ERA
                                     |org.joda.time.base|::BASEDATETIME.GET-ERA
                                     |org.joda.time.base|::BASEDATETIME.TO-CALENDAR
                                     |org.joda.time.base|::BASEDATETIME.GET-MILLIS-OF-SECOND
                                     |org.joda.time.base|::BASEDATETIME.GET-MILLIS
                                     |org.joda.time.base|::BASEDATETIME.GET-CHRONOLOGY
                                     |org.joda.time.base|::BASEDATETIME.NEW
                                     |org.joda.time.base|::BASEDATETIME.
                                     |org.joda.time.base|::ABSTRACTPERIOD.VALUES-PROP
                                     |org.joda.time.base|::ABSTRACTPERIOD.FIELD-TYPES-PROP
                                     |org.joda.time.base|::ABSTRACTPERIOD.FIELD-TYPE-PROP
                                     |org.joda.time.base|::ABSTRACTPERIOD.CLASS-PROP
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET-VALUE
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET-PERIOD-TYPE
                                     |org.joda.time.base|::ABSTRACTPERIOD.NOTIFY-ALL
                                     |org.joda.time.base|::ABSTRACTPERIOD.NOTIFY
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET-CLASS
                                     |org.joda.time.base|::ABSTRACTPERIOD.WAIT
                                     |org.joda.time.base|::ABSTRACTPERIOD.IS-SUPPORTED
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET-FIELD-TYPE
                                     |org.joda.time.base|::ABSTRACTPERIOD.SIZE
                                     |org.joda.time.base|::ABSTRACTPERIOD.INDEX-OF
                                     |org.joda.time.base|::ABSTRACTPERIOD.HASH-CODE
                                     |org.joda.time.base|::ABSTRACTPERIOD.TO-STRING
                                     |org.joda.time.base|::ABSTRACTPERIOD.EQUALS
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET-FIELD-TYPES
                                     |org.joda.time.base|::ABSTRACTPERIOD.TO-MUTABLE-PERIOD
                                     |org.joda.time.base|::ABSTRACTPERIOD.GET-VALUES
                                     |org.joda.time.base|::ABSTRACTPERIOD.TO-PERIOD
                                     |org.joda.time.base|::ABSTRACTPERIOD.
                                     |org.joda.time.base|::ABSTRACTPARTIAL.VALUES-PROP
                                     |org.joda.time.base|::ABSTRACTPARTIAL.FIELDS-PROP
                                     |org.joda.time.base|::ABSTRACTPARTIAL.FIELD-TYPES-PROP
                                     |org.joda.time.base|::ABSTRACTPARTIAL.FIELD-TYPE-PROP
                                     |org.joda.time.base|::ABSTRACTPARTIAL.FIELD-PROP
                                     |org.joda.time.base|::ABSTRACTPARTIAL.CLASS-PROP
                                     |org.joda.time.base|::ABSTRACTPARTIAL.SIZE
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-VALUE
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-CHRONOLOGY
                                     |org.joda.time.base|::ABSTRACTPARTIAL.NOTIFY-ALL
                                     |org.joda.time.base|::ABSTRACTPARTIAL.NOTIFY
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-CLASS
                                     |org.joda.time.base|::ABSTRACTPARTIAL.WAIT
                                     |org.joda.time.base|::ABSTRACTPARTIAL.IS-SUPPORTED
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD-TYPE
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELDS
                                     |org.joda.time.base|::ABSTRACTPARTIAL.INDEX-OF
                                     |org.joda.time.base|::ABSTRACTPARTIAL.COMPARE-TO
                                     |org.joda.time.base|::ABSTRACTPARTIAL.HASH-CODE
                                     |org.joda.time.base|::ABSTRACTPARTIAL.TO-STRING
                                     |org.joda.time.base|::ABSTRACTPARTIAL.EQUALS
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET
                                     |org.joda.time.base|::ABSTRACTPARTIAL.IS-EQUAL
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-FIELD-TYPES
                                     |org.joda.time.base|::ABSTRACTPARTIAL.GET-VALUES
                                     |org.joda.time.base|::ABSTRACTPARTIAL.IS-BEFORE
                                     |org.joda.time.base|::ABSTRACTPARTIAL.IS-AFTER
                                     |org.joda.time.base|::ABSTRACTPARTIAL.TO-DATE-TIME
                                     |org.joda.time.base|::ABSTRACTPARTIAL.
                                     |org.joda.time.base|::ABSTRACTINTERVAL.START-PROP
                                     |org.joda.time.base|::ABSTRACTINTERVAL.END-PROP
                                     |org.joda.time.base|::ABSTRACTINTERVAL.CLASS-PROP
                                     |org.joda.time.base|::ABSTRACTINTERVAL.BEFORE-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTINTERVAL.AFTER-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTINTERVAL.GET-END-MILLIS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.GET-START-MILLIS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.GET-CHRONOLOGY
                                     |org.joda.time.base|::ABSTRACTINTERVAL.NOTIFY-ALL
                                     |org.joda.time.base|::ABSTRACTINTERVAL.NOTIFY
                                     |org.joda.time.base|::ABSTRACTINTERVAL.GET-CLASS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.WAIT
                                     |org.joda.time.base|::ABSTRACTINTERVAL.CONTAINS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.HASH-CODE
                                     |org.joda.time.base|::ABSTRACTINTERVAL.TO-STRING
                                     |org.joda.time.base|::ABSTRACTINTERVAL.EQUALS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.IS-EQUAL
                                     |org.joda.time.base|::ABSTRACTINTERVAL.TO-DURATION-MILLIS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.TO-MUTABLE-INTERVAL
                                     |org.joda.time.base|::ABSTRACTINTERVAL.TO-INTERVAL
                                     |org.joda.time.base|::ABSTRACTINTERVAL.OVERLAPS
                                     |org.joda.time.base|::ABSTRACTINTERVAL.CONTAINS-NOW
                                     |org.joda.time.base|::ABSTRACTINTERVAL.GET-END
                                     |org.joda.time.base|::ABSTRACTINTERVAL.GET-START
                                     |org.joda.time.base|::ABSTRACTINTERVAL.TO-PERIOD
                                     |org.joda.time.base|::ABSTRACTINTERVAL.TO-DURATION
                                     |org.joda.time.base|::ABSTRACTINTERVAL.IS-BEFORE
                                     |org.joda.time.base|::ABSTRACTINTERVAL.IS-AFTER
                                     |org.joda.time.base|::ABSTRACTINTERVAL.IS-AFTER-NOW
                                     |org.joda.time.base|::ABSTRACTINTERVAL.IS-BEFORE-NOW
                                     |org.joda.time.base|::ABSTRACTINTERVAL.
                                     |org.joda.time.base|::ABSTRACTINSTANT.ZONE-PROP
                                     |org.joda.time.base|::ABSTRACTINSTANT.EQUAL-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTINSTANT.CLASS-PROP
                                     |org.joda.time.base|::ABSTRACTINSTANT.BEFORE-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTINSTANT.AFTER-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTINSTANT.GET-MILLIS
                                     |org.joda.time.base|::ABSTRACTINSTANT.GET-CHRONOLOGY
                                     |org.joda.time.base|::ABSTRACTINSTANT.NOTIFY-ALL
                                     |org.joda.time.base|::ABSTRACTINSTANT.NOTIFY
                                     |org.joda.time.base|::ABSTRACTINSTANT.GET-CLASS
                                     |org.joda.time.base|::ABSTRACTINSTANT.WAIT
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-SUPPORTED
                                     |org.joda.time.base|::ABSTRACTINSTANT.COMPARE-TO
                                     |org.joda.time.base|::ABSTRACTINSTANT.HASH-CODE
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-STRING
                                     |org.joda.time.base|::ABSTRACTINSTANT.EQUALS
                                     |org.joda.time.base|::ABSTRACTINSTANT.GET
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-EQUAL
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-DATE-TIME-+ISO+
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-BEFORE
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-AFTER
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-AFTER-NOW
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-MUTABLE-DATE-TIME-+ISO+
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-INSTANT
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-MUTABLE-DATE-TIME
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-EQUAL-NOW
                                     |org.joda.time.base|::ABSTRACTINSTANT.IS-BEFORE-NOW
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-DATE-TIME
                                     |org.joda.time.base|::ABSTRACTINSTANT.TO-DATE
                                     |org.joda.time.base|::ABSTRACTINSTANT.GET-ZONE
                                     |org.joda.time.base|::ABSTRACTINSTANT.
                                     |org.joda.time.base|::ABSTRACTDURATION.CLASS-PROP
                                     |org.joda.time.base|::ABSTRACTDURATION.GET-MILLIS
                                     |org.joda.time.base|::ABSTRACTDURATION.NOTIFY-ALL
                                     |org.joda.time.base|::ABSTRACTDURATION.NOTIFY
                                     |org.joda.time.base|::ABSTRACTDURATION.GET-CLASS
                                     |org.joda.time.base|::ABSTRACTDURATION.WAIT
                                     |org.joda.time.base|::ABSTRACTDURATION.COMPARE-TO
                                     |org.joda.time.base|::ABSTRACTDURATION.HASH-CODE
                                     |org.joda.time.base|::ABSTRACTDURATION.TO-STRING
                                     |org.joda.time.base|::ABSTRACTDURATION.EQUALS
                                     |org.joda.time.base|::ABSTRACTDURATION.IS-EQUAL
                                     |org.joda.time.base|::ABSTRACTDURATION.IS-SHORTER-THAN
                                     |org.joda.time.base|::ABSTRACTDURATION.IS-LONGER-THAN
                                     |org.joda.time.base|::ABSTRACTDURATION.TO-PERIOD
                                     |org.joda.time.base|::ABSTRACTDURATION.TO-DURATION
                                     |org.joda.time.base|::ABSTRACTDURATION.
                                     |org.joda.time.base|::ABSTRACTDATETIME.ZONE-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.YEAR-OF-ERA-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.YEAR-OF-CENTURY-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.YEAR-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.WEEKYEAR-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.WEEK-OF-WEEKYEAR-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.SECOND-OF-MINUTE-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.SECOND-OF-DAY-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.MONTH-OF-YEAR-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.MINUTE-OF-HOUR-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.MINUTE-OF-DAY-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.MILLIS-OF-SECOND-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.MILLIS-OF-DAY-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.HOUR-OF-DAY-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.ERA-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.EQUAL-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.DAY-OF-YEAR-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.DAY-OF-WEEK-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.DAY-OF-MONTH-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.CLASS-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.CENTURY-OF-ERA-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.BEFORE-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.AFTER-NOW-PROP
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-CHRONOLOGY
                                     |org.joda.time.base|::ABSTRACTDATETIME.NOTIFY-ALL
                                     |org.joda.time.base|::ABSTRACTDATETIME.NOTIFY
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-CLASS
                                     |org.joda.time.base|::ABSTRACTDATETIME.WAIT
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-SUPPORTED
                                     |org.joda.time.base|::ABSTRACTDATETIME.COMPARE-TO
                                     |org.joda.time.base|::ABSTRACTDATETIME.HASH-CODE
                                     |org.joda.time.base|::ABSTRACTDATETIME.EQUALS
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-EQUAL
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-DATE-TIME-+ISO+
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-BEFORE
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-AFTER
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-AFTER-NOW
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-MUTABLE-DATE-TIME-+ISO+
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-INSTANT
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-MUTABLE-DATE-TIME
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-EQUAL-NOW
                                     |org.joda.time.base|::ABSTRACTDATETIME.IS-BEFORE-NOW
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-DATE-TIME
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-DATE
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-ZONE
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-STRING
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-GREGORIAN-CALENDAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-WEEK-OF-WEEKYEAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS-OF-DAY
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-SECOND-OF-MINUTE
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-SECOND-OF-DAY
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-MINUTE-OF-HOUR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-MINUTE-OF-DAY
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-HOUR-OF-DAY
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-WEEK
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-MONTH
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-DAY-OF-YEAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-MONTH-OF-YEAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-WEEKYEAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR-OF-CENTURY
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-YEAR-OF-ERA
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-CENTURY-OF-ERA
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-ERA
                                     |org.joda.time.base|::ABSTRACTDATETIME.TO-CALENDAR
                                     |org.joda.time.base|::ABSTRACTDATETIME.GET-MILLIS-OF-SECOND
                                     |org.joda.time.base|::ABSTRACTDATETIME.) "org.joda.time.base"))
