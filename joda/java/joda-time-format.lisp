(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE "java.util")
(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "org.joda.time.format")
)(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMAT.
   '|org.joda.time.format|::|DateTimeFormat|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMAT. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.SHORT-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.shortDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "shortDateTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.SHORT-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.MEDIUM-DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.mediumDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "mediumDate"
                          '|org.joda.time.format|::DATETIMEFORMAT.MEDIUM-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.FOR-PATTERN (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.forPattern(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "forPattern"
                          '|org.joda.time.format|::DATETIMEFORMAT.FOR-PATTERN
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.FOR-STYLE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.forStyle(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "forStyle"
                          '|org.joda.time.format|::DATETIMEFORMAT.FOR-STYLE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.PATTERN-FOR-STYLE
       (&REST FOIL::ARGS)
  "public static java.lang.String org.joda.time.format.DateTimeFormat.patternForStyle(java.lang.String,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "patternForStyle"
                          '|org.joda.time.format|::DATETIMEFORMAT.PATTERN-FOR-STYLE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.LONG-DATE-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.longDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "longDateTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.LONG-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.FULL-DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.fullDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "fullDate"
                          '|org.joda.time.format|::DATETIMEFORMAT.FULL-DATE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.MEDIUM-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.mediumTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "mediumTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.MEDIUM-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.MEDIUM-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.mediumDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "mediumDateTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.MEDIUM-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.FULL-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.fullTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "fullTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.FULL-TIME NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.FULL-DATE-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.fullDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat|
                          "fullDateTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.FULL-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.LONG-DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.longDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "longDate"
                          '|org.joda.time.format|::DATETIMEFORMAT.LONG-DATE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.LONG-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.longTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "longTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.LONG-TIME NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.SHORT-DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.shortDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "shortDate"
                          '|org.joda.time.format|::DATETIMEFORMAT.SHORT-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.SHORT-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormat.shortTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "shortTime"
                          '|org.joda.time.format|::DATETIMEFORMAT.SHORT-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "wait"
                          '|org.joda.time.format|::DATETIMEFORMAT.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "equals"
                          '|org.joda.time.format|::DATETIMEFORMAT.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "toString"
                          '|org.joda.time.format|::DATETIMEFORMAT.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "hashCode"
                          '|org.joda.time.format|::DATETIMEFORMAT.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "getClass"
                          '|org.joda.time.format|::DATETIMEFORMAT.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "notify"
                          '|org.joda.time.format|::DATETIMEFORMAT.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormat| "notifyAll"
                          '|org.joda.time.format|::DATETIMEFORMAT.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormat| "class"
                       '|org.joda.time.format|::DATETIMEFORMAT.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.
  '|org.joda.time.format|::|DateTimeFormat$StyleFormatter|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormat$StyleFormatter.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormat$StyleFormatter.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "printTo"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.PRINT-TO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormat$StyleFormatter.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.PARSE-INTO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormat$StyleFormatter.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormat$StyleFormatter.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "wait"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "equals"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "toString"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "getClass"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "notify"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatter| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormat$StyleFormatter|
                       "class"
                       '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.
  '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NEW
       (&REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormat$StyleFormatterCacheKey(int,int,int,java.util.Locale)
"
  (FOIL::CALL-CTOR
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL
              (QUOTE
               |org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NEW
         FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormat$StyleFormatterCacheKey.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "equals"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormat$StyleFormatterCacheKey.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "wait"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "toString"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "getClass"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "notify"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormat$StyleFormatterCacheKey| "class"
   '|org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTER.
  '|org.joda.time.format|::|DateTimeFormatter|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter(org.joda.time.format.DateTimePrinter,org.joda.time.format.DateTimeParser)
"
  (FOIL::CALL-CTOR '|org.joda.time.format|::|DateTimeFormatter| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.format|::|DateTimeFormatter|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.format|::DATETIMEFORMATTER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.format.DateTimeFormatter.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getChronology"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.format.DateTimeFormatter.getZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getZone"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withLocale(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withLocale"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withZone(org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withZone"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-OFFSET-PARSED
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withOffsetParsed()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withOffsetParsed"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-OFFSET-PARSED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTime org.joda.time.format.DateTimeFormatter.parseDateTime(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseDateTime"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withChronology(org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withChronology"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-ZONE-+UTC+
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withZoneUTC()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withZoneUTC"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-ZONE-+UTC+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.format.DateTimeFormatter.parseMillis(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseMillis"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-PIVOT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withPivotYear(int)
public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withPivotYear(java.lang.Integer)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withPivotYear"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-PIVOT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-PIVOT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Integer org.joda.time.format.DateTimeFormatter.getPivotYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getPivotYear"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-PIVOT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WITH-DEFAULT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatter.withDefaultYear(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "withDefaultYear"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WITH-DEFAULT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-DEFAULT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatter.getDefaultYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getDefaultYear"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-DEFAULT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.StringBuilder,org.joda.time.ReadableInstant)
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.Appendable,org.joda.time.ReadablePartial) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.StringBuilder,long)
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.StringBuffer,org.joda.time.ReadableInstant)
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.StringBuffer,long)
public void org.joda.time.format.DateTimeFormatter.printTo(java.io.Writer,org.joda.time.ReadablePartial) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.Appendable,org.joda.time.ReadableInstant) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatter.printTo(java.io.Writer,org.joda.time.ReadableInstant) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.StringBuilder,org.joda.time.ReadablePartial)
public void org.joda.time.format.DateTimeFormatter.printTo(java.io.Writer,long) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.Appendable,long) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatter.printTo(java.lang.StringBuffer,org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "printTo"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PRINT-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatter.parseInto(org.joda.time.ReadWritableInstant,java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseInto"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-DATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.LocalDate org.joda.time.format.DateTimeFormatter.parseLocalDate(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseLocalDate"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.LocalTime org.joda.time.format.DateTimeFormatter.parseLocalTime(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseLocalTime"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.LocalDateTime org.joda.time.format.DateTimeFormatter.parseLocalDateTime(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseLocalDateTime"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSE-MUTABLE-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutableDateTime org.joda.time.format.DateTimeFormatter.parseMutableDateTime(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "parseMutableDateTime"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PARSE-MUTABLE-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.IS-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatter.isPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "isPrinter"
                          '|org.joda.time.format|::DATETIMEFORMATTER.IS-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimePrinter org.joda.time.format.DateTimeFormatter.getPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getPrinter"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.IS-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatter.isParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "isParser"
                          '|org.joda.time.format|::DATETIMEFORMATTER.IS-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeParser org.joda.time.format.DateTimeFormatter.getParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getParser"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale org.joda.time.format.DateTimeFormatter.getLocale()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getLocale"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.IS-OFFSET-PARSED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatter.isOffsetParsed()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "isOffsetParsed"
                          '|org.joda.time.format|::DATETIMEFORMATTER.IS-OFFSET-PARSED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-CHRONOLGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.format.DateTimeFormatter.getChronolgy()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getChronolgy"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-CHRONOLGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.format.DateTimeFormatter.print(long)
public java.lang.String org.joda.time.format.DateTimeFormatter.print(org.joda.time.ReadablePartial)
public java.lang.String org.joda.time.format.DateTimeFormatter.print(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter| "print"
                          '|org.joda.time.format|::DATETIMEFORMATTER.PRINT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter| "wait"
                          '|org.joda.time.format|::DATETIMEFORMATTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter| "equals"
                          '|org.joda.time.format|::DATETIMEFORMATTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "toString"
                          '|org.joda.time.format|::DATETIMEFORMATTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "hashCode"
                          '|org.joda.time.format|::DATETIMEFORMATTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "getClass"
                          '|org.joda.time.format|::DATETIMEFORMATTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter| "notify"
                          '|org.joda.time.format|::DATETIMEFORMATTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatter|
                          "notifyAll"
                          '|org.joda.time.format|::DATETIMEFORMATTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.CHRONOLGY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.format.DateTimeFormatter.getChronolgy()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "chronolgy"
                       '|org.joda.time.format|::DATETIMEFORMATTER.CHRONOLGY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.CHRONOLOGY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.format.DateTimeFormatter.getChronology()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter|
                       "chronology"
                       '|org.joda.time.format|::DATETIMEFORMATTER.CHRONOLOGY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "class"
                       '|org.joda.time.format|::DATETIMEFORMATTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.DEFAULT-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatter.getDefaultYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter|
                       "defaultYear"
                       '|org.joda.time.format|::DATETIMEFORMATTER.DEFAULTYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.LOCALE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale org.joda.time.format.DateTimeFormatter.getLocale()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "locale"
                       '|org.joda.time.format|::DATETIMEFORMATTER.LOCALE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.OFFSET-PARSED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatter.isOffsetParsed()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter|
                       "offsetParsed"
                       '|org.joda.time.format|::DATETIMEFORMATTER.OFFSETPARSED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PARSER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatter.isParser()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "parser"
                       '|org.joda.time.format|::DATETIMEFORMATTER.PARSER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PIVOT-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Integer org.joda.time.format.DateTimeFormatter.getPivotYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "pivotYear"
                       '|org.joda.time.format|::DATETIMEFORMATTER.PIVOTYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.PRINTER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatter.isPrinter()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "printer"
                       '|org.joda.time.format|::DATETIMEFORMATTER.PRINTER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTER.ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.format.DateTimeFormatter.getZone()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatter| "zone"
                       '|org.joda.time.format|::DATETIMEFORMATTER.ZONE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER.
  '|org.joda.time.format|::|DateTimeFormatterBuilder|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder()
"
  (FOIL::CALL-CTOR '|org.joda.time.format|::|DateTimeFormatterBuilder|
                   FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.format|::|DateTimeFormatterBuilder|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.format|::DATETIMEFORMATTERBUILDER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MILLIS-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMillisOfDay(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMillisOfDay"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MILLIS-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SECOND-OF-MINUTE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendSecondOfMinute(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendSecondOfMinute"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SECOND-OF-MINUTE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SECOND-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendSecondOfDay(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendSecondOfDay"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SECOND-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MINUTE-OF-HOUR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMinuteOfHour(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMinuteOfHour"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MINUTE-OF-HOUR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MINUTE-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMinuteOfDay(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMinuteOfDay"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MINUTE-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HOUR-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendHourOfDay(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendHourOfDay"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HOUR-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CLOCKHOUR-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendClockhourOfDay(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendClockhourOfDay"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CLOCKHOUR-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HOUR-OF-HALFDAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendHourOfHalfday(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendHourOfHalfday"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HOUR-OF-HALFDAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CLOCKHOUR-OF-HALFDAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendClockhourOfHalfday(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendClockhourOfHalfday"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CLOCKHOUR-OF-HALFDAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendDayOfWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendDayOfWeek"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-MONTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendDayOfMonth(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendDayOfMonth"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-MONTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendDayOfYear(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendDayOfYear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-WEEK-OF-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendWeekOfWeekyear(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendWeekOfWeekyear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-WEEK-OF-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendWeekyear(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendWeekyear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMonthOfYear(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMonthOfYear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendYear(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendYear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TWO-DIGIT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTwoDigitYear(int)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTwoDigitYear(int,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendTwoDigitYear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TWO-DIGIT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TWO-DIGIT-WEEKYEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTwoDigitWeekyear(int)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTwoDigitWeekyear(int,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendTwoDigitWeekyear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TWO-DIGIT-WEEKYEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR-OF-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendYearOfEra(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendYearOfEra"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR-OF-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR-OF-CENTURY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendYearOfCentury(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendYearOfCentury"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR-OF-CENTURY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CENTURY-OF-ERA
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendCenturyOfEra(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendCenturyOfEra"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CENTURY-OF-ERA
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HALFDAY-OF-DAY-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendHalfdayOfDayText()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendHalfdayOfDayText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HALFDAY-OF-DAY-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendDayOfWeekText()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendDayOfWeekText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendDayOfWeekShortText()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendDayOfWeekShortText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMonthOfYearText()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMonthOfYearText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMonthOfYearShortText()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMonthOfYearShortText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-ERA-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendEraText()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendEraText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-ERA-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneName()
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneName(java.util.Map)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendTimeZoneName"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneShortName(java.util.Map)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneShortName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendTimeZoneShortName"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-SHORT-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-ID
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneId()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendTimeZoneId"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-ID
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-PATTERN
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendPattern(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendPattern"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-PATTERN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneOffset(java.lang.String,java.lang.String,boolean,int,int)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendTimeZoneOffset(java.lang.String,boolean,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendTimeZoneOffset"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-FORMATTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatter org.joda.time.format.DateTimeFormatterBuilder.toFormatter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "toFormatter"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-FORMATTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-OPTIONAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendOptional(org.joda.time.format.DateTimeParser)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendOptional"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-OPTIONAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-LITERAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendLiteral(java.lang.String)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendLiteral(char)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendLiteral"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-LITERAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DECIMAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendDecimal(org.joda.time.DateTimeFieldType,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendDecimal"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DECIMAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FIXED-DECIMAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFixedDecimal(org.joda.time.DateTimeFieldType,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFixedDecimal"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FIXED-DECIMAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SIGNED-DECIMAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendSignedDecimal(org.joda.time.DateTimeFieldType,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendSignedDecimal"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SIGNED-DECIMAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FIXED-SIGNED-DECIMAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFixedSignedDecimal(org.joda.time.DateTimeFieldType,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFixedSignedDecimal"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FIXED-SIGNED-DECIMAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendText(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendShortText(org.joda.time.DateTimeFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendShortText"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFraction(org.joda.time.DateTimeFieldType,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFraction"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-SECOND
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFractionOfSecond(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFractionOfSecond"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-SECOND
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-MINUTE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFractionOfMinute(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFractionOfMinute"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-MINUTE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-HOUR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFractionOfHour(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFractionOfHour"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-HOUR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendFractionOfDay(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendFractionOfDay"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MILLIS-OF-SECOND
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.appendMillisOfSecond(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "appendMillisOfSecond"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MILLIS-OF-SECOND
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimePrinter org.joda.time.format.DateTimeFormatterBuilder.toPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "toPrinter"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeParser org.joda.time.format.DateTimeFormatterBuilder.toParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "toParser"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-FORMATTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatterBuilder.canBuildFormatter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "canBuildFormatter"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-FORMATTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatterBuilder.canBuildPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "canBuildPrinter"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeFormatterBuilder.canBuildParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "canBuildParser"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.append(org.joda.time.format.DateTimeFormatter)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.append(org.joda.time.format.DateTimePrinter)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.append(org.joda.time.format.DateTimePrinter,org.joda.time.format.DateTimeParser[])
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.append(org.joda.time.format.DateTimePrinter,org.joda.time.format.DateTimeParser)
public org.joda.time.format.DateTimeFormatterBuilder org.joda.time.format.DateTimeFormatterBuilder.append(org.joda.time.format.DateTimeParser)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "append"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CLEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder.clear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "clear"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.CLEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "wait"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "equals"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "toString"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "hashCode"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "getClass"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "notify"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeFormatterBuilder|
                          "notifyAll"
                          '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeFormatterBuilder|
                       "class"
                       '|org.joda.time.format|::DATETIMEFORMATTERBUILDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$CharacterLiteral.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$CharacterLiteral.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$CharacterLiteral.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$CharacterLiteral.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$CharacterLiteral.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral|
   "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$CharacterLiteral| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$Composite.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$Composite.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$Composite.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$Composite.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$Composite.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Composite| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.
          (|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$FixedNumber.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$PaddedNumber.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$PaddedNumber.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$PaddedNumber.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$FixedNumber| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$Fraction.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$Fraction.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$Fraction.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$Fraction.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$Fraction.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$Fraction| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.
          (|org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$MatchingParser.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|
   "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$MatchingParser.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|
   "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|
   "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|
   "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser|
   "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$MatchingParser| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.InternalPrinter.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public abstract void org.joda.time.format.InternalPrinter.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.InternalPrinter.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$NumberFormatter| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.
          (|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$PaddedNumber.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$PaddedNumber.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$PaddedNumber.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$PaddedNumber| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$StringLiteral.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$StringLiteral.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$StringLiteral.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral|
   "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$StringLiteral.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$StringLiteral.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral|
   "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$StringLiteral| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$TextField.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$TextField.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TextField.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TextField.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TextField.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TextField| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.
          (|java.lang|::ENUM. |org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.VALUES
       (&REST FOIL::ARGS)
  "public static final org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId[] org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.values()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "values"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.VALUES NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.VALUE-OF
       (&REST FOIL::ARGS)
  "public static java.lang.Enum java.lang.Enum.valueOf(java.lang.Class,java.lang.String)
public static org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.valueOf(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "valueOf"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.VALUE-OF NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.lang.Enum.name()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "name"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NAME FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.lang.Enum.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Enum.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int java.lang.Enum.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int java.lang.Enum.compareTo(java.lang.Enum)
public int java.lang.Enum.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "compareTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.COMPARE-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.GET-DECLARING-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.Class java.lang.Enum.getDeclaringClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId|
   "getDeclaringClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.GET-DECLARING-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ORDINAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int java.lang.Enum.ordinal()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "ordinal"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ORDINAL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*
       ()
  "public static final org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId org.joda.time.format.DateTimeFormatterBuilder$TimeZoneId.INSTANCE"
  (FOIL::CALL-FIELD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "INSTANCE"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*
   NIL))
(DEFUN (SETF |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*)
       (FOIL::VAL)
  (FOIL::CALL-FIELD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "INSTANCE"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+* NIL
   FOIL::VAL))
(DEFINE-SYMBOL-MACRO
 |org.joda.time.format|::+DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*+
 (|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.DECLARING-CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.Class java.lang.Enum.getDeclaringClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneId|
   "declaringClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.DECLARINGCLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$TimeZoneName.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$TimeZoneName.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneName.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneName.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneName.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneName| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$TimeZoneOffset.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$TimeZoneOffset.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneOffset.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneOffset.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TimeZoneOffset.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset|
   "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TimeZoneOffset| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.
          (|org.joda.time.format|::INTERNALPRINTER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$TwoDigitYear.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$TwoDigitYear.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TwoDigitYear.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TwoDigitYear.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$TwoDigitYear.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$TwoDigitYear| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.
  '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|)
(DEFCLASS |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.
          (|org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeFormatterBuilder$UnpaddedNumber.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimeFormatterBuilder$UnpaddedNumber.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber| "printTo"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$UnpaddedNumber.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "parseInto"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeFormatterBuilder$NumberFormatter.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber| "wait"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber| "equals"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "toString"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "hashCode"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "getClass"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber| "notify"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber|
   "notifyAll"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeFormatterBuilder$UnpaddedNumber| "class"
   '|org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPARSER.
  '|org.joda.time.format|::|DateTimeParser|)
(DEFCLASS |org.joda.time.format|::DATETIMEPARSER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEPARSER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.DateTimeParser.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParser| "parseInto"
                          '|org.joda.time.format|::DATETIMEPARSER.PARSE-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.DateTimeParser.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParser|
                          "estimateParsedLength"
                          '|org.joda.time.format|::DATETIMEPARSER.ESTIMATE-PARSED-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPARSERBUCKET.
  '|org.joda.time.format|::|DateTimeParserBucket|)
(DEFCLASS |org.joda.time.format|::DATETIMEPARSERBUCKET. (|java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.NEW (&REST FOIL::ARGS)
  "public org.joda.time.format.DateTimeParserBucket(long,org.joda.time.Chronology,java.util.Locale,java.lang.Integer,int)
public org.joda.time.format.DateTimeParserBucket(long,org.joda.time.Chronology,java.util.Locale,java.lang.Integer)
public org.joda.time.format.DateTimeParserBucket(long,org.joda.time.Chronology,java.util.Locale)
"
  (FOIL::CALL-CTOR '|org.joda.time.format|::|DateTimeParserBucket| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.format|::|DateTimeParserBucket|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.format|::DATETIMEPARSERBUCKET.NEW FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.format.DateTimeParserBucket.getChronology()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getChronology"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.format.DateTimeParserBucket.getZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getZone"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.PARSE-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.format.DateTimeParserBucket.parseMillis(org.joda.time.format.DateTimeParser,java.lang.CharSequence)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "parseMillis"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.PARSE-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-PIVOT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Integer org.joda.time.format.DateTimeParserBucket.getPivotYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getPivotYear"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-PIVOT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.COMPUTE-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.format.DateTimeParserBucket.computeMillis(boolean,java.lang.String)
public long org.joda.time.format.DateTimeParserBucket.computeMillis(boolean,java.lang.CharSequence)
public long org.joda.time.format.DateTimeParserBucket.computeMillis(boolean)
public long org.joda.time.format.DateTimeParserBucket.computeMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "computeMillis"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.COMPUTE-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-OFFSET-INTEGER
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Integer org.joda.time.format.DateTimeParserBucket.getOffsetInteger()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getOffsetInteger"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-OFFSET-INTEGER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale org.joda.time.format.DateTimeParserBucket.getLocale()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getLocale"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.SET-PIVOT-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.setPivotYear(java.lang.Integer)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "setPivotYear"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.SET-PIVOT-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.SAVE-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.saveField(org.joda.time.DateTimeFieldType,java.lang.String,java.util.Locale)
public void org.joda.time.format.DateTimeParserBucket.saveField(org.joda.time.DateTimeFieldType,int)
public void org.joda.time.format.DateTimeParserBucket.saveField(org.joda.time.DateTimeField,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "saveField"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.SAVE-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.SAVE-STATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object org.joda.time.format.DateTimeParserBucket.saveState()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "saveState"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.SAVE-STATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.RESTORE-STATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.DateTimeParserBucket.restoreState(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "restoreState"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.RESTORE-STATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.SET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.setZone(org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "setZone"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.SET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.RESET
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.reset()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "reset"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.RESET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeParserBucket.getOffset()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getOffset"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.SET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.setOffset(java.lang.Integer)
public void org.joda.time.format.DateTimeParserBucket.setOffset(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "setOffset"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.SET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "wait"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "equals"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "toString"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "hashCode"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "getClass"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "notify"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimeParserBucket|
                          "notifyAll"
                          '|org.joda.time.format|::DATETIMEPARSERBUCKET.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.CHRONOLOGY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.format.DateTimeParserBucket.getChronology()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket|
                       "chronology"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.CHRONOLOGY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket| "class"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.LOCALE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale org.joda.time.format.DateTimeParserBucket.getLocale()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket| "locale"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.LOCALE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeParserBucket.getOffset()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket| "offset"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.setOffset(int)
"
  (FOIL::CALL-PROP-SET '|org.joda.time.format|::|DateTimeParserBucket| "offset"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-INTEGER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Integer org.joda.time.format.DateTimeParserBucket.getOffsetInteger()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket|
                       "offsetInteger"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSETINTEGER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.PIVOT-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Integer org.joda.time.format.DateTimeParserBucket.getPivotYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket|
                       "pivotYear"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.PIVOTYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |org.joda.time.format|::DATETIMEPARSERBUCKET.PIVOT-YEAR-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.setPivotYear(java.lang.Integer)
"
  (FOIL::CALL-PROP-SET '|org.joda.time.format|::|DateTimeParserBucket|
                       "pivotYear"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.PIVOTYEAR-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET.ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.format.DateTimeParserBucket.getZone()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserBucket| "zone"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.ZONE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |org.joda.time.format|::DATETIMEPARSERBUCKET.ZONE-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimeParserBucket.setZone(org.joda.time.DateTimeZone)
"
  (FOIL::CALL-PROP-SET '|org.joda.time.format|::|DateTimeParserBucket| "zone"
                       '|org.joda.time.format|::DATETIMEPARSERBUCKET.ZONE-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.
  '|org.joda.time.format|::|DateTimeParserBucket$SavedField|)
(DEFCLASS |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.
          (|java.lang|::COMPARABLE. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeParserBucket$SavedField.compareTo(java.lang.Object)
public int org.joda.time.format.DateTimeParserBucket$SavedField.compareTo(org.joda.time.format.DateTimeParserBucket$SavedField)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "compareTo"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.COMPARE-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "wait"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "equals"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "toString"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "hashCode"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "getClass"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "notify"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "notifyAll"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeParserBucket$SavedField| "class"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.
  '|org.joda.time.format|::|DateTimeParserBucket$SavedState|)
(DEFCLASS |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "wait"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "equals"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "toString"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "hashCode"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "getClass"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "notify"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "notifyAll"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimeParserBucket$SavedState| "class"
   '|org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.
  '|org.joda.time.format|::|DateTimeParserInternalParser|)
(DEFCLASS |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.
          (|org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeParserInternalParser.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "parseInto"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.PARSE-INTO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimeParserInternalParser.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser|
   "estimateParsedLength"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "wait"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "equals"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "toString"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "hashCode"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "getClass"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "notify"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimeParserInternalParser| "notifyAll"
   '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|DateTimeParserInternalParser|
                       "class"
                       '|org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPRINTER.
  '|org.joda.time.format|::|DateTimePrinter|)
(DEFCLASS |org.joda.time.format|::DATETIMEPRINTER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEPRINTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.DateTimePrinter.printTo(java.io.Writer,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public abstract void org.joda.time.format.DateTimePrinter.printTo(java.lang.StringBuffer,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale)
public abstract void org.joda.time.format.DateTimePrinter.printTo(java.io.Writer,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public abstract void org.joda.time.format.DateTimePrinter.printTo(java.lang.StringBuffer,org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimePrinter| "printTo"
                          '|org.joda.time.format|::DATETIMEPRINTER.PRINT-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.DateTimePrinter.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|DateTimePrinter|
                          "estimatePrintedLength"
                          '|org.joda.time.format|::DATETIMEPRINTER.ESTIMATE-PRINTED-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.
  '|org.joda.time.format|::|DateTimePrinterInternalPrinter|)
(DEFCLASS |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.
          (|org.joda.time.format|::INTERNALPRINTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.DateTimePrinterInternalPrinter.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.DateTimePrinterInternalPrinter.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "printTo"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.PRINT-TO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.DateTimePrinterInternalPrinter.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter|
   "estimatePrintedLength"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "wait"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "equals"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "toString"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "hashCode"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "getClass"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "notify"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "notifyAll"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|DateTimePrinterInternalPrinter| "class"
   '|org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.CLASS-GET FOIL::THIS
   FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::FORMATUTILS.
  '|org.joda.time.format|::|FormatUtils|)
(DEFCLASS |org.joda.time.format|::FORMATUTILS. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::FORMATUTILS.APPEND-PADDED-INTEGER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.format.FormatUtils.appendPaddedInteger(java.lang.Appendable,long,int) throws java.io.IOException
public static void org.joda.time.format.FormatUtils.appendPaddedInteger(java.lang.StringBuffer,int,int)
public static void org.joda.time.format.FormatUtils.appendPaddedInteger(java.lang.Appendable,int,int) throws java.io.IOException
public static void org.joda.time.format.FormatUtils.appendPaddedInteger(java.lang.StringBuffer,long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils|
                          "appendPaddedInteger"
                          '|org.joda.time.format|::FORMATUTILS.APPEND-PADDED-INTEGER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.APPEND-UNPADDED-INTEGER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.format.FormatUtils.appendUnpaddedInteger(java.lang.Appendable,int) throws java.io.IOException
public static void org.joda.time.format.FormatUtils.appendUnpaddedInteger(java.lang.Appendable,long) throws java.io.IOException
public static void org.joda.time.format.FormatUtils.appendUnpaddedInteger(java.lang.StringBuffer,long)
public static void org.joda.time.format.FormatUtils.appendUnpaddedInteger(java.lang.StringBuffer,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils|
                          "appendUnpaddedInteger"
                          '|org.joda.time.format|::FORMATUTILS.APPEND-UNPADDED-INTEGER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.CALCULATE-DIGIT-COUNT
       (&REST FOIL::ARGS)
  "public static int org.joda.time.format.FormatUtils.calculateDigitCount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils|
                          "calculateDigitCount"
                          '|org.joda.time.format|::FORMATUTILS.CALCULATE-DIGIT-COUNT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.WRITE-UNPADDED-INTEGER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.format.FormatUtils.writeUnpaddedInteger(java.io.Writer,int) throws java.io.IOException
public static void org.joda.time.format.FormatUtils.writeUnpaddedInteger(java.io.Writer,long) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils|
                          "writeUnpaddedInteger"
                          '|org.joda.time.format|::FORMATUTILS.WRITE-UNPADDED-INTEGER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.WRITE-PADDED-INTEGER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.format.FormatUtils.writePaddedInteger(java.io.Writer,long,int) throws java.io.IOException
public static void org.joda.time.format.FormatUtils.writePaddedInteger(java.io.Writer,int,int) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils|
                          "writePaddedInteger"
                          '|org.joda.time.format|::FORMATUTILS.WRITE-PADDED-INTEGER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "wait"
                          '|org.joda.time.format|::FORMATUTILS.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "equals"
                          '|org.joda.time.format|::FORMATUTILS.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "toString"
                          '|org.joda.time.format|::FORMATUTILS.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "hashCode"
                          '|org.joda.time.format|::FORMATUTILS.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "getClass"
                          '|org.joda.time.format|::FORMATUTILS.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "notify"
                          '|org.joda.time.format|::FORMATUTILS.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|FormatUtils| "notifyAll"
                          '|org.joda.time.format|::FORMATUTILS.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::FORMATUTILS.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|FormatUtils| "class"
                       '|org.joda.time.format|::FORMATUTILS.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::INTERNALPARSER.
  '|org.joda.time.format|::|InternalParser|)
(DEFCLASS |org.joda.time.format|::INTERNALPARSER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::INTERNALPARSER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.InternalParser.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|InternalParser| "parseInto"
                          '|org.joda.time.format|::INTERNALPARSER.PARSE-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.InternalParser.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|InternalParser|
                          "estimateParsedLength"
                          '|org.joda.time.format|::INTERNALPARSER.ESTIMATE-PARSED-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.
  '|org.joda.time.format|::|InternalParserDateTimeParser|)
(DEFCLASS |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.
          (|org.joda.time.format|::DATETIMEPARSER.
           |org.joda.time.format|::INTERNALPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.InternalParserDateTimeParser.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.CharSequence,int)
public int org.joda.time.format.InternalParserDateTimeParser.parseInto(org.joda.time.format.DateTimeParserBucket,java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "parseInto"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.PARSE-INTO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.ESTIMATE-PARSED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.InternalParserDateTimeParser.estimateParsedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser|
   "estimateParsedLength"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.ESTIMATE-PARSED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.InternalParserDateTimeParser.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "equals"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "wait"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "toString"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "hashCode"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "getClass"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "notify"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalParserDateTimeParser| "notifyAll"
   '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|InternalParserDateTimeParser|
                       "class"
                       '|org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::INTERNALPRINTER.
  '|org.joda.time.format|::|InternalPrinter|)
(DEFCLASS |org.joda.time.format|::INTERNALPRINTER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::INTERNALPRINTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.InternalPrinter.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public abstract void org.joda.time.format.InternalPrinter.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|InternalPrinter| "printTo"
                          '|org.joda.time.format|::INTERNALPRINTER.PRINT-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.InternalPrinter.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|InternalPrinter|
                          "estimatePrintedLength"
                          '|org.joda.time.format|::INTERNALPRINTER.ESTIMATE-PRINTED-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.
  '|org.joda.time.format|::|InternalPrinterDateTimePrinter|)
(DEFCLASS |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.
          (|org.joda.time.format|::DATETIMEPRINTER.
           |org.joda.time.format|::INTERNALPRINTER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.InternalPrinterDateTimePrinter.printTo(java.lang.StringBuffer,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale)
public void org.joda.time.format.InternalPrinterDateTimePrinter.printTo(java.lang.Appendable,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.InternalPrinterDateTimePrinter.printTo(java.io.Writer,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.InternalPrinterDateTimePrinter.printTo(java.lang.StringBuffer,org.joda.time.ReadablePartial,java.util.Locale)
public void org.joda.time.format.InternalPrinterDateTimePrinter.printTo(java.lang.Appendable,org.joda.time.ReadablePartial,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.InternalPrinterDateTimePrinter.printTo(java.io.Writer,long,org.joda.time.Chronology,int,org.joda.time.DateTimeZone,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "printTo"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.PRINT-TO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.ESTIMATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.InternalPrinterDateTimePrinter.estimatePrintedLength()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter|
   "estimatePrintedLength"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.ESTIMATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.InternalPrinterDateTimePrinter.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "equals"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "wait"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "toString"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "hashCode"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "getClass"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "notify"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "notifyAll"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|InternalPrinterDateTimePrinter| "class"
   '|org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.CLASS-GET FOIL::THIS
   FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::ISODATETIMEFORMAT.
  '|org.joda.time.format|::|ISODateTimeFormat|)
(DEFCLASS |org.joda.time.format|::ISODATETIMEFORMAT. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.TIME-PARSER (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.timeParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "timeParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.TIME-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.T-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.tTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "tTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.T-TIME NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-DATE-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.localDateParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "localDateParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-DATE-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.YEAR (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.year()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "year"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.YEAR NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.weekyear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "weekyear"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateTimeParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateTimeParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-ELEMENT-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateElementParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateElementParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-ELEMENT-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.weekDateTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "weekDateTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.TIME-ELEMENT-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.timeElementParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "timeElementParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.TIME-ELEMENT-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.weekDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "weekDateTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.FOR-FIELDS (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.forFields(java.util.Collection,boolean,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "forFields"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.FOR-FIELDS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.YEAR-MONTH-DAY
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.yearMonthDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "yearMonthDay"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.YEAR-MONTH-DAY
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.T-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.tTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "tTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.T-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.ordinalDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "ordinalDate"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.ordinalDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "ordinalDateTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.weekDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "weekDate"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-T-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicTTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicTTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-T-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-T-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicTTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicTTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-T-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicDateTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicDateTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicDateTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicOrdinalDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicOrdinalDate"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicOrdinalDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicOrdinalDateTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.timeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "timeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-PARSER (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicOrdinalDateTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicOrdinalDateTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicWeekDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicWeekDate"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE-TIME
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicWeekDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicWeekDateTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicWeekDateTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicWeekDateTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.YEAR-MONTH (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.yearMonth()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "yearMonth"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.YEAR-MONTH
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR-WEEK
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.weekyearWeek()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "weekyearWeek"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR-WEEK
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR-WEEK-DAY
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.weekyearWeekDay()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "weekyearWeekDay"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR-WEEK-DAY
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.HOUR (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.hour()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "hour"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.HOUR NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.hourMinute()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "hourMinute"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.hourMinuteSecond()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "hourMinuteSecond"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.hourMinuteSecondMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "hourMinuteSecondMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND-FRACTION
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.hourMinuteSecondFraction()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "hourMinuteSecondFraction"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND-FRACTION
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateHour()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateHour"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateHourMinute()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateHourMinute"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateHourMinuteSecond()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateHourMinuteSecond"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateHourMinuteSecondMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateHourMinuteSecondMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND-FRACTION
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateHourMinuteSecondFraction()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateHourMinuteSecondFraction"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND-FRACTION
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE-TIME-NO-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.ordinalDateTimeNoMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "ordinalDateTimeNoMillis"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE-TIME-NO-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-DATE-OPTIONAL-TIME-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.localDateOptionalTimeParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "localDateOptionalTimeParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-DATE-OPTIONAL-TIME-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE-OPTIONAL-TIME-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.dateOptionalTimeParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "dateOptionalTimeParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE-OPTIONAL-TIME-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-TIME-PARSER
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.localTimeParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "localTimeParser"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-TIME-PARSER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicDate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicDate"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.basicTime()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "basicTime"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.BASIC-TIME
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.TIME (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.time()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "time"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.TIME NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.DATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.DateTimeFormatter org.joda.time.format.ISODateTimeFormat.date()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "date"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.DATE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "wait"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "equals"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "toString"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "hashCode"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "getClass"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat| "notify"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISODateTimeFormat|
                          "notifyAll"
                          '|org.joda.time.format|::ISODATETIMEFORMAT.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|ISODateTimeFormat| "class"
                       '|org.joda.time.format|::ISODATETIMEFORMAT.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.
  '|org.joda.time.format|::|ISODateTimeFormat$Constants|)
(DEFCLASS |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "wait"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "equals"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "toString"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "hashCode"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "getClass"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "notify"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|ISODateTimeFormat$Constants| "notifyAll"
   '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|ISODateTimeFormat$Constants|
                       "class"
                       '|org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::ISOPERIODFORMAT.
  '|org.joda.time.format|::|ISOPeriodFormat|)
(DEFCLASS |org.joda.time.format|::ISOPERIODFORMAT. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.STANDARD (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.ISOPeriodFormat.standard()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "standard"
                          '|org.joda.time.format|::ISOPERIODFORMAT.STANDARD NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.ISOPeriodFormat.alternate()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat|
                          "alternate"
                          '|org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-EXTENDED
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.ISOPeriodFormat.alternateExtended()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat|
                          "alternateExtended"
                          '|org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-EXTENDED
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-WITH-WEEKS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.ISOPeriodFormat.alternateWithWeeks()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat|
                          "alternateWithWeeks"
                          '|org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-WITH-WEEKS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-EXTENDED-WITH-WEEKS
       (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.ISOPeriodFormat.alternateExtendedWithWeeks()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat|
                          "alternateExtendedWithWeeks"
                          '|org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-EXTENDED-WITH-WEEKS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "wait"
                          '|org.joda.time.format|::ISOPERIODFORMAT.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "equals"
                          '|org.joda.time.format|::ISOPERIODFORMAT.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "toString"
                          '|org.joda.time.format|::ISOPERIODFORMAT.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "hashCode"
                          '|org.joda.time.format|::ISOPERIODFORMAT.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "getClass"
                          '|org.joda.time.format|::ISOPERIODFORMAT.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat| "notify"
                          '|org.joda.time.format|::ISOPERIODFORMAT.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|ISOPeriodFormat|
                          "notifyAll"
                          '|org.joda.time.format|::ISOPERIODFORMAT.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::ISOPERIODFORMAT.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|ISOPeriodFormat| "class"
                       '|org.joda.time.format|::ISOPERIODFORMAT.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMAT.
  '|org.joda.time.format|::|PeriodFormat|)
(DEFCLASS |org.joda.time.format|::PERIODFORMAT. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMAT.WORD-BASED (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.PeriodFormat.wordBased(java.util.Locale)
public static org.joda.time.format.PeriodFormatter org.joda.time.format.PeriodFormat.wordBased()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "wordBased"
                          '|org.joda.time.format|::PERIODFORMAT.WORD-BASED NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.GET-DEFAULT (&REST FOIL::ARGS)
  "public static org.joda.time.format.PeriodFormatter org.joda.time.format.PeriodFormat.getDefault()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "getDefault"
                          '|org.joda.time.format|::PERIODFORMAT.GET-DEFAULT NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "wait"
                          '|org.joda.time.format|::PERIODFORMAT.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "equals"
                          '|org.joda.time.format|::PERIODFORMAT.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "toString"
                          '|org.joda.time.format|::PERIODFORMAT.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "hashCode"
                          '|org.joda.time.format|::PERIODFORMAT.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "getClass"
                          '|org.joda.time.format|::PERIODFORMAT.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "notify"
                          '|org.joda.time.format|::PERIODFORMAT.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormat| "notifyAll"
                          '|org.joda.time.format|::PERIODFORMAT.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormat| "class"
                       '|org.joda.time.format|::PERIODFORMAT.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.
  '|org.joda.time.format|::|PeriodFormat$DynamicWordBased|)
(DEFCLASS |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.
          (|org.joda.time.format|::PERIODPRINTER.
           |org.joda.time.format|::PERIODPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormat$DynamicWordBased.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod,java.util.Locale)
public void org.joda.time.format.PeriodFormat$DynamicWordBased.printTo(java.io.Writer,org.joda.time.ReadablePeriod,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "printTo"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.PRINT-TO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormat$DynamicWordBased.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "parseInto"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.PARSE-INTO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormat$DynamicWordBased.calculatePrintedLength(org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.COUNT-FIELDS-TO-PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormat$DynamicWordBased.countFieldsToPrint(org.joda.time.ReadablePeriod,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased|
   "countFieldsToPrint"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.COUNT-FIELDS-TO-PRINT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "wait"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "equals"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "toString"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "hashCode"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "getClass"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "notify"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormat$DynamicWordBased| "notifyAll"
   '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormat$DynamicWordBased|
                       "class"
                       '|org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTER.
  '|org.joda.time.format|::|PeriodFormatter|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatter(org.joda.time.format.PeriodPrinter,org.joda.time.format.PeriodParser)
"
  (FOIL::CALL-CTOR '|org.joda.time.format|::|PeriodFormatter| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.format|::|PeriodFormatter|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.format|::PERIODFORMATTER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.WITH-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatter org.joda.time.format.PeriodFormatter.withLocale(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "withLocale"
                          '|org.joda.time.format|::PERIODFORMATTER.WITH-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatter.printTo(java.io.Writer,org.joda.time.ReadablePeriod) throws java.io.IOException
public void org.joda.time.format.PeriodFormatter.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "printTo"
                          '|org.joda.time.format|::PERIODFORMATTER.PRINT-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatter.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "parseInto"
                          '|org.joda.time.format|::PERIODFORMATTER.PARSE-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.IS-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.PeriodFormatter.isPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "isPrinter"
                          '|org.joda.time.format|::PERIODFORMATTER.IS-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.GET-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodPrinter org.joda.time.format.PeriodFormatter.getPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "getPrinter"
                          '|org.joda.time.format|::PERIODFORMATTER.GET-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.IS-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.PeriodFormatter.isParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "isParser"
                          '|org.joda.time.format|::PERIODFORMATTER.IS-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.GET-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodParser org.joda.time.format.PeriodFormatter.getParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "getParser"
                          '|org.joda.time.format|::PERIODFORMATTER.GET-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.GET-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale org.joda.time.format.PeriodFormatter.getLocale()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "getLocale"
                          '|org.joda.time.format|::PERIODFORMATTER.GET-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PARSE-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Period org.joda.time.format.PeriodFormatter.parsePeriod(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "parsePeriod"
                          '|org.joda.time.format|::PERIODFORMATTER.PARSE-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.WITH-PARSE-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatter org.joda.time.format.PeriodFormatter.withParseType(org.joda.time.PeriodType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "withParseType"
                          '|org.joda.time.format|::PERIODFORMATTER.WITH-PARSE-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PARSE-MUTABLE-PERIOD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.MutablePeriod org.joda.time.format.PeriodFormatter.parseMutablePeriod(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "parseMutablePeriod"
                          '|org.joda.time.format|::PERIODFORMATTER.PARSE-MUTABLE-PERIOD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.GET-PARSE-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.format.PeriodFormatter.getParseType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "getParseType"
                          '|org.joda.time.format|::PERIODFORMATTER.GET-PARSE-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.format.PeriodFormatter.print(org.joda.time.ReadablePeriod)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "print"
                          '|org.joda.time.format|::PERIODFORMATTER.PRINT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "wait"
                          '|org.joda.time.format|::PERIODFORMATTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "equals"
                          '|org.joda.time.format|::PERIODFORMATTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "toString"
                          '|org.joda.time.format|::PERIODFORMATTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "hashCode"
                          '|org.joda.time.format|::PERIODFORMATTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "getClass"
                          '|org.joda.time.format|::PERIODFORMATTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter| "notify"
                          '|org.joda.time.format|::PERIODFORMATTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatter|
                          "notifyAll"
                          '|org.joda.time.format|::PERIODFORMATTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormatter| "class"
                       '|org.joda.time.format|::PERIODFORMATTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.LOCALE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Locale org.joda.time.format.PeriodFormatter.getLocale()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormatter| "locale"
                       '|org.joda.time.format|::PERIODFORMATTER.LOCALE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PARSE-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.format.PeriodFormatter.getParseType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormatter| "parseType"
                       '|org.joda.time.format|::PERIODFORMATTER.PARSETYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PARSER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.PeriodFormatter.isParser()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormatter| "parser"
                       '|org.joda.time.format|::PERIODFORMATTER.PARSER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTER.PRINTER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.format.PeriodFormatter.isPrinter()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormatter| "printer"
                       '|org.joda.time.format|::PERIODFORMATTER.PRINTER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER.
  '|org.joda.time.format|::|PeriodFormatterBuilder|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder()
"
  (FOIL::CALL-CTOR '|org.joda.time.format|::|PeriodFormatterBuilder|
                   FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.format|::|PeriodFormatterBuilder|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.format|::PERIODFORMATTERBUILDER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-FORMATTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatter org.joda.time.format.PeriodFormatterBuilder.toFormatter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "toFormatter"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.TO-FORMATTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-LITERAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendLiteral(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendLiteral"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-LITERAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-ALWAYS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.printZeroAlways()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "printZeroAlways"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-ALWAYS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.MINIMUM-PRINTED-DIGITS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.minimumPrintedDigits(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "minimumPrintedDigits"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.MINIMUM-PRINTED-DIGITS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR-IF-FIELDS-BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSeparatorIfFieldsBefore(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSeparatorIfFieldsBefore"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR-IF-FIELDS-BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-PRINTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodPrinter org.joda.time.format.PeriodFormatterBuilder.toPrinter()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "toPrinter"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.TO-PRINTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-PARSER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodParser org.joda.time.format.PeriodFormatterBuilder.toParser()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "toParser"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.TO-PARSER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.MAXIMUM-PARSED-DIGITS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.maximumParsedDigits(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "maximumParsedDigits"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.MAXIMUM-PARSED-DIGITS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.REJECT-SIGNED-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.rejectSignedValues(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "rejectSignedValues"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.REJECT-SIGNED-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-RARELY-LAST
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.printZeroRarelyLast()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "printZeroRarelyLast"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-RARELY-LAST
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-RARELY-FIRST
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.printZeroRarelyFirst()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "printZeroRarelyFirst"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-RARELY-FIRST
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-IF-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.printZeroIfSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "printZeroIfSupported"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-IF-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-NEVER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.printZeroNever()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "printZeroNever"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-NEVER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSeconds()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSeconds"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS-WITH-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSecondsWithMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSecondsWithMillis"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS-WITH-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendMillis"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MILLIS-3-*DIGIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendMillis3Digit()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendMillis3Digit"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MILLIS-3-*DIGIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-YEARS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendYears()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendYears"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-YEARS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SUFFIX
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSuffix(java.lang.String)
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSuffix(java.lang.String,java.lang.String)
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSuffix(java.lang.String[],java.lang.String[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSuffix"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SUFFIX
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MONTHS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendMonths()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendMonths"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MONTHS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-WEEKS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendWeeks()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendWeeks"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-WEEKS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-DAYS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendDays()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendDays"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-DAYS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR-IF-FIELDS-AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSeparatorIfFieldsAfter(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSeparatorIfFieldsAfter"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR-IF-FIELDS-AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-HOURS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendHours()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendHours"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-HOURS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MINUTES
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendMinutes()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendMinutes"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MINUTES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS-WITH-OPTIONAL-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSecondsWithOptionalMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSecondsWithOptionalMillis"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS-WITH-OPTIONAL-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSeparator(java.lang.String)
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSeparator(java.lang.String,java.lang.String,java.lang.String[])
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendSeparator(java.lang.String,java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendSeparator"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-PREFIX
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendPrefix(java.lang.String)
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendPrefix(java.lang.String,java.lang.String)
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.appendPrefix(java.lang.String[],java.lang.String[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "appendPrefix"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-PREFIX
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.append(org.joda.time.format.PeriodPrinter,org.joda.time.format.PeriodParser)
public org.joda.time.format.PeriodFormatterBuilder org.joda.time.format.PeriodFormatterBuilder.append(org.joda.time.format.PeriodFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "append"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.CLEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder.clear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "clear"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.CLEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "wait"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "equals"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "toString"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "hashCode"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "getClass"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "notify"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodFormatterBuilder|
                          "notifyAll"
                          '|org.joda.time.format|::PERIODFORMATTERBUILDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.format|::|PeriodFormatterBuilder|
                       "class"
                       '|org.joda.time.format|::PERIODFORMATTERBUILDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.
  '|org.joda.time.format|::|PeriodFormatterBuilder$Composite|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.
          (|org.joda.time.format|::PERIODPRINTER.
           |org.joda.time.format|::PERIODPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$Composite.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod,java.util.Locale)
public void org.joda.time.format.PeriodFormatterBuilder$Composite.printTo(java.io.Writer,org.joda.time.ReadablePeriod,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Composite.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "parseInto"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Composite.calculatePrintedLength(org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.COUNT-FIELDS-TO-PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Composite.countFieldsToPrint(org.joda.time.ReadablePeriod,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite|
   "countFieldsToPrint"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.COUNT-FIELDS-TO-PRINT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$Composite| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.
  '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.
          (|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.printTo(java.io.Writer,int) throws java.io.IOException
public void org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.printTo(java.lang.StringBuffer,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.calculatePrintedLength(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.GET-AFFIXES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.getAffixes()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix|
   "getAffixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.GET-AFFIXES
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.SCAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.scan(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "scan"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.SCAN
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.PARSE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.parse(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "parse"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.PARSE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$IgnorableAffix.finish(java.util.Set)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.FINISH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.AFFIXES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$CompositeAffix.getAffixes()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "affixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.AFFIXES-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$CompositeAffix| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.
  '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.
          (|org.joda.time.format|::PERIODPRINTER.
           |org.joda.time.format|::PERIODPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$FieldFormatter.printTo(java.io.Writer,org.joda.time.ReadablePeriod,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.PeriodFormatterBuilder$FieldFormatter.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$FieldFormatter.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "parseInto"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$FieldFormatter.calculatePrintedLength(org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$FieldFormatter.finish(org.joda.time.format.PeriodFormatterBuilder$FieldFormatter[])
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.FINISH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.COUNT-FIELDS-TO-PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$FieldFormatter.countFieldsToPrint(org.joda.time.ReadablePeriod,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter|
   "countFieldsToPrint"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.COUNT-FIELDS-TO-PRINT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$FieldFormatter| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.
  '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.
          (|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.
           |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$IgnorableAffix.finish(java.util.Set)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.FINISH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.printTo(java.lang.StringBuffer,int)
public abstract void org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.printTo(java.io.Writer,int) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.calculatePrintedLength(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.GET-AFFIXES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.getAffixes()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix|
   "getAffixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.GET-AFFIXES
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.SCAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.scan(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "scan"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.SCAN
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.PARSE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.parse(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "parse"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.PARSE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$IgnorableAffix| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.
  '|org.joda.time.format|::|PeriodFormatterBuilder$Literal|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.
          (|org.joda.time.format|::PERIODPRINTER.
           |org.joda.time.format|::PERIODPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$Literal.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod,java.util.Locale)
public void org.joda.time.format.PeriodFormatterBuilder$Literal.printTo(java.io.Writer,org.joda.time.ReadablePeriod,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.PRINT-TO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Literal.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "parseInto"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Literal.calculatePrintedLength(org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.COUNT-FIELDS-TO-PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Literal.countFieldsToPrint(org.joda.time.ReadablePeriod,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal|
   "countFieldsToPrint"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.COUNT-FIELDS-TO-PRINT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$Literal| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.CLASS-GET FOIL::THIS
   FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.
  '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.printTo(java.lang.StringBuffer,int)
public abstract void org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.printTo(java.io.Writer,int) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.calculatePrintedLength(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.finish(java.util.Set)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.FINISH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.GET-AFFIXES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.getAffixes()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix|
   "getAffixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.GET-AFFIXES
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.SCAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.scan(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix| "scan"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.SCAN
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.PARSE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.parse(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix| "parse"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.PARSE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.AFFIXES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix.getAffixes()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$PeriodFieldAffix| "affixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.AFFIXES-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.
  '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.
          (|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$PluralAffix.printTo(java.io.Writer,int) throws java.io.IOException
public void org.joda.time.format.PeriodFormatterBuilder$PluralAffix.printTo(java.lang.StringBuffer,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$PluralAffix.calculatePrintedLength(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.GET-AFFIXES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$PluralAffix.getAffixes()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "getAffixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.GET-AFFIXES
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.SCAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$PluralAffix.scan(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "scan"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.SCAN FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.PARSE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$PluralAffix.parse(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "parse"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.PARSE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$IgnorableAffix.finish(java.util.Set)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.FINISH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.AFFIXES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$PluralAffix.getAffixes()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "affixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.AFFIXES-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$PluralAffix| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.
  '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.
          (|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$RegExAffix.printTo(java.io.Writer,int) throws java.io.IOException
public void org.joda.time.format.PeriodFormatterBuilder$RegExAffix.printTo(java.lang.StringBuffer,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$RegExAffix.calculatePrintedLength(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.GET-AFFIXES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$RegExAffix.getAffixes()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "getAffixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.GET-AFFIXES
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.SCAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$RegExAffix.scan(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "scan"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.SCAN FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.PARSE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$RegExAffix.parse(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "parse"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.PARSE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$IgnorableAffix.finish(java.util.Set)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.FINISH FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.AFFIXES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$RegExAffix.getAffixes()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "affixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.AFFIXES-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.
  '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.
          (|java.util|::COMPARATOR. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.COMPARE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$RegExAffix$1.compare(java.lang.Object,java.lang.Object)
public int org.joda.time.format.PeriodFormatterBuilder$RegExAffix$1.compare(java.lang.String,java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "compare"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.COMPARE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.REVERSED
       (FOIL::THIS &REST FOIL::ARGS)
  "public default java.util.Comparator java.util.Comparator.reversed()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "reversed"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.REVERSED
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING
       (FOIL::THIS &REST FOIL::ARGS)
  "public default java.util.Comparator java.util.Comparator.thenComparing(java.util.Comparator)
public default java.util.Comparator java.util.Comparator.thenComparing(java.util.function.Function,java.util.Comparator)
public default java.util.Comparator java.util.Comparator.thenComparing(java.util.function.Function)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1|
   "thenComparing"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-INT
       (FOIL::THIS &REST FOIL::ARGS)
  "public default java.util.Comparator java.util.Comparator.thenComparingInt(java.util.function.ToIntFunction)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1|
   "thenComparingInt"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-INT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public default java.util.Comparator java.util.Comparator.thenComparingLong(java.util.function.ToLongFunction)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1|
   "thenComparingLong"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-LONG
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-DOUBLE
       (FOIL::THIS &REST FOIL::ARGS)
  "public default java.util.Comparator java.util.Comparator.thenComparingDouble(java.util.function.ToDoubleFunction)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1|
   "thenComparingDouble"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-DOUBLE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$RegExAffix$1| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.
  '|org.joda.time.format|::|PeriodFormatterBuilder$Separator|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.
          (|org.joda.time.format|::PERIODPRINTER.
           |org.joda.time.format|::PERIODPARSER. |java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$Separator.printTo(java.io.Writer,org.joda.time.ReadablePeriod,java.util.Locale) throws java.io.IOException
public void org.joda.time.format.PeriodFormatterBuilder$Separator.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Separator.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "parseInto"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.PARSE-INTO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Separator.calculatePrintedLength(org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.COUNT-FIELDS-TO-PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$Separator.countFieldsToPrint(org.joda.time.ReadablePeriod,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator|
   "countFieldsToPrint"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.COUNT-FIELDS-TO-PRINT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$Separator| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.
  '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix|)
(DEFCLASS |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.
          (|org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.) NIL)
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.printTo(java.io.Writer,int) throws java.io.IOException
public void org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.printTo(java.lang.StringBuffer,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "printTo"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.PRINT-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.calculatePrintedLength(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix|
   "calculatePrintedLength"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.CALCULATE-PRINTED-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.GET-AFFIXES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.getAffixes()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "getAffixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.GET-AFFIXES
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.SCAN
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.scan(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "scan"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.SCAN FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.PARSE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.parse(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "parse"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.PARSE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.FINISH
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.format.PeriodFormatterBuilder$IgnorableAffix.finish(java.util.Set)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "finish"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.FINISH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "wait"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "equals"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "toString"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "hashCode"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "getClass"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "notify"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "notifyAll"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.AFFIXES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String[] org.joda.time.format.PeriodFormatterBuilder$SimpleAffix.getAffixes()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "affixes"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.AFFIXES-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.format|::|PeriodFormatterBuilder$SimpleAffix| "class"
   '|org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODPARSER.
  '|org.joda.time.format|::|PeriodParser|)
(DEFCLASS |org.joda.time.format|::PERIODPARSER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODPARSER.PARSE-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodParser.parseInto(org.joda.time.ReadWritablePeriod,java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodParser| "parseInto"
                          '|org.joda.time.format|::PERIODPARSER.PARSE-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.format|::PERIODPRINTER.
  '|org.joda.time.format|::|PeriodPrinter|)
(DEFCLASS |org.joda.time.format|::PERIODPRINTER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.format|::PERIODPRINTER.PRINT-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.format.PeriodPrinter.printTo(java.lang.StringBuffer,org.joda.time.ReadablePeriod,java.util.Locale)
public abstract void org.joda.time.format.PeriodPrinter.printTo(java.io.Writer,org.joda.time.ReadablePeriod,java.util.Locale) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodPrinter| "printTo"
                          '|org.joda.time.format|::PERIODPRINTER.PRINT-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODPRINTER.CALCULATE-PRINTED-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodPrinter.calculatePrintedLength(org.joda.time.ReadablePeriod,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodPrinter|
                          "calculatePrintedLength"
                          '|org.joda.time.format|::PERIODPRINTER.CALCULATE-PRINTED-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.format|::PERIODPRINTER.COUNT-FIELDS-TO-PRINT
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.format.PeriodPrinter.countFieldsToPrint(org.joda.time.ReadablePeriod,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.format|::|PeriodPrinter|
                          "countFieldsToPrint"
                          '|org.joda.time.format|::PERIODPRINTER.COUNT-FIELDS-TO-PRINT
                          FOIL::THIS FOIL::ARGS))
(eval-when (:load-toplevel)(export '(|org.joda.time.format|::PERIODPRINTER.COUNT-FIELDS-TO-PRINT
                                     |org.joda.time.format|::PERIODPRINTER.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODPRINTER.PRINT-TO
                                     |org.joda.time.format|::PERIODPRINTER.
                                     |org.joda.time.format|::PERIODPARSER.PARSE-INTO
                                     |org.joda.time.format|::PERIODPARSER.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.AFFIXES-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.PARSE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.SCAN
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.GET-AFFIXES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SIMPLEAFFIX.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.COUNT-FIELDS-TO-PRINT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.PARSE-INTO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$SEPARATOR.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-DOUBLE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-LONG
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING-INT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.THEN-COMPARING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.REVERSED
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.COMPARE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX$1.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.AFFIXES-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.PARSE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.SCAN
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.GET-AFFIXES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$REGEXAFFIX.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.AFFIXES-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.PARSE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.SCAN
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.GET-AFFIXES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PLURALAFFIX.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.AFFIXES-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.PARSE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.SCAN
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.GET-AFFIXES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$PERIODFIELDAFFIX.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.COUNT-FIELDS-TO-PRINT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.PARSE-INTO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$LITERAL.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.PARSE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.SCAN
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.GET-AFFIXES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$IGNORABLEAFFIX.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.COUNT-FIELDS-TO-PRINT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.PARSE-INTO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$FIELDFORMATTER.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.AFFIXES-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.FINISH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.PARSE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.SCAN
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.GET-AFFIXES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITEAFFIX.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.COUNT-FIELDS-TO-PRINT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.PARSE-INTO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER$COMPOSITE.
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.WAIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.CLEAR
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-PREFIX
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS-WITH-OPTIONAL-MILLIS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MINUTES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-HOURS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR-IF-FIELDS-AFTER
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-DAYS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-WEEKS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MONTHS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SUFFIX
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-YEARS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MILLIS-3-*DIGIT
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-MILLIS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS-WITH-MILLIS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SECONDS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-NEVER
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-IF-SUPPORTED
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-RARELY-FIRST
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-RARELY-LAST
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.REJECT-SIGNED-VALUES
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.MAXIMUM-PARSED-DIGITS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-PARSER
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-PRINTER
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-SEPARATOR-IF-FIELDS-BEFORE
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.MINIMUM-PRINTED-DIGITS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.PRINT-ZERO-ALWAYS
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.APPEND-LITERAL
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.TO-FORMATTER
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.NEW
                                     |org.joda.time.format|::PERIODFORMATTERBUILDER.
                                     |org.joda.time.format|::PERIODFORMATTER.PRINTER-PROP
                                     |org.joda.time.format|::PERIODFORMATTER.PARSER-PROP
                                     |org.joda.time.format|::PERIODFORMATTER.PARSE-TYPE-PROP
                                     |org.joda.time.format|::PERIODFORMATTER.LOCALE-PROP
                                     |org.joda.time.format|::PERIODFORMATTER.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMATTER.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMATTER.NOTIFY
                                     |org.joda.time.format|::PERIODFORMATTER.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMATTER.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMATTER.TO-STRING
                                     |org.joda.time.format|::PERIODFORMATTER.EQUALS
                                     |org.joda.time.format|::PERIODFORMATTER.WAIT
                                     |org.joda.time.format|::PERIODFORMATTER.PRINT
                                     |org.joda.time.format|::PERIODFORMATTER.GET-PARSE-TYPE
                                     |org.joda.time.format|::PERIODFORMATTER.PARSE-MUTABLE-PERIOD
                                     |org.joda.time.format|::PERIODFORMATTER.WITH-PARSE-TYPE
                                     |org.joda.time.format|::PERIODFORMATTER.PARSE-PERIOD
                                     |org.joda.time.format|::PERIODFORMATTER.GET-LOCALE
                                     |org.joda.time.format|::PERIODFORMATTER.GET-PARSER
                                     |org.joda.time.format|::PERIODFORMATTER.IS-PARSER
                                     |org.joda.time.format|::PERIODFORMATTER.GET-PRINTER
                                     |org.joda.time.format|::PERIODFORMATTER.IS-PRINTER
                                     |org.joda.time.format|::PERIODFORMATTER.PARSE-INTO
                                     |org.joda.time.format|::PERIODFORMATTER.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMATTER.WITH-LOCALE
                                     |org.joda.time.format|::PERIODFORMATTER.NEW
                                     |org.joda.time.format|::PERIODFORMATTER.
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.NOTIFY
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.TO-STRING
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.EQUALS
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.WAIT
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.COUNT-FIELDS-TO-PRINT
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.CALCULATE-PRINTED-LENGTH
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.PARSE-INTO
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.PRINT-TO
                                     |org.joda.time.format|::PERIODFORMAT$DYNAMICWORDBASED.
                                     |org.joda.time.format|::PERIODFORMAT.CLASS-PROP
                                     |org.joda.time.format|::PERIODFORMAT.NOTIFY-ALL
                                     |org.joda.time.format|::PERIODFORMAT.NOTIFY
                                     |org.joda.time.format|::PERIODFORMAT.GET-CLASS
                                     |org.joda.time.format|::PERIODFORMAT.HASH-CODE
                                     |org.joda.time.format|::PERIODFORMAT.TO-STRING
                                     |org.joda.time.format|::PERIODFORMAT.EQUALS
                                     |org.joda.time.format|::PERIODFORMAT.WAIT
                                     |org.joda.time.format|::PERIODFORMAT.GET-DEFAULT
                                     |org.joda.time.format|::PERIODFORMAT.WORD-BASED
                                     |org.joda.time.format|::PERIODFORMAT.
                                     |org.joda.time.format|::ISOPERIODFORMAT.CLASS-PROP
                                     |org.joda.time.format|::ISOPERIODFORMAT.NOTIFY-ALL
                                     |org.joda.time.format|::ISOPERIODFORMAT.NOTIFY
                                     |org.joda.time.format|::ISOPERIODFORMAT.GET-CLASS
                                     |org.joda.time.format|::ISOPERIODFORMAT.HASH-CODE
                                     |org.joda.time.format|::ISOPERIODFORMAT.TO-STRING
                                     |org.joda.time.format|::ISOPERIODFORMAT.EQUALS
                                     |org.joda.time.format|::ISOPERIODFORMAT.WAIT
                                     |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-EXTENDED-WITH-WEEKS
                                     |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-WITH-WEEKS
                                     |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE-EXTENDED
                                     |org.joda.time.format|::ISOPERIODFORMAT.ALTERNATE
                                     |org.joda.time.format|::ISOPERIODFORMAT.STANDARD
                                     |org.joda.time.format|::ISOPERIODFORMAT.
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.CLASS-PROP
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.NOTIFY-ALL
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.NOTIFY
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.GET-CLASS
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.HASH-CODE
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.TO-STRING
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.EQUALS
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.WAIT
                                     |org.joda.time.format|::ISODATETIMEFORMAT$CONSTANTS.
                                     |org.joda.time.format|::ISODATETIMEFORMAT.CLASS-PROP
                                     |org.joda.time.format|::ISODATETIMEFORMAT.NOTIFY-ALL
                                     |org.joda.time.format|::ISODATETIMEFORMAT.NOTIFY
                                     |org.joda.time.format|::ISODATETIMEFORMAT.GET-CLASS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.HASH-CODE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.TO-STRING
                                     |org.joda.time.format|::ISODATETIMEFORMAT.EQUALS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WAIT
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-TIME-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-OPTIONAL-TIME-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-DATE-OPTIONAL-TIME-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND-FRACTION
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE-SECOND
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR-MINUTE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-HOUR
                                     |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND-FRACTION
                                     |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE-SECOND
                                     |org.joda.time.format|::ISODATETIMEFORMAT.HOUR-MINUTE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.HOUR
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR-WEEK-DAY
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR-WEEK
                                     |org.joda.time.format|::ISODATETIMEFORMAT.YEAR-MONTH
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-WEEK-DATE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-ORDINAL-DATE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-DATE-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-T-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-T-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.BASIC-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.ORDINAL-DATE
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.T-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.YEAR-MONTH-DAY
                                     |org.joda.time.format|::ISODATETIMEFORMAT.FOR-FIELDS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.TIME-ELEMENT-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WEEK-DATE-TIME-NO-MILLIS
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-ELEMENT-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.DATE-TIME-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.WEEKYEAR
                                     |org.joda.time.format|::ISODATETIMEFORMAT.YEAR
                                     |org.joda.time.format|::ISODATETIMEFORMAT.LOCAL-DATE-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.T-TIME
                                     |org.joda.time.format|::ISODATETIMEFORMAT.TIME-PARSER
                                     |org.joda.time.format|::ISODATETIMEFORMAT.
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.CLASS-PROP
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.NOTIFY-ALL
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.NOTIFY
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.GET-CLASS
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.HASH-CODE
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.TO-STRING
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.WAIT
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.EQUALS
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.PRINT-TO
                                     |org.joda.time.format|::INTERNALPRINTERDATETIMEPRINTER.
                                     |org.joda.time.format|::INTERNALPRINTER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::INTERNALPRINTER.PRINT-TO
                                     |org.joda.time.format|::INTERNALPRINTER.
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.CLASS-PROP
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.NOTIFY-ALL
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.NOTIFY
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.GET-CLASS
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.HASH-CODE
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.TO-STRING
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.WAIT
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.EQUALS
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.PARSE-INTO
                                     |org.joda.time.format|::INTERNALPARSERDATETIMEPARSER.
                                     |org.joda.time.format|::INTERNALPARSER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::INTERNALPARSER.PARSE-INTO
                                     |org.joda.time.format|::INTERNALPARSER.
                                     |org.joda.time.format|::FORMATUTILS.CLASS-PROP
                                     |org.joda.time.format|::FORMATUTILS.NOTIFY-ALL
                                     |org.joda.time.format|::FORMATUTILS.NOTIFY
                                     |org.joda.time.format|::FORMATUTILS.GET-CLASS
                                     |org.joda.time.format|::FORMATUTILS.HASH-CODE
                                     |org.joda.time.format|::FORMATUTILS.TO-STRING
                                     |org.joda.time.format|::FORMATUTILS.EQUALS
                                     |org.joda.time.format|::FORMATUTILS.WAIT
                                     |org.joda.time.format|::FORMATUTILS.WRITE-PADDED-INTEGER
                                     |org.joda.time.format|::FORMATUTILS.WRITE-UNPADDED-INTEGER
                                     |org.joda.time.format|::FORMATUTILS.CALCULATE-DIGIT-COUNT
                                     |org.joda.time.format|::FORMATUTILS.APPEND-UNPADDED-INTEGER
                                     |org.joda.time.format|::FORMATUTILS.APPEND-PADDED-INTEGER
                                     |org.joda.time.format|::FORMATUTILS.
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.NOTIFY
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.TO-STRING
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.EQUALS
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.WAIT
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEPRINTERINTERNALPRINTER.
                                     |org.joda.time.format|::DATETIMEPRINTER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEPRINTER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEPRINTER.
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.NOTIFY
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.TO-STRING
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.EQUALS
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.WAIT
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEPARSERINTERNALPARSER.
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.NOTIFY
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.GET-CLASS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.HASH-CODE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.TO-STRING
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.EQUALS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.WAIT
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDSTATE.
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.NOTIFY
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.GET-CLASS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.HASH-CODE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.TO-STRING
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.EQUALS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.WAIT
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.COMPARE-TO
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET$SAVEDFIELD.
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.ZONE-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.PIVOT-YEAR-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-INTEGER-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.OFFSET-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.LOCALE-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.CHRONOLOGY-PROP
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.NOTIFY
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-CLASS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.HASH-CODE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.TO-STRING
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.EQUALS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.WAIT
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.SET-OFFSET
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-OFFSET
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.RESET
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.SET-ZONE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.RESTORE-STATE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.SAVE-STATE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.SAVE-FIELD
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.SET-PIVOT-YEAR
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-LOCALE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-OFFSET-INTEGER
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.COMPUTE-MILLIS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-PIVOT-YEAR
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.PARSE-MILLIS
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-ZONE
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.GET-CHRONOLOGY
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.NEW
                                     |org.joda.time.format|::DATETIMEPARSERBUCKET.
                                     |org.joda.time.format|::DATETIMEPARSER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEPARSER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEPARSER.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$UNPADDEDNUMBER.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TWODIGITYEAR.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEOFFSET.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONENAME.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.DECLARING-CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.CLASS-PROP
                                     |org.joda.time.format|::+DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*+
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.+INSTANCE+*
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ORDINAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.GET-DECLARING-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.COMPARE-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.NAME
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.VALUE-OF
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.VALUES
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TIMEZONEID.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$TEXTFIELD.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$STRINGLITERAL.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$PADDEDNUMBER.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$NUMBERFORMATTER.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$MATCHINGPARSER.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FRACTION.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$FIXEDNUMBER.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$COMPOSITE.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER$CHARACTERLITERAL.
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CLEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-PARSER
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-PRINTER
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.CAN-BUILD-FORMATTER
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-PARSER
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-PRINTER
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MILLIS-OF-SECOND
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-DAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-HOUR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-MINUTE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION-OF-SECOND
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FRACTION
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SHORT-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FIXED-SIGNED-DECIMAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SIGNED-DECIMAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-FIXED-DECIMAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DECIMAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-LITERAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-OPTIONAL
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.TO-FORMATTER
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-OFFSET
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-PATTERN
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-ID
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-SHORT-NAME
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TIME-ZONE-NAME
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-ERA-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR-SHORT-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK-SHORT-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HALFDAY-OF-DAY-TEXT
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CENTURY-OF-ERA
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR-OF-CENTURY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR-OF-ERA
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TWO-DIGIT-WEEKYEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-TWO-DIGIT-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MONTH-OF-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-WEEKYEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-WEEK-OF-WEEKYEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-MONTH
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-DAY-OF-WEEK
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CLOCKHOUR-OF-HALFDAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HOUR-OF-HALFDAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-CLOCKHOUR-OF-DAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-HOUR-OF-DAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MINUTE-OF-DAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MINUTE-OF-HOUR
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SECOND-OF-DAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-SECOND-OF-MINUTE
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.APPEND-MILLIS-OF-DAY
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.NEW
                                     |org.joda.time.format|::DATETIMEFORMATTERBUILDER.
                                     |org.joda.time.format|::DATETIMEFORMATTER.ZONE-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.PRINTER-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.PIVOT-YEAR-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSER-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.OFFSET-PARSED-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.LOCALE-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.DEFAULT-YEAR-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.CHRONOLOGY-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.CHRONOLGY-PROP
                                     |org.joda.time.format|::DATETIMEFORMATTER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMATTER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMATTER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMATTER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMATTER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMATTER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMATTER.PRINT
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-CHRONOLGY
                                     |org.joda.time.format|::DATETIMEFORMATTER.IS-OFFSET-PARSED
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-LOCALE
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-PARSER
                                     |org.joda.time.format|::DATETIMEFORMATTER.IS-PARSER
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-PRINTER
                                     |org.joda.time.format|::DATETIMEFORMATTER.IS-PRINTER
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-MUTABLE-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-TIME
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-LOCAL-DATE
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMATTER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-DEFAULT-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-DEFAULT-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-PIVOT-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-PIVOT-YEAR
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-MILLIS
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-ZONE-+UTC+
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-CHRONOLOGY
                                     |org.joda.time.format|::DATETIMEFORMATTER.PARSE-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-OFFSET-PARSED
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-ZONE
                                     |org.joda.time.format|::DATETIMEFORMATTER.WITH-LOCALE
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-ZONE
                                     |org.joda.time.format|::DATETIMEFORMATTER.GET-CHRONOLOGY
                                     |org.joda.time.format|::DATETIMEFORMATTER.NEW
                                     |org.joda.time.format|::DATETIMEFORMATTER.
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.WAIT
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.NEW
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTERCACHEKEY.
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.WAIT
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.ESTIMATE-PARSED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.ESTIMATE-PRINTED-LENGTH
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.PARSE-INTO
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.PRINT-TO
                                     |org.joda.time.format|::DATETIMEFORMAT$STYLEFORMATTER.
                                     |org.joda.time.format|::DATETIMEFORMAT.CLASS-PROP
                                     |org.joda.time.format|::DATETIMEFORMAT.NOTIFY-ALL
                                     |org.joda.time.format|::DATETIMEFORMAT.NOTIFY
                                     |org.joda.time.format|::DATETIMEFORMAT.GET-CLASS
                                     |org.joda.time.format|::DATETIMEFORMAT.HASH-CODE
                                     |org.joda.time.format|::DATETIMEFORMAT.TO-STRING
                                     |org.joda.time.format|::DATETIMEFORMAT.EQUALS
                                     |org.joda.time.format|::DATETIMEFORMAT.WAIT
                                     |org.joda.time.format|::DATETIMEFORMAT.SHORT-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.SHORT-DATE
                                     |org.joda.time.format|::DATETIMEFORMAT.LONG-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.LONG-DATE
                                     |org.joda.time.format|::DATETIMEFORMAT.FULL-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.FULL-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.MEDIUM-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.MEDIUM-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.FULL-DATE
                                     |org.joda.time.format|::DATETIMEFORMAT.LONG-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.PATTERN-FOR-STYLE
                                     |org.joda.time.format|::DATETIMEFORMAT.FOR-STYLE
                                     |org.joda.time.format|::DATETIMEFORMAT.FOR-PATTERN
                                     |org.joda.time.format|::DATETIMEFORMAT.MEDIUM-DATE
                                     |org.joda.time.format|::DATETIMEFORMAT.SHORT-DATE-TIME
                                     |org.joda.time.format|::DATETIMEFORMAT.) "org.joda.time.format"))
