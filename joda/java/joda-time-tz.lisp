(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "org.joda.time")
(FOIL:ENSURE-PACKAGE "org.joda.time.tz")
)(DEFCONSTANT |org.joda.time.tz|::CACHEDDATETIMEZONE.
   '|org.joda.time.tz|::|CachedDateTimeZone|)
(DEFCLASS |org.joda.time.tz|::CACHEDDATETIMEZONE.
          (|org.joda.time|::DATETIMEZONE.) NIL)
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.CachedDateTimeZone.getNameKey(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getNameKey"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.CachedDateTimeZone.getStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getStandardOffset"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.IS-FIXED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.CachedDateTimeZone.isFixed()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "isFixed"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.IS-FIXED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.NEXT-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.CachedDateTimeZone.nextTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "nextTransition"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.NEXT-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.PREVIOUS-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.CachedDateTimeZone.previousTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "previousTransition"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.PREVIOUS-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-UNCACHED-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.tz.CachedDateTimeZone.getUncachedZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getUncachedZone"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-UNCACHED-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-ZONE (&REST FOIL::ARGS)
  "public static org.joda.time.tz.CachedDateTimeZone org.joda.time.tz.CachedDateTimeZone.forZone(org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "forZone"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-ZONE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.CachedDateTimeZone.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "equals"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.CachedDateTimeZone.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "hashCode"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int org.joda.time.DateTimeZone.getOffset(org.joda.time.ReadableInstant)
public int org.joda.time.tz.CachedDateTimeZone.getOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "getOffset"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.TO-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone org.joda.time.DateTimeZone.toTimeZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "toTimeZone"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.TO-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-MILLIS-KEEP-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.getMillisKeepLocal(org.joda.time.DateTimeZone,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getMillisKeepLocal"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-MILLIS-KEEP-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.ADJUST-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.adjustOffset(long,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "adjustOffset"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.ADJUST-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.CONVERT-LOCAL-TO-+UTC+
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean)
public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "convertLocalToUTC"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.CONVERT-LOCAL-TO-+UTC+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-+ID+ (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forID(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "forID"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-+ID+ NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-HOURS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHours(int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "forOffsetHours"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-HOURS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-HOURS-MINUTES
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHoursMinutes(int,int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "forOffsetHoursMinutes"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-HOURS-MINUTES
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetMillis(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "forOffsetMillis"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-TIME-ZONE (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "forTimeZone"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-TIME-ZONE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-AVAILABLE-I-DS
       (&REST FOIL::ARGS)
  "public static java.util.Set org.joda.time.DateTimeZone.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getAvailableIDs"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-AVAILABLE-I-DS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.SET-PROVIDER (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setProvider(org.joda.time.tz.Provider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "setProvider"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.SET-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static org.joda.time.tz.NameProvider org.joda.time.DateTimeZone.getNameProvider()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getNameProvider"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.SET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setNameProvider(org.joda.time.tz.NameProvider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "setNameProvider"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.SET-NAME-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getShortName(long)
public java.lang.String org.joda.time.DateTimeZone.getShortName(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getShortName"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-SHORT-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.IS-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "isStandardOffset"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.IS-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-OFFSET-FROM-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.DateTimeZone.getOffsetFromLocal(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getOffsetFromLocal"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-OFFSET-FROM-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.CONVERT-+UTC+-TO-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertUTCToLocal(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "convertUTCToLocal"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.CONVERT-+UTC+-TO-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.IS-LOCAL-DATE-TIME-GAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isLocalDateTimeGap(org.joda.time.LocalDateTime)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "isLocalDateTimeGap"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.IS-LOCAL-DATE-TIME-GAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-PROVIDER (&REST FOIL::ARGS)
  "public static org.joda.time.tz.Provider org.joda.time.DateTimeZone.getProvider()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getProvider"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "toString"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.getName(long,java.util.Locale)
public final java.lang.String org.joda.time.DateTimeZone.getName(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "getName"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-DEFAULT (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.getDefault()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "getDefault"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-DEFAULT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.SET-DEFAULT (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setDefault(org.joda.time.DateTimeZone) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone|
                          "setDefault"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.SET-DEFAULT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-+ID+
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "getID"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-+ID+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "wait"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "getClass"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "notify"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone| "notifyAll"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.+UTC+* ()
  "public static final org.joda.time.DateTimeZone org.joda.time.DateTimeZone.UTC"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|CachedDateTimeZone| "UTC"
                    '|org.joda.time.tz|::CACHEDDATETIMEZONE.+UTC+* NIL))
(DEFUN (SETF |org.joda.time.tz|::CACHEDDATETIMEZONE.+UTC+*) (FOIL::VAL)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|CachedDateTimeZone| "UTC"
                    '|org.joda.time.tz|::CACHEDDATETIMEZONE.+UTC+* NIL
                    FOIL::VAL))
(DEFINE-SYMBOL-MACRO |org.joda.time.tz|::+CACHEDDATETIMEZONE.+UTC+*+
                     (|org.joda.time.tz|::CACHEDDATETIMEZONE.+UTC+*))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.+ID+-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|CachedDateTimeZone| "ID"
                       '|org.joda.time.tz|::CACHEDDATETIMEZONE.ID-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|CachedDateTimeZone| "class"
                       '|org.joda.time.tz|::CACHEDDATETIMEZONE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.FIXED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.CachedDateTimeZone.isFixed()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|CachedDateTimeZone| "fixed"
                       '|org.joda.time.tz|::CACHEDDATETIMEZONE.FIXED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE.UNCACHED-ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.tz.CachedDateTimeZone.getUncachedZone()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|CachedDateTimeZone| "uncachedZone"
                       '|org.joda.time.tz|::CACHEDDATETIMEZONE.UNCACHEDZONE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.
  '|org.joda.time.tz|::|CachedDateTimeZone$Info|)
(DEFCLASS |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO. (|java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.CachedDateTimeZone$Info.getNameKey(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "getNameKey"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.CachedDateTimeZone$Info.getStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "getStandardOffset"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.CachedDateTimeZone$Info.getOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "getOffset"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info| "wait"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "equals"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "toString"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "hashCode"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "getClass"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "notify"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                          "notifyAll"
                          '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-PERIOD-START* (FOIL::OBJ)
  "public final long org.joda.time.tz.CachedDateTimeZone$Info.iPeriodStart"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                    "iPeriodStart"
                    '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-PERIOD-START*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-PERIOD-START*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|CachedDateTimeZone$Info|
                    "iPeriodStart"
                    '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-PERIOD-START*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-ZONE-REF* (FOIL::OBJ)
  "public final org.joda.time.DateTimeZone org.joda.time.tz.CachedDateTimeZone$Info.iZoneRef"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|CachedDateTimeZone$Info| "iZoneRef"
                    '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-ZONE-REF*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-ZONE-REF*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|CachedDateTimeZone$Info| "iZoneRef"
                    '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-ZONE-REF*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|CachedDateTimeZone$Info| "class"
                       '|org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER.
  '|org.joda.time.tz|::|DateTimeZoneBuilder|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder()
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|DateTimeZoneBuilder| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.tz|::|DateTimeZoneBuilder|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::DATETIMEZONEBUILDER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.ADD-CUTOVER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder org.joda.time.tz.DateTimeZoneBuilder.addCutover(int,char,int,int,int,boolean,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder|
                          "addCutover"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.ADD-CUTOVER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.SET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder org.joda.time.tz.DateTimeZoneBuilder.setStandardOffset(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder|
                          "setStandardOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.SET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.ADD-RECURRING-SAVINGS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder org.joda.time.tz.DateTimeZoneBuilder.addRecurringSavings(java.lang.String,int,int,int,char,int,int,int,boolean,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder|
                          "addRecurringSavings"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.ADD-RECURRING-SAVINGS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.READ-FROM (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.tz.DateTimeZoneBuilder.readFrom(java.io.DataInput,java.lang.String) throws java.io.IOException
public static org.joda.time.DateTimeZone org.joda.time.tz.DateTimeZoneBuilder.readFrom(java.io.InputStream,java.lang.String) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "readFrom"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.READ-FROM
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.SET-FIXED-SAVINGS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder org.joda.time.tz.DateTimeZoneBuilder.setFixedSavings(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder|
                          "setFixedSavings"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.SET-FIXED-SAVINGS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.TO-DATE-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.tz.DateTimeZoneBuilder.toDateTimeZone(java.lang.String,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder|
                          "toDateTimeZone"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.TO-DATE-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.WRITE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder.writeTo(java.lang.String,java.io.DataOutput) throws java.io.IOException
public void org.joda.time.tz.DateTimeZoneBuilder.writeTo(java.lang.String,java.io.OutputStream) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "writeTo"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.WRITE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder| "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder| "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.
          (|org.joda.time|::DATETIMEZONE.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$DSTZone.getNameKey(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getNameKey"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$DSTZone.getStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getStandardOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-FIXED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$DSTZone.isFixed()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "isFixed"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-FIXED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NEXT-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$DSTZone.nextTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "nextTransition"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NEXT-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.PREVIOUS-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$DSTZone.previousTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "previousTransition"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.PREVIOUS-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.WRITE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$DSTZone.writeTo(java.io.DataOutput) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "writeTo"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.WRITE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$DSTZone.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int org.joda.time.DateTimeZone.getOffset(org.joda.time.ReadableInstant)
public int org.joda.time.tz.DateTimeZoneBuilder$DSTZone.getOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.TO-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone org.joda.time.DateTimeZone.toTimeZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "toTimeZone"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.TO-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-MILLIS-KEEP-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.getMillisKeepLocal(org.joda.time.DateTimeZone,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getMillisKeepLocal"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-MILLIS-KEEP-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.ADJUST-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.adjustOffset(long,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "adjustOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.ADJUST-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CONVERT-LOCAL-TO-+UTC+
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean)
public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "convertLocalToUTC"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CONVERT-LOCAL-TO-+UTC+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-+ID+
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forID(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "forID"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-+ID+
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-HOURS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHours(int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "forOffsetHours"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-HOURS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-HOURS-MINUTES
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHoursMinutes(int,int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "forOffsetHoursMinutes"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-HOURS-MINUTES
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetMillis(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "forOffsetMillis"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-TIME-ZONE
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "forTimeZone"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-TIME-ZONE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-AVAILABLE-I-DS
       (&REST FOIL::ARGS)
  "public static java.util.Set org.joda.time.DateTimeZone.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getAvailableIDs"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-AVAILABLE-I-DS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-PROVIDER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setProvider(org.joda.time.tz.Provider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "setProvider"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static org.joda.time.tz.NameProvider org.joda.time.DateTimeZone.getNameProvider()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getNameProvider"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setNameProvider(org.joda.time.tz.NameProvider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "setNameProvider"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-NAME-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getShortName(long)
public java.lang.String org.joda.time.DateTimeZone.getShortName(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getShortName"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-SHORT-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "isStandardOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-OFFSET-FROM-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.DateTimeZone.getOffsetFromLocal(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getOffsetFromLocal"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-OFFSET-FROM-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CONVERT-+UTC+-TO-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertUTCToLocal(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "convertUTCToLocal"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CONVERT-+UTC+-TO-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-LOCAL-DATE-TIME-GAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isLocalDateTimeGap(org.joda.time.LocalDateTime)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "isLocalDateTimeGap"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-LOCAL-DATE-TIME-GAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-PROVIDER
       (&REST FOIL::ARGS)
  "public static org.joda.time.tz.Provider org.joda.time.DateTimeZone.getProvider()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getProvider"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.DateTimeZone.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.getName(long,java.util.Locale)
public final java.lang.String org.joda.time.DateTimeZone.getName(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getName"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-DEFAULT
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.getDefault()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getDefault"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-DEFAULT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-DEFAULT
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setDefault(org.joda.time.DateTimeZone) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "setDefault"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-DEFAULT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-+ID+
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getID"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-+ID+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+UTC+* ()
  "public static final org.joda.time.DateTimeZone org.joda.time.DateTimeZone.UTC"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone| "UTC"
                    '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+UTC+*
                    NIL))
(DEFUN (SETF |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+UTC+*)
       (FOIL::VAL)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone| "UTC"
                    '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+UTC+* NIL
                    FOIL::VAL))
(DEFINE-SYMBOL-MACRO |org.joda.time.tz|::+DATETIMEZONEBUILDER$DSTZONE.+UTC+*+
                     (|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+UTC+*))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+ID+-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone| "ID"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.ID-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                       "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FIXED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DateTimeZone.isFixed()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$DSTZone|
                       "fixed"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FIXED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.SET-INSTANT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$OfYear.setInstant(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "setInstant"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.SET-INSTANT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.WRITE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$OfYear.writeTo(java.io.DataOutput) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "writeTo"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.WRITE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$OfYear.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$OfYear.next(long,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "next"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.PREVIOUS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$OfYear.previous(long,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "previous"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.PREVIOUS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$OfYear|
                       "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.
          (|org.joda.time|::DATETIMEZONE.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.getNameKey(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getNameKey"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME-KEY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.getStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "getStandardOffset"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-STANDARD-OFFSET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-FIXED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.isFixed()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "isFixed"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-FIXED
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NEXT-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.nextTransition(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "nextTransition"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NEXT-TRANSITION
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.PREVIOUS-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.previousTransition(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "previousTransition"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.PREVIOUS-TRANSITION
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-CACHABLE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.isCachable()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "isCachable"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-CACHABLE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.WRITE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.writeTo(java.io.DataOutput) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "writeTo"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.WRITE-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "equals"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int org.joda.time.DateTimeZone.getOffset(org.joda.time.ReadableInstant)
public int org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.getOffset(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getOffset"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-OFFSET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.TO-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone org.joda.time.DateTimeZone.toTimeZone()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "toTimeZone"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.TO-TIME-ZONE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-MILLIS-KEEP-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.getMillisKeepLocal(org.joda.time.DateTimeZone,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "getMillisKeepLocal"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-MILLIS-KEEP-LOCAL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.ADJUST-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.adjustOffset(long,boolean)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "adjustOffset"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.ADJUST-OFFSET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CONVERT-LOCAL-TO-+UTC+
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean)
public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "convertLocalToUTC"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CONVERT-LOCAL-TO-+UTC+
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-+ID+
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forID(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "forID"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-+ID+ NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-HOURS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHours(int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "forOffsetHours"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-HOURS
   NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-HOURS-MINUTES
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHoursMinutes(int,int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "forOffsetHoursMinutes"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-HOURS-MINUTES
   NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetMillis(int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "forOffsetMillis"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-MILLIS
   NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-TIME-ZONE
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "forTimeZone"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-TIME-ZONE NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-AVAILABLE-I-DS
       (&REST FOIL::ARGS)
  "public static java.util.Set org.joda.time.DateTimeZone.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "getAvailableIDs"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-AVAILABLE-I-DS
   NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-PROVIDER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setProvider(org.joda.time.tz.Provider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "setProvider"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-PROVIDER NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static org.joda.time.tz.NameProvider org.joda.time.DateTimeZone.getNameProvider()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "getNameProvider"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME-PROVIDER
   NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setNameProvider(org.joda.time.tz.NameProvider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "setNameProvider"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-NAME-PROVIDER
   NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getShortName(long)
public java.lang.String org.joda.time.DateTimeZone.getShortName(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getShortName"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-SHORT-NAME
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "isStandardOffset"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-STANDARD-OFFSET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-OFFSET-FROM-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.DateTimeZone.getOffsetFromLocal(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "getOffsetFromLocal"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-OFFSET-FROM-LOCAL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CONVERT-+UTC+-TO-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertUTCToLocal(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "convertUTCToLocal"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CONVERT-+UTC+-TO-LOCAL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-LOCAL-DATE-TIME-GAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isLocalDateTimeGap(org.joda.time.LocalDateTime)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone|
   "isLocalDateTimeGap"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-LOCAL-DATE-TIME-GAP
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-PROVIDER
       (&REST FOIL::ARGS)
  "public static org.joda.time.tz.Provider org.joda.time.DateTimeZone.getProvider()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getProvider"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-PROVIDER NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "toString"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.DateTimeZone.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "hashCode"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.getName(long,java.util.Locale)
public final java.lang.String org.joda.time.DateTimeZone.getName(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getName"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-DEFAULT
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.getDefault()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getDefault"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-DEFAULT NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-DEFAULT
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setDefault(org.joda.time.DateTimeZone) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "setDefault"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-DEFAULT NIL
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-+ID+
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getID"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-+ID+
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "wait"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "getClass"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "notify"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "notifyAll"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+* ()
  "public static final org.joda.time.DateTimeZone org.joda.time.DateTimeZone.UTC"
  (FOIL::CALL-FIELD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "UTC"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+* NIL))
(DEFUN (SETF |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+*)
       (FOIL::VAL)
  (FOIL::CALL-FIELD
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "UTC"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+* NIL
   FOIL::VAL))
(DEFINE-SYMBOL-MACRO
 |org.joda.time.tz|::+DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+*+
 (|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+*))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+ID+-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "ID"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.ID-GET FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CACHABLE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone.isCachable()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "cachable"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CACHABLE-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "class"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FIXED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DateTimeZone.isFixed()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.tz|::|DateTimeZoneBuilder$PrecalculatedZone| "fixed"
   '|org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FIXED-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$Recurrence.getNameKey()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "getNameKey"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-SAVE-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Recurrence.getSaveMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "getSaveMillis"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-SAVE-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$OfYear org.joda.time.tz.DateTimeZoneBuilder$Recurrence.getOfYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "getOfYear"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.WRITE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$Recurrence.writeTo(java.io.DataOutput) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "writeTo"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.WRITE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$Recurrence.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$Recurrence.next(long,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "next"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.PREVIOUS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$Recurrence.previous(long,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "previous"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.PREVIOUS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                       "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NAME-KEY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$Recurrence.getNameKey()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                       "nameKey"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NAMEKEY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.OF-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$OfYear org.joda.time.tz.DateTimeZoneBuilder$Recurrence.getOfYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                       "ofYear"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.OFYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.SAVE-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Recurrence.getSaveMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Recurrence|
                       "saveMillis"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.SAVEMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE. (|java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$Rule.getNameKey()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "getNameKey"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-FROM-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Rule.getFromYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "getFromYear"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-FROM-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-TO-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Rule.getToYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "getToYear"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-TO-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-SAVE-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Rule.getSaveMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "getSaveMillis"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-SAVE-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-OF-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$OfYear org.joda.time.tz.DateTimeZoneBuilder$Rule.getOfYear()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "getOfYear"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-OF-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$Rule.next(long,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "next"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule| "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.FROM-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Rule.getFromYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                       "fromYear"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.FROMYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NAME-KEY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$Rule.getNameKey()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                       "nameKey"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NAMEKEY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.OF-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$OfYear org.joda.time.tz.DateTimeZoneBuilder$Rule.getOfYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule| "ofYear"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.OFYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.SAVE-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Rule.getSaveMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule|
                       "saveMillis"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.SAVEMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.TO-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Rule.getToYear()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Rule| "toYear"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.TOYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$RuleSet.getStandardOffset()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "getStandardOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NEXT-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$Transition org.joda.time.tz.DateTimeZoneBuilder$RuleSet.nextTransition(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "nextTransition"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NEXT-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-UPPER-LIMIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$RuleSet.setUpperLimit(int,org.joda.time.tz.DateTimeZoneBuilder$OfYear)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "setUpperLimit"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-UPPER-LIMIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-UPPER-LIMIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$RuleSet.getUpperLimit(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "getUpperLimit"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-UPPER-LIMIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$RuleSet.setStandardOffset(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "setStandardOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.ADD-RULE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$RuleSet.addRule(org.joda.time.tz.DateTimeZoneBuilder$Rule)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "addRule"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.ADD-RULE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.FIRST-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$Transition org.joda.time.tz.DateTimeZoneBuilder$RuleSet.firstTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "firstTransition"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.FIRST-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.BUILD-TAIL-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.tz.DateTimeZoneBuilder$DSTZone org.joda.time.tz.DateTimeZoneBuilder$RuleSet.buildTailZone(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "buildTailZone"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.BUILD-TAIL-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-FIXED-SAVINGS
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$RuleSet.setFixedSavings(java.lang.String,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "setFixedSavings"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-FIXED-SAVINGS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                       "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.STANDARD-OFFSET-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$RuleSet.getStandardOffset()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                       "standardOffset"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.STANDARDOFFSET-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.STANDARD-OFFSET-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.DateTimeZoneBuilder$RuleSet.setStandardOffset(int)
"
  (FOIL::CALL-PROP-SET '|org.joda.time.tz|::|DateTimeZoneBuilder$RuleSet|
                       "standardOffset"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.STANDARDOFFSET-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFCONSTANT |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.
  '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|)
(DEFCLASS |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$Transition.getMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "getMillis"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$Transition.getNameKey()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "getNameKey"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Transition.getStandardOffset()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "getStandardOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-SAVE-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Transition.getSaveMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "getSaveMillis"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-SAVE-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-WALL-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Transition.getWallOffset()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "getWallOffset"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-WALL-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.IS-TRANSITION-FROM
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.DateTimeZoneBuilder$Transition.isTransitionFrom(org.joda.time.tz.DateTimeZoneBuilder$Transition)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "isTransitionFrom"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.IS-TRANSITION-FROM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "wait"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "equals"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "toString"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "hashCode"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "getClass"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "notify"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                          "notifyAll"
                          '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                       "class"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.DateTimeZoneBuilder$Transition.getMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                       "millis"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.MILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NAME-KEY-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DateTimeZoneBuilder$Transition.getNameKey()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                       "nameKey"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NAMEKEY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.SAVE-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Transition.getSaveMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                       "saveMillis"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.SAVEMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.STANDARD-OFFSET-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Transition.getStandardOffset()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                       "standardOffset"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.STANDARDOFFSET-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.WALL-OFFSET-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.DateTimeZoneBuilder$Transition.getWallOffset()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DateTimeZoneBuilder$Transition|
                       "wallOffset"
                       '|org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.WALLOFFSET-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::DEFAULTNAMEPROVIDER.
  '|org.joda.time.tz|::|DefaultNameProvider|)
(DEFCLASS |org.joda.time.tz|::DEFAULTNAMEPROVIDER.
          (|org.joda.time.tz|::NAMEPROVIDER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.DefaultNameProvider()
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|DefaultNameProvider| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.tz|::|DefaultNameProvider|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::DEFAULTNAMEPROVIDER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DefaultNameProvider.getShortName(java.util.Locale,java.lang.String,java.lang.String)
public java.lang.String org.joda.time.tz.DefaultNameProvider.getShortName(java.util.Locale,java.lang.String,java.lang.String,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider|
                          "getShortName"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-SHORT-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.DefaultNameProvider.getName(java.util.Locale,java.lang.String,java.lang.String)
public java.lang.String org.joda.time.tz.DefaultNameProvider.getName(java.util.Locale,java.lang.String,java.lang.String,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "getName"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "wait"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "equals"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "toString"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "hashCode"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "getClass"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider| "notify"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|DefaultNameProvider|
                          "notifyAll"
                          '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::DEFAULTNAMEPROVIDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|DefaultNameProvider| "class"
                       '|org.joda.time.tz|::DEFAULTNAMEPROVIDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::FIXEDDATETIMEZONE.
  '|org.joda.time.tz|::|FixedDateTimeZone|)
(DEFCLASS |org.joda.time.tz|::FIXEDDATETIMEZONE.
          (|org.joda.time|::DATETIMEZONE.) NIL)
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.FixedDateTimeZone(java.lang.String,java.lang.String,int,int)
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|FixedDateTimeZone| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.tz|::|FixedDateTimeZone|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::FIXEDDATETIMEZONE.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.TO-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone org.joda.time.tz.FixedDateTimeZone.toTimeZone()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "toTimeZone"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.TO-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME-KEY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.FixedDateTimeZone.getNameKey(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "getNameKey"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME-KEY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.FixedDateTimeZone.getStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getStandardOffset"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-OFFSET-FROM-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.FixedDateTimeZone.getOffsetFromLocal(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getOffsetFromLocal"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-OFFSET-FROM-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.IS-FIXED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.FixedDateTimeZone.isFixed()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "isFixed"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.IS-FIXED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.NEXT-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.FixedDateTimeZone.nextTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "nextTransition"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.NEXT-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.PREVIOUS-TRANSITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.tz.FixedDateTimeZone.previousTransition(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "previousTransition"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.PREVIOUS-TRANSITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.FixedDateTimeZone.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "equals"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.tz.FixedDateTimeZone.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "hashCode"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public final int org.joda.time.DateTimeZone.getOffset(org.joda.time.ReadableInstant)
public int org.joda.time.tz.FixedDateTimeZone.getOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "getOffset"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-MILLIS-KEEP-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.getMillisKeepLocal(org.joda.time.DateTimeZone,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getMillisKeepLocal"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-MILLIS-KEEP-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.ADJUST-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.adjustOffset(long,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "adjustOffset"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.ADJUST-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.CONVERT-LOCAL-TO-+UTC+
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean)
public long org.joda.time.DateTimeZone.convertLocalToUTC(long,boolean,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "convertLocalToUTC"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.CONVERT-LOCAL-TO-+UTC+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-+ID+ (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forID(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "forID"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-+ID+ NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-HOURS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHours(int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "forOffsetHours"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-HOURS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-HOURS-MINUTES
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetHoursMinutes(int,int) throws java.lang.IllegalArgumentException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "forOffsetHoursMinutes"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-HOURS-MINUTES
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-MILLIS
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forOffsetMillis(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "forOffsetMillis"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-MILLIS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-TIME-ZONE (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.forTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "forTimeZone"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-TIME-ZONE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-AVAILABLE-I-DS
       (&REST FOIL::ARGS)
  "public static java.util.Set org.joda.time.DateTimeZone.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getAvailableIDs"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-AVAILABLE-I-DS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.SET-PROVIDER (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setProvider(org.joda.time.tz.Provider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "setProvider"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.SET-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static org.joda.time.tz.NameProvider org.joda.time.DateTimeZone.getNameProvider()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getNameProvider"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.SET-NAME-PROVIDER
       (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setNameProvider(org.joda.time.tz.NameProvider) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "setNameProvider"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.SET-NAME-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getShortName(long)
public java.lang.String org.joda.time.DateTimeZone.getShortName(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getShortName"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-SHORT-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.IS-STANDARD-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isStandardOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "isStandardOffset"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.IS-STANDARD-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.CONVERT-+UTC+-TO-LOCAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeZone.convertUTCToLocal(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "convertUTCToLocal"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.CONVERT-+UTC+-TO-LOCAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.IS-LOCAL-DATE-TIME-GAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.DateTimeZone.isLocalDateTimeGap(org.joda.time.LocalDateTime)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "isLocalDateTimeGap"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.IS-LOCAL-DATE-TIME-GAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-PROVIDER (&REST FOIL::ARGS)
  "public static org.joda.time.tz.Provider org.joda.time.DateTimeZone.getProvider()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone|
                          "getProvider"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-PROVIDER
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "toString"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.DateTimeZone.getName(long,java.util.Locale)
public final java.lang.String org.joda.time.DateTimeZone.getName(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "getName"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-DEFAULT (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeZone org.joda.time.DateTimeZone.getDefault()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "getDefault"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-DEFAULT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.SET-DEFAULT (&REST FOIL::ARGS)
  "public static void org.joda.time.DateTimeZone.setDefault(org.joda.time.DateTimeZone) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "setDefault"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.SET-DEFAULT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-+ID+
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "getID"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-+ID+
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "wait"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "getClass"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "notify"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|FixedDateTimeZone| "notifyAll"
                          '|org.joda.time.tz|::FIXEDDATETIMEZONE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.+UTC+* ()
  "public static final org.joda.time.DateTimeZone org.joda.time.DateTimeZone.UTC"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|FixedDateTimeZone| "UTC"
                    '|org.joda.time.tz|::FIXEDDATETIMEZONE.+UTC+* NIL))
(DEFUN (SETF |org.joda.time.tz|::FIXEDDATETIMEZONE.+UTC+*) (FOIL::VAL)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|FixedDateTimeZone| "UTC"
                    '|org.joda.time.tz|::FIXEDDATETIMEZONE.+UTC+* NIL
                    FOIL::VAL))
(DEFINE-SYMBOL-MACRO |org.joda.time.tz|::+FIXEDDATETIMEZONE.+UTC+*+
                     (|org.joda.time.tz|::FIXEDDATETIMEZONE.+UTC+*))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.+ID+-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.DateTimeZone.getID()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|FixedDateTimeZone| "ID"
                       '|org.joda.time.tz|::FIXEDDATETIMEZONE.ID-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|FixedDateTimeZone| "class"
                       '|org.joda.time.tz|::FIXEDDATETIMEZONE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::FIXEDDATETIMEZONE.FIXED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.tz.FixedDateTimeZone.isFixed()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|FixedDateTimeZone| "fixed"
                       '|org.joda.time.tz|::FIXEDDATETIMEZONE.FIXED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::NAMEPROVIDER.
  '|org.joda.time.tz|::|NameProvider|)
(DEFCLASS |org.joda.time.tz|::NAMEPROVIDER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::NAMEPROVIDER.GET-SHORT-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String org.joda.time.tz.NameProvider.getShortName(java.util.Locale,java.lang.String,java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|NameProvider| "getShortName"
                          '|org.joda.time.tz|::NAMEPROVIDER.GET-SHORT-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::NAMEPROVIDER.GET-NAME (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String org.joda.time.tz.NameProvider.getName(java.util.Locale,java.lang.String,java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|NameProvider| "getName"
                          '|org.joda.time.tz|::NAMEPROVIDER.GET-NAME FOIL::THIS
                          FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::PROVIDER. '|org.joda.time.tz|::|Provider|)
(DEFCLASS |org.joda.time.tz|::PROVIDER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::PROVIDER.GET-ZONE (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DateTimeZone org.joda.time.tz.Provider.getZone(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|Provider| "getZone"
                          '|org.joda.time.tz|::PROVIDER.GET-ZONE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::PROVIDER.GET-AVAILABLE-I-DS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Set org.joda.time.tz.Provider.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|Provider| "getAvailableIDs"
                          '|org.joda.time.tz|::PROVIDER.GET-AVAILABLE-I-DS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::PROVIDER.AVAILABLE-I-DS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Set org.joda.time.tz.Provider.getAvailableIDs()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|Provider| "availableIDs"
                       '|org.joda.time.tz|::PROVIDER.AVAILABLEIDS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::UTCPROVIDER.
  '|org.joda.time.tz|::|UTCProvider|)
(DEFCLASS |org.joda.time.tz|::UTCPROVIDER.
          (|org.joda.time.tz|::PROVIDER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::UTCPROVIDER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.UTCProvider()
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|UTCProvider| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |org.joda.time.tz|::|UTCProvider|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::UTCPROVIDER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.GET-ZONE (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.tz.UTCProvider.getZone(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "getZone"
                          '|org.joda.time.tz|::UTCPROVIDER.GET-ZONE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.GET-AVAILABLE-I-DS
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set org.joda.time.tz.UTCProvider.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "getAvailableIDs"
                          '|org.joda.time.tz|::UTCPROVIDER.GET-AVAILABLE-I-DS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "wait"
                          '|org.joda.time.tz|::UTCPROVIDER.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "equals"
                          '|org.joda.time.tz|::UTCPROVIDER.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "toString"
                          '|org.joda.time.tz|::UTCPROVIDER.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "hashCode"
                          '|org.joda.time.tz|::UTCPROVIDER.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "getClass"
                          '|org.joda.time.tz|::UTCPROVIDER.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "notify"
                          '|org.joda.time.tz|::UTCPROVIDER.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|UTCProvider| "notifyAll"
                          '|org.joda.time.tz|::UTCPROVIDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.AVAILABLE-I-DS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set org.joda.time.tz.UTCProvider.getAvailableIDs()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|UTCProvider| "availableIDs"
                       '|org.joda.time.tz|::UTCPROVIDER.AVAILABLEIDS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::UTCPROVIDER.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|UTCProvider| "class"
                       '|org.joda.time.tz|::UTCPROVIDER.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOCOMPILER.
  '|org.joda.time.tz|::|ZoneInfoCompiler|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOCOMPILER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.ZoneInfoCompiler()
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|ZoneInfoCompiler| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.tz|::|ZoneInfoCompiler|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::ZONEINFOCOMPILER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.PARSE-DATA-FILE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.ZoneInfoCompiler.parseDataFile(java.io.BufferedReader,boolean) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler|
                          "parseDataFile"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.PARSE-DATA-FILE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.MAIN (&REST FOIL::ARGS)
  "public static void org.joda.time.tz.ZoneInfoCompiler.main(java.lang.String[]) throws java.lang.Exception
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "main"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.MAIN NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.COMPILE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Map org.joda.time.tz.ZoneInfoCompiler.compile(java.io.File,java.io.File[]) throws java.io.IOException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "compile"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.COMPILE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "wait"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "equals"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "toString"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "hashCode"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "getClass"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "notify"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler| "notifyAll"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoCompiler| "class"
                       '|org.joda.time.tz|::ZONEINFOCOMPILER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.
  '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.ADD-RECURRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.addRecurring(org.joda.time.tz.DateTimeZoneBuilder,java.lang.String,int,int,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "addRecurring"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.ADD-RECURRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.ADD-CUTOVER
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.addCutover(org.joda.time.tz.DateTimeZoneBuilder,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "addCutover"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.ADD-CUTOVER FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "toString"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "wait"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "equals"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "hashCode"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "getClass"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "notify"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear| "notifyAll"
   '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MONTH-OF-YEAR*
       (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.iMonthOfYear"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iMonthOfYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MONTH-OF-YEAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MONTH-OF-YEAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iMonthOfYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MONTH-OF-YEAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-MONTH*
       (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.iDayOfMonth"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iDayOfMonth"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-MONTH*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-MONTH*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iDayOfMonth"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-MONTH*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-WEEK*
       (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.iDayOfWeek"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iDayOfWeek"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-WEEK*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-WEEK*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iDayOfWeek"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-WEEK*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ADVANCE-DAY-OF-WEEK*
       (FOIL::OBJ)
  "public final boolean org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.iAdvanceDayOfWeek"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iAdvanceDayOfWeek"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ADVANCE-DAY-OF-WEEK*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ADVANCE-DAY-OF-WEEK*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iAdvanceDayOfWeek"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ADVANCE-DAY-OF-WEEK*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MILLIS-OF-DAY*
       (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.iMillisOfDay"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iMillisOfDay"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MILLIS-OF-DAY*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MILLIS-OF-DAY*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iMillisOfDay"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MILLIS-OF-DAY*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ZONE-CHAR*
       (FOIL::OBJ)
  "public final char org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear.iZoneChar"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iZoneChar"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ZONE-CHAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ZONE-CHAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                    "iZoneChar"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ZONE-CHAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoCompiler$DateTimeOfYear|
                       "class"
                       '|org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.
  '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOCOMPILER$RULE. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.ADD-RECURRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.ZoneInfoCompiler$Rule.addRecurring(org.joda.time.tz.DateTimeZoneBuilder,java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                          "addRecurring"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.ADD-RECURRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.ZoneInfoCompiler$Rule.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                          "toString"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "wait"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "equals"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                          "hashCode"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                          "getClass"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "notify"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                          "notifyAll"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-NAME* (FOIL::OBJ)
  "public final java.lang.String org.joda.time.tz.ZoneInfoCompiler$Rule.iName"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iName"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-NAME*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-NAME*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iName"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-NAME*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-FROM-YEAR* (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$Rule.iFromYear"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iFromYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-FROM-YEAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-FROM-YEAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iFromYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-FROM-YEAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TO-YEAR* (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$Rule.iToYear"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iToYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TO-YEAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TO-YEAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iToYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TO-YEAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TYPE* (FOIL::OBJ)
  "public final java.lang.String org.joda.time.tz.ZoneInfoCompiler$Rule.iType"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iType"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TYPE*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TYPE*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iType"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TYPE*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-DATE-TIME-OF-YEAR*
       (FOIL::OBJ)
  "public final org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear org.joda.time.tz.ZoneInfoCompiler$Rule.iDateTimeOfYear"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                    "iDateTimeOfYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-DATE-TIME-OF-YEAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-DATE-TIME-OF-YEAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule|
                    "iDateTimeOfYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-DATE-TIME-OF-YEAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-SAVE-MILLIS* (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$Rule.iSaveMillis"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iSaveMillis"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-SAVE-MILLIS*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-SAVE-MILLIS*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iSaveMillis"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-SAVE-MILLIS*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-LETTER-S* (FOIL::OBJ)
  "public final java.lang.String org.joda.time.tz.ZoneInfoCompiler$Rule.iLetterS"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iLetterS"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-LETTER-S*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-LETTER-S*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "iLetterS"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-LETTER-S*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoCompiler$Rule| "class"
                       '|org.joda.time.tz|::ZONEINFOCOMPILER$RULE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.
  '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET. (|java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.ADD-RECURRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.ZoneInfoCompiler$RuleSet.addRecurring(org.joda.time.tz.DateTimeZoneBuilder,java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "addRecurring"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.ADD-RECURRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "wait"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "equals"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "toString"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "hashCode"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "getClass"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "notify"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet|
                          "notifyAll"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoCompiler$RuleSet| "class"
                       '|org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.
  '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.ADD-TO-BUILDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.tz.ZoneInfoCompiler$Zone.addToBuilder(org.joda.time.tz.DateTimeZoneBuilder,java.util.Map)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                          "addToBuilder"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.ADD-TO-BUILDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.tz.ZoneInfoCompiler$Zone.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                          "toString"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "wait"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "equals"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                          "hashCode"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                          "getClass"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "notify"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                          "notifyAll"
                          '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-NAME* (FOIL::OBJ)
  "public final java.lang.String org.joda.time.tz.ZoneInfoCompiler$Zone.iName"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iName"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-NAME*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-NAME*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iName"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-NAME*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-OFFSET-MILLIS* (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$Zone.iOffsetMillis"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                    "iOffsetMillis"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-OFFSET-MILLIS*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-OFFSET-MILLIS*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                    "iOffsetMillis"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-OFFSET-MILLIS*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-RULES* (FOIL::OBJ)
  "public final java.lang.String org.joda.time.tz.ZoneInfoCompiler$Zone.iRules"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iRules"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-RULES*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-RULES*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iRules"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-RULES*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-FORMAT* (FOIL::OBJ)
  "public final java.lang.String org.joda.time.tz.ZoneInfoCompiler$Zone.iFormat"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iFormat"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-FORMAT*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-FORMAT*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iFormat"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-FORMAT*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-YEAR* (FOIL::OBJ)
  "public final int org.joda.time.tz.ZoneInfoCompiler$Zone.iUntilYear"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iUntilYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-YEAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-YEAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "iUntilYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-YEAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-DATE-TIME-OF-YEAR*
       (FOIL::OBJ)
  "public final org.joda.time.tz.ZoneInfoCompiler$DateTimeOfYear org.joda.time.tz.ZoneInfoCompiler$Zone.iUntilDateTimeOfYear"
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                    "iUntilDateTimeOfYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-DATE-TIME-OF-YEAR*
                    FOIL::OBJ))
(DEFUN (SETF |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-DATE-TIME-OF-YEAR*)
       (FOIL::VAL FOIL::OBJ)
  (FOIL::CALL-FIELD '|org.joda.time.tz|::|ZoneInfoCompiler$Zone|
                    "iUntilDateTimeOfYear"
                    '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-DATE-TIME-OF-YEAR*
                    FOIL::OBJ FOIL::VAL))
(DEFUN |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoCompiler$Zone| "class"
                       '|org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOLOGGER.
  '|org.joda.time.tz|::|ZoneInfoLogger|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOLOGGER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.ZoneInfoLogger()
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|ZoneInfoLogger| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.tz|::|ZoneInfoLogger|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::ZONEINFOLOGGER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.VERBOSE (&REST FOIL::ARGS)
  "public static boolean org.joda.time.tz.ZoneInfoLogger.verbose()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "verbose"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.VERBOSE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.SET (&REST FOIL::ARGS)
  "public static void org.joda.time.tz.ZoneInfoLogger.set(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "set"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.SET NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "wait"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "equals"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "toString"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "hashCode"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "getClass"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "notify"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoLogger| "notifyAll"
                          '|org.joda.time.tz|::ZONEINFOLOGGER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOLOGGER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoLogger| "class"
                       '|org.joda.time.tz|::ZONEINFOLOGGER.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.tz|::ZONEINFOPROVIDER.
  '|org.joda.time.tz|::|ZoneInfoProvider|)
(DEFCLASS |org.joda.time.tz|::ZONEINFOPROVIDER.
          (|org.joda.time.tz|::PROVIDER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.NEW (&REST FOIL::ARGS)
  "public org.joda.time.tz.ZoneInfoProvider(java.lang.String) throws java.io.IOException
public org.joda.time.tz.ZoneInfoProvider(java.io.File) throws java.io.IOException
public org.joda.time.tz.ZoneInfoProvider(java.lang.String,java.lang.ClassLoader) throws java.io.IOException
"
  (FOIL::CALL-CTOR '|org.joda.time.tz|::|ZoneInfoProvider| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.tz|::|ZoneInfoProvider|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.tz|::ZONEINFOPROVIDER.NEW FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.GET-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeZone org.joda.time.tz.ZoneInfoProvider.getZone(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "getZone"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.GET-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.GET-AVAILABLE-I-DS
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set org.joda.time.tz.ZoneInfoProvider.getAvailableIDs()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider|
                          "getAvailableIDs"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.GET-AVAILABLE-I-DS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "wait"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "equals"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "toString"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "hashCode"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "getClass"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "notify"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.tz|::|ZoneInfoProvider| "notifyAll"
                          '|org.joda.time.tz|::ZONEINFOPROVIDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.AVAILABLE-I-DS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Set org.joda.time.tz.ZoneInfoProvider.getAvailableIDs()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoProvider| "availableIDs"
                       '|org.joda.time.tz|::ZONEINFOPROVIDER.AVAILABLEIDS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.tz|::ZONEINFOPROVIDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.tz|::|ZoneInfoProvider| "class"
                       '|org.joda.time.tz|::ZONEINFOPROVIDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(eval-when (:load-toplevel)(export '(|org.joda.time.tz|::ZONEINFOPROVIDER.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.AVAILABLE-I-DS-PROP
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.EQUALS
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.WAIT
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.GET-ZONE
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.NEW
                                     |org.joda.time.tz|::ZONEINFOPROVIDER.
                                     |org.joda.time.tz|::ZONEINFOLOGGER.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOLOGGER.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOLOGGER.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOLOGGER.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOLOGGER.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOLOGGER.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOLOGGER.EQUALS
                                     |org.joda.time.tz|::ZONEINFOLOGGER.WAIT
                                     |org.joda.time.tz|::ZONEINFOLOGGER.SET
                                     |org.joda.time.tz|::ZONEINFOLOGGER.VERBOSE
                                     |org.joda.time.tz|::ZONEINFOLOGGER.NEW
                                     |org.joda.time.tz|::ZONEINFOLOGGER.
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-DATE-TIME-OF-YEAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-UNTIL-YEAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-FORMAT*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-RULES*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-OFFSET-MILLIS*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.I-NAME*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.EQUALS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.WAIT
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.ADD-TO-BUILDER
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$ZONE.
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.EQUALS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.WAIT
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.ADD-RECURRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULESET.
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-LETTER-S*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-SAVE-MILLIS*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-DATE-TIME-OF-YEAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TYPE*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-TO-YEAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-FROM-YEAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.I-NAME*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.EQUALS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.WAIT
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.ADD-RECURRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$RULE.
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ZONE-CHAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MILLIS-OF-DAY*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-ADVANCE-DAY-OF-WEEK*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-WEEK*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-DAY-OF-MONTH*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.I-MONTH-OF-YEAR*
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.EQUALS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.WAIT
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.ADD-CUTOVER
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.ADD-RECURRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER$DATETIMEOFYEAR.
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.CLASS-PROP
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.NOTIFY-ALL
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.NOTIFY
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.GET-CLASS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.HASH-CODE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.TO-STRING
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.EQUALS
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.WAIT
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.COMPILE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.MAIN
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.PARSE-DATA-FILE
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.NEW
                                     |org.joda.time.tz|::ZONEINFOCOMPILER.
                                     |org.joda.time.tz|::UTCPROVIDER.CLASS-PROP
                                     |org.joda.time.tz|::UTCPROVIDER.AVAILABLE-I-DS-PROP
                                     |org.joda.time.tz|::UTCPROVIDER.NOTIFY-ALL
                                     |org.joda.time.tz|::UTCPROVIDER.NOTIFY
                                     |org.joda.time.tz|::UTCPROVIDER.GET-CLASS
                                     |org.joda.time.tz|::UTCPROVIDER.HASH-CODE
                                     |org.joda.time.tz|::UTCPROVIDER.TO-STRING
                                     |org.joda.time.tz|::UTCPROVIDER.EQUALS
                                     |org.joda.time.tz|::UTCPROVIDER.WAIT
                                     |org.joda.time.tz|::UTCPROVIDER.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::UTCPROVIDER.GET-ZONE
                                     |org.joda.time.tz|::UTCPROVIDER.NEW
                                     |org.joda.time.tz|::UTCPROVIDER.
                                     |org.joda.time.tz|::PROVIDER.AVAILABLE-I-DS-PROP
                                     |org.joda.time.tz|::PROVIDER.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::PROVIDER.GET-ZONE
                                     |org.joda.time.tz|::PROVIDER.
                                     |org.joda.time.tz|::NAMEPROVIDER.GET-NAME
                                     |org.joda.time.tz|::NAMEPROVIDER.GET-SHORT-NAME
                                     |org.joda.time.tz|::NAMEPROVIDER.
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.FIXED-PROP
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.CLASS-PROP
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.+ID+-PROP
                                     |org.joda.time.tz|::+FIXEDDATETIMEZONE.+UTC+*+
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.+UTC+*
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.NOTIFY-ALL
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.NOTIFY
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-CLASS
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.WAIT
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-+ID+
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.SET-DEFAULT
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-DEFAULT
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.TO-STRING
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-PROVIDER
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.IS-LOCAL-DATE-TIME-GAP
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.CONVERT-+UTC+-TO-LOCAL
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.IS-STANDARD-OFFSET
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-SHORT-NAME
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.SET-NAME-PROVIDER
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME-PROVIDER
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.SET-PROVIDER
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-TIME-ZONE
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-MILLIS
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-HOURS-MINUTES
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-OFFSET-HOURS
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.FOR-+ID+
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.CONVERT-LOCAL-TO-+UTC+
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.ADJUST-OFFSET
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-MILLIS-KEEP-LOCAL
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-OFFSET
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.HASH-CODE
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.EQUALS
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.PREVIOUS-TRANSITION
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.NEXT-TRANSITION
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.IS-FIXED
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-OFFSET-FROM-LOCAL
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.GET-NAME-KEY
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.TO-TIME-ZONE
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.NEW
                                     |org.joda.time.tz|::FIXEDDATETIMEZONE.
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.CLASS-PROP
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.NOTIFY-ALL
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.NOTIFY
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-CLASS
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.HASH-CODE
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.TO-STRING
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.EQUALS
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.WAIT
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-NAME
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.GET-SHORT-NAME
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.NEW
                                     |org.joda.time.tz|::DEFAULTNAMEPROVIDER.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.WALL-OFFSET-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.STANDARD-OFFSET-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.SAVE-MILLIS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NAME-KEY-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.MILLIS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.IS-TRANSITION-FROM
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-WALL-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-SAVE-MILLIS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-NAME-KEY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.GET-MILLIS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$TRANSITION.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.UPPER-LIMIT-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.STANDARD-OFFSET-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-FIXED-SAVINGS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.BUILD-TAIL-ZONE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.FIRST-TRANSITION
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.ADD-RULE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-UPPER-LIMIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.SET-UPPER-LIMIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.NEXT-TRANSITION
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULESET.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.TO-YEAR-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.SAVE-MILLIS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.OF-YEAR-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NAME-KEY-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.FROM-YEAR-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.NEXT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-OF-YEAR
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-SAVE-MILLIS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-TO-YEAR
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-FROM-YEAR
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.GET-NAME-KEY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RULE.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.SAVE-MILLIS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.OF-YEAR-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NAME-KEY-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.PREVIOUS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.NEXT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.WRITE-TO
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-OF-YEAR
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-SAVE-MILLIS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.GET-NAME-KEY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$RECURRENCE.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FIXED-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CACHABLE-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+ID+-PROP
                                     |org.joda.time.tz|::+DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+*+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.+UTC+*
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-+ID+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-DEFAULT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-DEFAULT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-LOCAL-DATE-TIME-GAP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CONVERT-+UTC+-TO-LOCAL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-OFFSET-FROM-LOCAL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-SHORT-NAME
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-NAME-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.SET-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-TIME-ZONE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-MILLIS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-HOURS-MINUTES
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-OFFSET-HOURS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.FOR-+ID+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.CONVERT-LOCAL-TO-+UTC+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.ADJUST-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-MILLIS-KEEP-LOCAL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.TO-TIME-ZONE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.WRITE-TO
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-CACHABLE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.PREVIOUS-TRANSITION
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.NEXT-TRANSITION
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.IS-FIXED
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.GET-NAME-KEY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$PRECALCULATEDZONE.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.PREVIOUS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.NEXT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.WRITE-TO
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.SET-INSTANT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$OFYEAR.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FIXED-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+ID+-PROP
                                     |org.joda.time.tz|::+DATETIMEZONEBUILDER$DSTZONE.+UTC+*+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.+UTC+*
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-+ID+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-DEFAULT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-DEFAULT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-LOCAL-DATE-TIME-GAP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CONVERT-+UTC+-TO-LOCAL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-OFFSET-FROM-LOCAL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-SHORT-NAME
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-NAME-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.SET-PROVIDER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-TIME-ZONE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-MILLIS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-HOURS-MINUTES
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-OFFSET-HOURS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.FOR-+ID+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.CONVERT-LOCAL-TO-+UTC+
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.ADJUST-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-MILLIS-KEEP-LOCAL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.TO-TIME-ZONE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.WRITE-TO
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.PREVIOUS-TRANSITION
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.NEXT-TRANSITION
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.IS-FIXED
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.GET-NAME-KEY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER$DSTZONE.
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.CLASS-PROP
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.NOTIFY-ALL
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.NOTIFY
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.GET-CLASS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.HASH-CODE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.TO-STRING
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.EQUALS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.WAIT
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.WRITE-TO
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.TO-DATE-TIME-ZONE
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.SET-FIXED-SAVINGS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.READ-FROM
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.ADD-RECURRING-SAVINGS
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.SET-STANDARD-OFFSET
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.ADD-CUTOVER
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.NEW
                                     |org.joda.time.tz|::DATETIMEZONEBUILDER.
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.CLASS-PROP
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-ZONE-REF*
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.I-PERIOD-START*
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.NOTIFY-ALL
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.NOTIFY
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-CLASS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.HASH-CODE
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.TO-STRING
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.EQUALS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.WAIT
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-OFFSET
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.GET-NAME-KEY
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE$INFO.
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.UNCACHED-ZONE-PROP
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FIXED-PROP
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.CLASS-PROP
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.+ID+-PROP
                                     |org.joda.time.tz|::+CACHEDDATETIMEZONE.+UTC+*+
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.+UTC+*
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.NOTIFY-ALL
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.NOTIFY
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-CLASS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.WAIT
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-+ID+
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.SET-DEFAULT
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-DEFAULT
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.TO-STRING
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-PROVIDER
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.IS-LOCAL-DATE-TIME-GAP
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.CONVERT-+UTC+-TO-LOCAL
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-OFFSET-FROM-LOCAL
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.IS-STANDARD-OFFSET
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-SHORT-NAME
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.SET-NAME-PROVIDER
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME-PROVIDER
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.SET-PROVIDER
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-AVAILABLE-I-DS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-TIME-ZONE
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-MILLIS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-HOURS-MINUTES
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-OFFSET-HOURS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-+ID+
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.CONVERT-LOCAL-TO-+UTC+
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.ADJUST-OFFSET
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-MILLIS-KEEP-LOCAL
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.TO-TIME-ZONE
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-OFFSET
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.HASH-CODE
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.EQUALS
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.FOR-ZONE
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-UNCACHED-ZONE
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.PREVIOUS-TRANSITION
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.NEXT-TRANSITION
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.IS-FIXED
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-STANDARD-OFFSET
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.GET-NAME-KEY
                                     |org.joda.time.tz|::CACHEDDATETIMEZONE.) "org.joda.time.tz"))
