(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "org.joda.time.convert")
)(DEFCONSTANT |org.joda.time.convert|::ABSTRACTCONVERTER.
   '|org.joda.time.convert|::|AbstractConverter|)
(DEFCLASS |org.joda.time.convert|::ABSTRACTCONVERTER.
          (|org.joda.time.convert|::CONVERTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "getChronology"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.AbstractConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "toString"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter| "wait"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "equals"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "hashCode"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "getClass"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "notify"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|AbstractConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::ABSTRACTCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::ABSTRACTCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|AbstractConverter| "class"
                       '|org.joda.time.convert|::ABSTRACTCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::CALENDARCONVERTER.
  '|org.joda.time.convert|::|CalendarConverter|)
(DEFCLASS |org.joda.time.convert|::CALENDARCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INSTANTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.CalendarConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.CalendarConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "getChronology"
                          '|org.joda.time.convert|::CALENDARCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.CalendarConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::CALENDARCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.CalendarConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::CALENDARCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::CALENDARCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::CALENDARCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::CALENDARCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "toString"
                          '|org.joda.time.convert|::CALENDARCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter| "wait"
                          '|org.joda.time.convert|::CALENDARCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "equals"
                          '|org.joda.time.convert|::CALENDARCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "hashCode"
                          '|org.joda.time.convert|::CALENDARCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "getClass"
                          '|org.joda.time.convert|::CALENDARCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "notify"
                          '|org.joda.time.convert|::CALENDARCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|CalendarConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::CALENDARCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|CalendarConverter| "class"
                       '|org.joda.time.convert|::CALENDARCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CALENDARCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|CalendarConverter|
                       "supportedType"
                       '|org.joda.time.convert|::CALENDARCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::CONVERTER.
  '|org.joda.time.convert|::|Converter|)
(DEFCLASS |org.joda.time.convert|::CONVERTER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::CONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|Converter|
                          "getSupportedType"
                          '|org.joda.time.convert|::CONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|Converter| "supportedType"
                       '|org.joda.time.convert|::CONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::CONVERTERMANAGER.
  '|org.joda.time.convert|::|ConverterManager|)
(DEFCLASS |org.joda.time.convert|::CONVERTERMANAGER. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANT-CONVERTERS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.InstantConverter[] org.joda.time.convert.ConverterManager.getInstantConverters()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getInstantConverters"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANT-CONVERTERS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANT-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.InstantConverter org.joda.time.convert.ConverterManager.getInstantConverter(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getInstantConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANT-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.ADD-INSTANT-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.InstantConverter org.joda.time.convert.ConverterManager.addInstantConverter(org.joda.time.convert.InstantConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "addInstantConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.ADD-INSTANT-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-DURATION-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.DurationConverter org.joda.time.convert.ConverterManager.getDurationConverter(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getDurationConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-DURATION-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-PERIOD-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PeriodConverter org.joda.time.convert.ConverterManager.getPeriodConverter(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getPeriodConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-PERIOD-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-INTERVAL-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.IntervalConverter org.joda.time.convert.ConverterManager.getIntervalConverter(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getIntervalConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-INTERVAL-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-DURATION-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.DurationConverter org.joda.time.convert.ConverterManager.removeDurationConverter(org.joda.time.convert.DurationConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "removeDurationConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.REMOVE-DURATION-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-PERIOD-CONVERTERS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PeriodConverter[] org.joda.time.convert.ConverterManager.getPeriodConverters()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getPeriodConverters"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-PERIOD-CONVERTERS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.ADD-PERIOD-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PeriodConverter org.joda.time.convert.ConverterManager.addPeriodConverter(org.joda.time.convert.PeriodConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "addPeriodConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.ADD-PERIOD-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-PARTIAL-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PartialConverter org.joda.time.convert.ConverterManager.getPartialConverter(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getPartialConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-PARTIAL-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-INSTANT-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.InstantConverter org.joda.time.convert.ConverterManager.removeInstantConverter(org.joda.time.convert.InstantConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "removeInstantConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.REMOVE-INSTANT-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-PARTIAL-CONVERTERS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PartialConverter[] org.joda.time.convert.ConverterManager.getPartialConverters()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getPartialConverters"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-PARTIAL-CONVERTERS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-PERIOD-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PeriodConverter org.joda.time.convert.ConverterManager.removePeriodConverter(org.joda.time.convert.PeriodConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "removePeriodConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.REMOVE-PERIOD-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-INTERVAL-CONVERTERS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.IntervalConverter[] org.joda.time.convert.ConverterManager.getIntervalConverters()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getIntervalConverters"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-INTERVAL-CONVERTERS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.ADD-INTERVAL-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.IntervalConverter org.joda.time.convert.ConverterManager.addIntervalConverter(org.joda.time.convert.IntervalConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "addIntervalConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.ADD-INTERVAL-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-INTERVAL-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.IntervalConverter org.joda.time.convert.ConverterManager.removeIntervalConverter(org.joda.time.convert.IntervalConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "removeIntervalConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.REMOVE-INTERVAL-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.ADD-PARTIAL-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PartialConverter org.joda.time.convert.ConverterManager.addPartialConverter(org.joda.time.convert.PartialConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "addPartialConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.ADD-PARTIAL-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-PARTIAL-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PartialConverter org.joda.time.convert.ConverterManager.removePartialConverter(org.joda.time.convert.PartialConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "removePartialConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.REMOVE-PARTIAL-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-DURATION-CONVERTERS
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.DurationConverter[] org.joda.time.convert.ConverterManager.getDurationConverters()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getDurationConverters"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-DURATION-CONVERTERS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.ADD-DURATION-CONVERTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.DurationConverter org.joda.time.convert.ConverterManager.addDurationConverter(org.joda.time.convert.DurationConverter) throws java.lang.SecurityException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "addDurationConverter"
                          '|org.joda.time.convert|::CONVERTERMANAGER.ADD-DURATION-CONVERTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.ConverterManager.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "toString"
                          '|org.joda.time.convert|::CONVERTERMANAGER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANCE
       (&REST FOIL::ARGS)
  "public static org.joda.time.convert.ConverterManager org.joda.time.convert.ConverterManager.getInstance()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getInstance"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANCE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager| "wait"
                          '|org.joda.time.convert|::CONVERTERMANAGER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager| "equals"
                          '|org.joda.time.convert|::CONVERTERMANAGER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "hashCode"
                          '|org.joda.time.convert|::CONVERTERMANAGER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "getClass"
                          '|org.joda.time.convert|::CONVERTERMANAGER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager| "notify"
                          '|org.joda.time.convert|::CONVERTERMANAGER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterManager|
                          "notifyAll"
                          '|org.joda.time.convert|::CONVERTERMANAGER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterManager| "class"
                       '|org.joda.time.convert|::CONVERTERMANAGER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.DURATION-CONVERTERS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.DurationConverter[] org.joda.time.convert.ConverterManager.getDurationConverters()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterManager|
                       "durationConverters"
                       '|org.joda.time.convert|::CONVERTERMANAGER.DURATIONCONVERTERS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.INSTANT-CONVERTERS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.InstantConverter[] org.joda.time.convert.ConverterManager.getInstantConverters()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterManager|
                       "instantConverters"
                       '|org.joda.time.convert|::CONVERTERMANAGER.INSTANTCONVERTERS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.INTERVAL-CONVERTERS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.IntervalConverter[] org.joda.time.convert.ConverterManager.getIntervalConverters()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterManager|
                       "intervalConverters"
                       '|org.joda.time.convert|::CONVERTERMANAGER.INTERVALCONVERTERS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.PARTIAL-CONVERTERS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PartialConverter[] org.joda.time.convert.ConverterManager.getPartialConverters()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterManager|
                       "partialConverters"
                       '|org.joda.time.convert|::CONVERTERMANAGER.PARTIALCONVERTERS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERMANAGER.PERIOD-CONVERTERS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.convert.PeriodConverter[] org.joda.time.convert.ConverterManager.getPeriodConverters()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterManager|
                       "periodConverters"
                       '|org.joda.time.convert|::CONVERTERMANAGER.PERIODCONVERTERS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::CONVERTERSET.
  '|org.joda.time.convert|::|ConverterSet|)
(DEFCLASS |org.joda.time.convert|::CONVERTERSET. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::CONVERTERSET.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "wait"
                          '|org.joda.time.convert|::CONVERTERSET.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "equals"
                          '|org.joda.time.convert|::CONVERTERSET.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "toString"
                          '|org.joda.time.convert|::CONVERTERSET.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "hashCode"
                          '|org.joda.time.convert|::CONVERTERSET.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "getClass"
                          '|org.joda.time.convert|::CONVERTERSET.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "notify"
                          '|org.joda.time.convert|::CONVERTERSET.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet| "notifyAll"
                          '|org.joda.time.convert|::CONVERTERSET.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterSet| "class"
                       '|org.joda.time.convert|::CONVERTERSET.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::CONVERTERSET$ENTRY.
  '|org.joda.time.convert|::|ConverterSet$Entry|)
(DEFCLASS |org.joda.time.convert|::CONVERTERSET$ENTRY. (|java.lang|::OBJECT.)
          NIL)
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry| "wait"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry|
                          "equals"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry|
                          "toString"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry|
                          "hashCode"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry|
                          "getClass"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry|
                          "notify"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ConverterSet$Entry|
                          "notifyAll"
                          '|org.joda.time.convert|::CONVERTERSET$ENTRY.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::CONVERTERSET$ENTRY.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ConverterSet$Entry| "class"
                       '|org.joda.time.convert|::CONVERTERSET$ENTRY.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::DATECONVERTER.
  '|org.joda.time.convert|::|DateConverter|)
(DEFCLASS |org.joda.time.convert|::DATECONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INSTANTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::DATECONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.DateConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::DATECONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.DateConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::DATECONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter|
                          "getChronology"
                          '|org.joda.time.convert|::DATECONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::DATECONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::DATECONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::DATECONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "toString"
                          '|org.joda.time.convert|::DATECONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "wait"
                          '|org.joda.time.convert|::DATECONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "equals"
                          '|org.joda.time.convert|::DATECONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "hashCode"
                          '|org.joda.time.convert|::DATECONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "getClass"
                          '|org.joda.time.convert|::DATECONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "notify"
                          '|org.joda.time.convert|::DATECONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DateConverter| "notifyAll"
                          '|org.joda.time.convert|::DATECONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|DateConverter| "class"
                       '|org.joda.time.convert|::DATECONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DATECONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|DateConverter|
                       "supportedType"
                       '|org.joda.time.convert|::DATECONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::DURATIONCONVERTER.
  '|org.joda.time.convert|::|DurationConverter|)
(DEFCLASS |org.joda.time.convert|::DURATIONCONVERTER.
          (|org.joda.time.convert|::CONVERTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::DURATIONCONVERTER.GET-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.convert.DurationConverter.getDurationMillis(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DurationConverter|
                          "getDurationMillis"
                          '|org.joda.time.convert|::DURATIONCONVERTER.GET-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::DURATIONCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|DurationConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::DURATIONCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::INSTANTCONVERTER.
  '|org.joda.time.convert|::|InstantConverter|)
(DEFCLASS |org.joda.time.convert|::INSTANTCONVERTER.
          (|org.joda.time.convert|::CONVERTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::INSTANTCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.convert.InstantConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public abstract org.joda.time.Chronology org.joda.time.convert.InstantConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|InstantConverter|
                          "getChronology"
                          '|org.joda.time.convert|::INSTANTCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::INSTANTCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.convert.InstantConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|InstantConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::INSTANTCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::INSTANTCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|InstantConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::INSTANTCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::INTERVALCONVERTER.
  '|org.joda.time.convert|::|IntervalConverter|)
(DEFCLASS |org.joda.time.convert|::INTERVALCONVERTER.
          (|org.joda.time.convert|::CONVERTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::INTERVALCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.convert.IntervalConverter.setInto(org.joda.time.ReadWritableInterval,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|IntervalConverter|
                          "setInto"
                          '|org.joda.time.convert|::INTERVALCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::INTERVALCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.convert.IntervalConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|IntervalConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::INTERVALCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::INTERVALCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|IntervalConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::INTERVALCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::LONGCONVERTER.
  '|org.joda.time.convert|::|LongConverter|)
(DEFCLASS |org.joda.time.convert|::LONGCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INSTANTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.
           |org.joda.time.convert|::DURATIONCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.LongConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.LongConverter.getDurationMillis(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "getDurationMillis"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.LongConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "getChronology"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::LONGCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "toString"
                          '|org.joda.time.convert|::LONGCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "wait"
                          '|org.joda.time.convert|::LONGCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "equals"
                          '|org.joda.time.convert|::LONGCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "hashCode"
                          '|org.joda.time.convert|::LONGCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "getClass"
                          '|org.joda.time.convert|::LONGCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "notify"
                          '|org.joda.time.convert|::LONGCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|LongConverter| "notifyAll"
                          '|org.joda.time.convert|::LONGCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|LongConverter| "class"
                       '|org.joda.time.convert|::LONGCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::LONGCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|LongConverter|
                       "supportedType"
                       '|org.joda.time.convert|::LONGCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::NULLCONVERTER.
  '|org.joda.time.convert|::|NullConverter|)
(DEFCLASS |org.joda.time.convert|::NULLCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INSTANTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.
           |org.joda.time.convert|::DURATIONCONVERTER.
           |org.joda.time.convert|::PERIODCONVERTER.
           |org.joda.time.convert|::INTERVALCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.NullConverter.getDurationMillis(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "getDurationMillis"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.convert.NullConverter.setInto(org.joda.time.ReadWritableInterval,java.lang.Object,org.joda.time.Chronology)
public void org.joda.time.convert.NullConverter.setInto(org.joda.time.ReadWritablePeriod,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "setInto"
                          '|org.joda.time.convert|::NULLCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.NullConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "getChronology"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.AbstractConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::NULLCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "toString"
                          '|org.joda.time.convert|::NULLCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "wait"
                          '|org.joda.time.convert|::NULLCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "equals"
                          '|org.joda.time.convert|::NULLCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "hashCode"
                          '|org.joda.time.convert|::NULLCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "getClass"
                          '|org.joda.time.convert|::NULLCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "notify"
                          '|org.joda.time.convert|::NULLCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|NullConverter| "notifyAll"
                          '|org.joda.time.convert|::NULLCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|NullConverter| "class"
                       '|org.joda.time.convert|::NULLCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::NULLCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|NullConverter|
                       "supportedType"
                       '|org.joda.time.convert|::NULLCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::PARTIALCONVERTER.
  '|org.joda.time.convert|::|PartialConverter|)
(DEFCLASS |org.joda.time.convert|::PARTIALCONVERTER.
          (|org.joda.time.convert|::CONVERTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::PARTIALCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.Chronology org.joda.time.convert.PartialConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public abstract org.joda.time.Chronology org.joda.time.convert.PartialConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|PartialConverter|
                          "getChronology"
                          '|org.joda.time.convert|::PARTIALCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::PARTIALCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int[] org.joda.time.convert.PartialConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public abstract int[] org.joda.time.convert.PartialConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|PartialConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::PARTIALCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::PARTIALCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|PartialConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::PARTIALCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::PERIODCONVERTER.
  '|org.joda.time.convert|::|PeriodConverter|)
(DEFCLASS |org.joda.time.convert|::PERIODCONVERTER.
          (|org.joda.time.convert|::CONVERTER. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.convert|::PERIODCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.PeriodType org.joda.time.convert.PeriodConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|PeriodConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::PERIODCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::PERIODCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void org.joda.time.convert.PeriodConverter.setInto(org.joda.time.ReadWritablePeriod,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|PeriodConverter| "setInto"
                          '|org.joda.time.convert|::PERIODCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::PERIODCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|PeriodConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::PERIODCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::READABLEDURATIONCONVERTER.
  '|org.joda.time.convert|::|ReadableDurationConverter|)
(DEFCLASS |org.joda.time.convert|::READABLEDURATIONCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::DURATIONCONVERTER.
           |org.joda.time.convert|::PERIODCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.ReadableDurationConverter.getDurationMillis(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getDurationMillis"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.convert.ReadableDurationConverter.setInto(org.joda.time.ReadWritablePeriod,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "setInto"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.ReadableDurationConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getChronology"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.AbstractConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "toString"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "wait"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "equals"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "hashCode"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "getClass"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "notify"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableDurationConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::READABLEDURATIONCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadableDurationConverter|
                       "class"
                       '|org.joda.time.convert|::READABLEDURATIONCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEDURATIONCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadableDurationConverter|
                       "supportedType"
                       '|org.joda.time.convert|::READABLEDURATIONCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::READABLEINSTANTCONVERTER.
  '|org.joda.time.convert|::|ReadableInstantConverter|)
(DEFCLASS |org.joda.time.convert|::READABLEINSTANTCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INSTANTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.ReadableInstantConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
public org.joda.time.Chronology org.joda.time.convert.ReadableInstantConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "getChronology"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.ReadableInstantConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.ReadableInstantConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "toString"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "wait"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "equals"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "hashCode"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "getClass"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "notify"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableInstantConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::READABLEINSTANTCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadableInstantConverter|
                       "class"
                       '|org.joda.time.convert|::READABLEINSTANTCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINSTANTCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadableInstantConverter|
                       "supportedType"
                       '|org.joda.time.convert|::READABLEINSTANTCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::READABLEINTERVALCONVERTER.
  '|org.joda.time.convert|::|ReadableIntervalConverter|)
(DEFCLASS |org.joda.time.convert|::READABLEINTERVALCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INTERVALCONVERTER.
           |org.joda.time.convert|::DURATIONCONVERTER.
           |org.joda.time.convert|::PERIODCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.ReadableIntervalConverter.getDurationMillis(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getDurationMillis"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.convert.ReadableIntervalConverter.setInto(org.joda.time.ReadWritablePeriod,java.lang.Object,org.joda.time.Chronology)
public void org.joda.time.convert.ReadableIntervalConverter.setInto(org.joda.time.ReadWritableInterval,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "setInto"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.ReadableIntervalConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.ReadableIntervalConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getChronology"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.AbstractConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "toString"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "wait"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "equals"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "hashCode"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "getClass"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "notify"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadableIntervalConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::READABLEINTERVALCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadableIntervalConverter|
                       "class"
                       '|org.joda.time.convert|::READABLEINTERVALCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEINTERVALCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadableIntervalConverter|
                       "supportedType"
                       '|org.joda.time.convert|::READABLEINTERVALCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::READABLEPARTIALCONVERTER.
  '|org.joda.time.convert|::|ReadablePartialConverter|)
(DEFCLASS |org.joda.time.convert|::READABLEPARTIALCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.ReadablePartialConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
public org.joda.time.Chronology org.joda.time.convert.ReadablePartialConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "getChronology"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
public int[] org.joda.time.convert.ReadablePartialConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.ReadablePartialConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.AbstractConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "toString"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "wait"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "equals"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "hashCode"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "getClass"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "notify"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePartialConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::READABLEPARTIALCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadablePartialConverter|
                       "class"
                       '|org.joda.time.convert|::READABLEPARTIALCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPARTIALCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadablePartialConverter|
                       "supportedType"
                       '|org.joda.time.convert|::READABLEPARTIALCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::READABLEPERIODCONVERTER.
  '|org.joda.time.convert|::|ReadablePeriodConverter|)
(DEFCLASS |org.joda.time.convert|::READABLEPERIODCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::PERIODCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.ReadablePeriodConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.convert.ReadablePeriodConverter.setInto(org.joda.time.ReadWritablePeriod,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "setInto"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.ReadablePeriodConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "getChronology"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.AbstractConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "toString"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "wait"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "equals"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "hashCode"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "getClass"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "notify"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|ReadablePeriodConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::READABLEPERIODCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadablePeriodConverter|
                       "class"
                       '|org.joda.time.convert|::READABLEPERIODCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::READABLEPERIODCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|ReadablePeriodConverter|
                       "supportedType"
                       '|org.joda.time.convert|::READABLEPERIODCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.convert|::STRINGCONVERTER.
  '|org.joda.time.convert|::|StringConverter|)
(DEFCLASS |org.joda.time.convert|::STRINGCONVERTER.
          (|org.joda.time.convert|::ABSTRACTCONVERTER.
           |org.joda.time.convert|::INSTANTCONVERTER.
           |org.joda.time.convert|::PARTIALCONVERTER.
           |org.joda.time.convert|::DURATIONCONVERTER.
           |org.joda.time.convert|::PERIODCONVERTER.
           |org.joda.time.convert|::INTERVALCONVERTER.)
          NIL)
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-INSTANT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.StringConverter.getInstantMillis(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getInstantMillis"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-INSTANT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-DURATION-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.convert.StringConverter.getDurationMillis(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getDurationMillis"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-DURATION-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.SET-INTO
       (FOIL::THIS &REST FOIL::ARGS)
  "public void org.joda.time.convert.StringConverter.setInto(org.joda.time.ReadWritableInterval,java.lang.Object,org.joda.time.Chronology)
public void org.joda.time.convert.StringConverter.setInto(org.joda.time.ReadWritablePeriod,java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter| "setInto"
                          '|org.joda.time.convert|::STRINGCONVERTER.SET-INTO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-PARTIAL-VALUES
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.convert.AbstractConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology)
public int[] org.joda.time.convert.StringConverter.getPartialValues(org.joda.time.ReadablePartial,java.lang.Object,org.joda.time.Chronology,org.joda.time.format.DateTimeFormatter)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getPartialValues"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-PARTIAL-VALUES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-SUPPORTED-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Class org.joda.time.convert.StringConverter.getSupportedType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getSupportedType"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-SUPPORTED-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-CHRONOLOGY
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.Chronology)
public org.joda.time.Chronology org.joda.time.convert.AbstractConverter.getChronology(java.lang.Object,org.joda.time.DateTimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getChronology"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-CHRONOLOGY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-PERIOD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.PeriodType org.joda.time.convert.AbstractConverter.getPeriodType(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getPeriodType"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-PERIOD-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.IS-READABLE-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.convert.AbstractConverter.isReadableInterval(java.lang.Object,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "isReadableInterval"
                          '|org.joda.time.convert|::STRINGCONVERTER.IS-READABLE-INTERVAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.convert.AbstractConverter.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "toString"
                          '|org.joda.time.convert|::STRINGCONVERTER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter| "wait"
                          '|org.joda.time.convert|::STRINGCONVERTER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter| "equals"
                          '|org.joda.time.convert|::STRINGCONVERTER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "hashCode"
                          '|org.joda.time.convert|::STRINGCONVERTER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "getClass"
                          '|org.joda.time.convert|::STRINGCONVERTER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter| "notify"
                          '|org.joda.time.convert|::STRINGCONVERTER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.convert|::|StringConverter|
                          "notifyAll"
                          '|org.joda.time.convert|::STRINGCONVERTER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|StringConverter| "class"
                       '|org.joda.time.convert|::STRINGCONVERTER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.convert|::STRINGCONVERTER.SUPPORTED-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.Class org.joda.time.convert.Converter.getSupportedType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.convert|::|StringConverter|
                       "supportedType"
                       '|org.joda.time.convert|::STRINGCONVERTER.SUPPORTEDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(eval-when (:load-toplevel)(export '(|org.joda.time.convert|::STRINGCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::STRINGCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::STRINGCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::STRINGCONVERTER.NOTIFY
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::STRINGCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::STRINGCONVERTER.EQUALS
                                     |org.joda.time.convert|::STRINGCONVERTER.WAIT
                                     |org.joda.time.convert|::STRINGCONVERTER.TO-STRING
                                     |org.joda.time.convert|::STRINGCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::STRINGCONVERTER.SET-INTO
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-DURATION-MILLIS
                                     |org.joda.time.convert|::STRINGCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::STRINGCONVERTER.
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.NOTIFY
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.EQUALS
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.WAIT
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.TO-STRING
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.SET-INTO
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::READABLEPERIODCONVERTER.
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.NOTIFY
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.EQUALS
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.WAIT
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.TO-STRING
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::READABLEPARTIALCONVERTER.
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.NOTIFY
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.EQUALS
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.WAIT
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.TO-STRING
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.SET-INTO
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.GET-DURATION-MILLIS
                                     |org.joda.time.convert|::READABLEINTERVALCONVERTER.
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.NOTIFY
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.EQUALS
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.WAIT
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.TO-STRING
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::READABLEINSTANTCONVERTER.
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.NOTIFY
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.EQUALS
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.WAIT
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.TO-STRING
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.SET-INTO
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.GET-DURATION-MILLIS
                                     |org.joda.time.convert|::READABLEDURATIONCONVERTER.
                                     |org.joda.time.convert|::PERIODCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::PERIODCONVERTER.SET-INTO
                                     |org.joda.time.convert|::PERIODCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::PERIODCONVERTER.
                                     |org.joda.time.convert|::PARTIALCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::PARTIALCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::PARTIALCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::PARTIALCONVERTER.
                                     |org.joda.time.convert|::NULLCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::NULLCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::NULLCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::NULLCONVERTER.NOTIFY
                                     |org.joda.time.convert|::NULLCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::NULLCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::NULLCONVERTER.EQUALS
                                     |org.joda.time.convert|::NULLCONVERTER.WAIT
                                     |org.joda.time.convert|::NULLCONVERTER.TO-STRING
                                     |org.joda.time.convert|::NULLCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::NULLCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::NULLCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::NULLCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::NULLCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::NULLCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::NULLCONVERTER.SET-INTO
                                     |org.joda.time.convert|::NULLCONVERTER.GET-DURATION-MILLIS
                                     |org.joda.time.convert|::NULLCONVERTER.
                                     |org.joda.time.convert|::LONGCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::LONGCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::LONGCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::LONGCONVERTER.NOTIFY
                                     |org.joda.time.convert|::LONGCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::LONGCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::LONGCONVERTER.EQUALS
                                     |org.joda.time.convert|::LONGCONVERTER.WAIT
                                     |org.joda.time.convert|::LONGCONVERTER.TO-STRING
                                     |org.joda.time.convert|::LONGCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::LONGCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::LONGCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::LONGCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::LONGCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::LONGCONVERTER.GET-DURATION-MILLIS
                                     |org.joda.time.convert|::LONGCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::LONGCONVERTER.
                                     |org.joda.time.convert|::INTERVALCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::INTERVALCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::INTERVALCONVERTER.SET-INTO
                                     |org.joda.time.convert|::INTERVALCONVERTER.
                                     |org.joda.time.convert|::INSTANTCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::INSTANTCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::INSTANTCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::INSTANTCONVERTER.
                                     |org.joda.time.convert|::DURATIONCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::DURATIONCONVERTER.GET-DURATION-MILLIS
                                     |org.joda.time.convert|::DURATIONCONVERTER.
                                     |org.joda.time.convert|::DATECONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::DATECONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::DATECONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::DATECONVERTER.NOTIFY
                                     |org.joda.time.convert|::DATECONVERTER.GET-CLASS
                                     |org.joda.time.convert|::DATECONVERTER.HASH-CODE
                                     |org.joda.time.convert|::DATECONVERTER.EQUALS
                                     |org.joda.time.convert|::DATECONVERTER.WAIT
                                     |org.joda.time.convert|::DATECONVERTER.TO-STRING
                                     |org.joda.time.convert|::DATECONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::DATECONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::DATECONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::DATECONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::DATECONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::DATECONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::DATECONVERTER.
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.CLASS-PROP
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.NOTIFY-ALL
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.NOTIFY
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.GET-CLASS
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.HASH-CODE
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.TO-STRING
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.EQUALS
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.WAIT
                                     |org.joda.time.convert|::CONVERTERSET$ENTRY.
                                     |org.joda.time.convert|::CONVERTERSET.CLASS-PROP
                                     |org.joda.time.convert|::CONVERTERSET.NOTIFY-ALL
                                     |org.joda.time.convert|::CONVERTERSET.NOTIFY
                                     |org.joda.time.convert|::CONVERTERSET.GET-CLASS
                                     |org.joda.time.convert|::CONVERTERSET.HASH-CODE
                                     |org.joda.time.convert|::CONVERTERSET.TO-STRING
                                     |org.joda.time.convert|::CONVERTERSET.EQUALS
                                     |org.joda.time.convert|::CONVERTERSET.WAIT
                                     |org.joda.time.convert|::CONVERTERSET.
                                     |org.joda.time.convert|::CONVERTERMANAGER.PERIOD-CONVERTERS-PROP
                                     |org.joda.time.convert|::CONVERTERMANAGER.PARTIAL-CONVERTERS-PROP
                                     |org.joda.time.convert|::CONVERTERMANAGER.INTERVAL-CONVERTERS-PROP
                                     |org.joda.time.convert|::CONVERTERMANAGER.INSTANT-CONVERTERS-PROP
                                     |org.joda.time.convert|::CONVERTERMANAGER.DURATION-CONVERTERS-PROP
                                     |org.joda.time.convert|::CONVERTERMANAGER.CLASS-PROP
                                     |org.joda.time.convert|::CONVERTERMANAGER.NOTIFY-ALL
                                     |org.joda.time.convert|::CONVERTERMANAGER.NOTIFY
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-CLASS
                                     |org.joda.time.convert|::CONVERTERMANAGER.HASH-CODE
                                     |org.joda.time.convert|::CONVERTERMANAGER.EQUALS
                                     |org.joda.time.convert|::CONVERTERMANAGER.WAIT
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANCE
                                     |org.joda.time.convert|::CONVERTERMANAGER.TO-STRING
                                     |org.joda.time.convert|::CONVERTERMANAGER.ADD-DURATION-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-DURATION-CONVERTERS
                                     |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-PARTIAL-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.ADD-PARTIAL-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-INTERVAL-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.ADD-INTERVAL-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-INTERVAL-CONVERTERS
                                     |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-PERIOD-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-PARTIAL-CONVERTERS
                                     |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-INSTANT-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-PARTIAL-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.ADD-PERIOD-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-PERIOD-CONVERTERS
                                     |org.joda.time.convert|::CONVERTERMANAGER.REMOVE-DURATION-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-INTERVAL-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-PERIOD-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-DURATION-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.ADD-INSTANT-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANT-CONVERTER
                                     |org.joda.time.convert|::CONVERTERMANAGER.GET-INSTANT-CONVERTERS
                                     |org.joda.time.convert|::CONVERTERMANAGER.
                                     |org.joda.time.convert|::CONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::CONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::CONVERTER.
                                     |org.joda.time.convert|::CALENDARCONVERTER.SUPPORTED-TYPE-PROP
                                     |org.joda.time.convert|::CALENDARCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::CALENDARCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::CALENDARCONVERTER.NOTIFY
                                     |org.joda.time.convert|::CALENDARCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::CALENDARCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::CALENDARCONVERTER.EQUALS
                                     |org.joda.time.convert|::CALENDARCONVERTER.WAIT
                                     |org.joda.time.convert|::CALENDARCONVERTER.TO-STRING
                                     |org.joda.time.convert|::CALENDARCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::CALENDARCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::CALENDARCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::CALENDARCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::CALENDARCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::CALENDARCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::CALENDARCONVERTER.
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.CLASS-PROP
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.GET-SUPPORTED-TYPE
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.NOTIFY-ALL
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.NOTIFY
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.GET-CLASS
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.HASH-CODE
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.EQUALS
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.WAIT
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.TO-STRING
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.GET-PARTIAL-VALUES
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.IS-READABLE-INTERVAL
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.GET-PERIOD-TYPE
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.GET-INSTANT-MILLIS
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.GET-CHRONOLOGY
                                     |org.joda.time.convert|::ABSTRACTCONVERTER.) "org.joda.time.convert"))
