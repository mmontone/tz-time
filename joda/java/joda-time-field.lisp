(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE
                                              "org.joda.time")
(FOIL:ENSURE-PACKAGE "java.io")
(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "org.joda.time.field")
)(DEFCONSTANT |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.
   '|org.joda.time.field|::|AbstractPartialFieldProperty|)
(DEFCLASS |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-VALUE-OVERALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMaximumValueOverall()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty|
   "getMaximumValueOverall"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-VALUE-OVERALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractPartialFieldProperty.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getDurationField"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getMinimumValue"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MINIMUM-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getMaximumValue"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsText()
public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsText(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getAsText"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-TEXT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsShortText(java.util.Locale)
public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsShortText()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getAsShortText"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-SHORT-TEXT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractPartialFieldProperty.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty|
   "getRangeDurationField"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-RANGE-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty|
   "getMaximumTextLength"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-TEXT-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty|
   "getMaximumShortTextLength"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-SHORT-TEXT-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getAsString"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MINIMUM-VALUE-OVERALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMinimumValueOverall()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty|
   "getMinimumValueOverall"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MINIMUM-VALUE-OVERALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.AbstractPartialFieldProperty.get()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "get"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.AbstractPartialFieldProperty.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "equals"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "toString"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "hashCode"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.compareTo(org.joda.time.ReadableInstant)
public int org.joda.time.field.AbstractPartialFieldProperty.compareTo(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "compareTo"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.COMPARE-TO FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getName()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getName"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-NAME FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DateTimeField org.joda.time.field.AbstractPartialFieldProperty.getField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getField"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-FIELD FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.AbstractPartialFieldProperty.getFieldType()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getFieldType"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-FIELD-TYPE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "wait"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "getClass"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "notify"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractPartialFieldProperty| "notifyAll"
   '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.AS-SHORT-TEXT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsShortText()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "asShortText"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.ASSHORTTEXT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.AS-STRING-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsString()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "asString"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.ASSTRING-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.AS-TEXT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getAsText()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "asText"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.ASTEXT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "class"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractPartialFieldProperty.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "durationField"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DateTimeField org.joda.time.field.AbstractPartialFieldProperty.getField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "field"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.FIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.FIELD-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.AbstractPartialFieldProperty.getFieldType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "fieldType"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.FIELDTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "maximumValue"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MAXIMUM-VALUE-OVERALL-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMaximumValueOverall()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "maximumValueOverall"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MAXIMUMVALUEOVERALL-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "minimumValue"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MINIMUM-VALUE-OVERALL-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractPartialFieldProperty.getMinimumValueOverall()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "minimumValueOverall"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MINIMUMVALUEOVERALL-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractPartialFieldProperty.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "name"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractPartialFieldProperty.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|AbstractPartialFieldProperty|
                       "rangeDurationField"
                       '|org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.
  '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|)
(DEFCLASS |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.
          (|java.io|::SERIALIZABLE. |java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NEW
       (&REST FOIL::ARGS)
  "public org.joda.time.field.AbstractReadableInstantFieldProperty()
"
  (FOIL::CALL-CTOR
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL
              (QUOTE
               |org.joda.time.field|::|AbstractReadableInstantFieldProperty|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NEW
         FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-VALUE-OVERALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMaximumValueOverall()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getMaximumValueOverall"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-VALUE-OVERALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractReadableInstantFieldProperty.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getDurationField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getMinimumValue"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MINIMUM-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getMaximumValue"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getDifference(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getDifference"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DIFFERENCE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsText(java.util.Locale)
public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsText()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "getAsText"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-TEXT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsShortText()
public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsShortText(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getAsShortText"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-SHORT-TEXT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.AbstractReadableInstantFieldProperty.getDifferenceAsLong(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getDifferenceAsLong"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DIFFERENCE-AS-LONG
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractReadableInstantFieldProperty.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getRangeDurationField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-RANGE-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.AbstractReadableInstantFieldProperty.isLeap()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "isLeap"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.IS-LEAP
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getLeapAmount()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getLeapAmount"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-LEAP-AMOUNT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractReadableInstantFieldProperty.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getLeapDurationField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-LEAP-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getMaximumTextLength"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-TEXT-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getMaximumShortTextLength"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-SHORT-TEXT-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.TO-INTERVAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.Interval org.joda.time.field.AbstractReadableInstantFieldProperty.toInterval()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "toInterval"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.TO-INTERVAL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "getAsString"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MINIMUM-VALUE-OVERALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMinimumValueOverall()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getMinimumValueOverall"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MINIMUM-VALUE-OVERALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.get()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "get"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.AbstractReadableInstantFieldProperty.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "equals"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "toString"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "hashCode"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.compareTo(org.joda.time.ReadablePartial)
public int org.joda.time.field.AbstractReadableInstantFieldProperty.compareTo(org.joda.time.ReadableInstant)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "compareTo"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.COMPARE-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getName()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "getName"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-NAME
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DateTimeField org.joda.time.field.AbstractReadableInstantFieldProperty.getField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "getField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-FIELD-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.AbstractReadableInstantFieldProperty.getFieldType()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "getFieldType"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-FIELD-TYPE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.AbstractReadableInstantFieldProperty.remainder()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "remainder"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.REMAINDER
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "wait"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "getClass"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "notify"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "notifyAll"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.AS-SHORT-TEXT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsShortText()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "asShortText"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.ASSHORTTEXT-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.AS-STRING-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsString()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "asString"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.ASSTRING-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.AS-TEXT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getAsText()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "asText"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.ASTEXT-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "class"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractReadableInstantFieldProperty.getDurationField()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "durationField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.DURATIONFIELD-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DateTimeField org.joda.time.field.AbstractReadableInstantFieldProperty.getField()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "field"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.FIELD-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.FIELD-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.AbstractReadableInstantFieldProperty.getFieldType()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "fieldType"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.FIELDTYPE-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.AbstractReadableInstantFieldProperty.isLeap()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "leap"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-AMOUNT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getLeapAmount()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "leapAmount"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAPAMOUNT-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractReadableInstantFieldProperty.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "leapDurationField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAPDURATIONFIELD-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMaximumValue()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "maximumValue"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MAXIMUMVALUE-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MAXIMUM-VALUE-OVERALL-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMaximumValueOverall()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "maximumValueOverall"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MAXIMUMVALUEOVERALL-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMinimumValue()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "minimumValue"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MINIMUMVALUE-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MINIMUM-VALUE-OVERALL-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.AbstractReadableInstantFieldProperty.getMinimumValueOverall()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "minimumValueOverall"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MINIMUMVALUEOVERALL-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.AbstractReadableInstantFieldProperty.getName()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty| "name"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NAME-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.AbstractReadableInstantFieldProperty.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|AbstractReadableInstantFieldProperty|
   "rangeDurationField"
   '|org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.RANGEDURATIONFIELD-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::BASEDATETIMEFIELD.
  '|org.joda.time.field|::|BaseDateTimeField|)
(DEFCLASS |org.joda.time.field|::BASEDATETIMEFIELD.
          (|org.joda.time|::DATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.field.BaseDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMinimumValue()
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public abstract int org.joda.time.field.BaseDateTimeField.getMaximumValue()
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "isLeap"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.BaseDateTimeField.add(long,int)
public long org.joda.time.field.BaseDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "add"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "get"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "toString"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "getName"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "getType"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public abstract long org.joda.time.field.BaseDateTimeField.set(long,int)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "set"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "wait"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "equals"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField| "notify"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::BASEDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField| "class"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField| "lenient"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField| "name"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField| "supported"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDateTimeField| "type"
                       '|org.joda.time.field|::BASEDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::BASEDURATIONFIELD.
  '|org.joda.time.field|::|BaseDurationField|)
(DEFCLASS |org.joda.time.field|::BASEDURATIONFIELD.
          (|org.joda.time|::DURATIONFIELD. |java.io|::SERIALIZABLE.) NIL)
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.getMillis(int,long)
public abstract long org.joda.time.DurationField.getMillis(long,long)
public long org.joda.time.field.BaseDurationField.getMillis(int)
public long org.joda.time.field.BaseDurationField.getMillis(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getMillis"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.getValueAsLong(long,long)
public long org.joda.time.field.BaseDurationField.getValueAsLong(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getDifference"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "toString"
                          '|org.joda.time.field|::BASEDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.BaseDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "compareTo"
                          '|org.joda.time.field|::BASEDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField| "getName"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getValue(long)
public int org.joda.time.field.BaseDurationField.getValue(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getValue"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField| "getType"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "isSupported"
                          '|org.joda.time.field|::BASEDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "subtract"
                          '|org.joda.time.field|::BASEDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::BASEDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.add(long,int)
public abstract long org.joda.time.DurationField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField| "add"
                          '|org.joda.time.field|::BASEDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField| "wait"
                          '|org.joda.time.field|::BASEDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField| "equals"
                          '|org.joda.time.field|::BASEDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "hashCode"
                          '|org.joda.time.field|::BASEDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "getClass"
                          '|org.joda.time.field|::BASEDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField| "notify"
                          '|org.joda.time.field|::BASEDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|BaseDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::BASEDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDurationField| "class"
                       '|org.joda.time.field|::BASEDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDurationField| "name"
                       '|org.joda.time.field|::BASEDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDurationField| "precise"
                       '|org.joda.time.field|::BASEDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDurationField| "supported"
                       '|org.joda.time.field|::BASEDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDurationField| "type"
                       '|org.joda.time.field|::BASEDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::BASEDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|BaseDurationField| "unitMillis"
                       '|org.joda.time.field|::BASEDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::DECORATEDDATETIMEFIELD.
  '|org.joda.time.field|::|DecoratedDateTimeField|)
(DEFCLASS |org.joda.time.field|::DECORATEDDATETIMEFIELD.
          (|org.joda.time.field|::BASEDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DecoratedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DecoratedDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DecoratedDateTimeField.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DecoratedDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "get"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DecoratedDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "set"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.BaseDateTimeField.add(long,int)
public long org.joda.time.field.BaseDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "add"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "toString"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getName"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getType"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "wait"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "equals"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "notify"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::DECORATEDDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField| "class"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DecoratedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DecoratedDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField| "name"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "supported"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField| "type"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::DECORATEDDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::DECORATEDDURATIONFIELD.
  '|org.joda.time.field|::|DecoratedDurationField|)
(DEFCLASS |org.joda.time.field|::DECORATEDDURATIONFIELD.
          (|org.joda.time.field|::BASEDURATIONFIELD.) NIL)
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.DecoratedDurationField(org.joda.time.DurationField,org.joda.time.DurationFieldType)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|DecoratedDurationField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|DecoratedDurationField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::DECORATEDDURATIONFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDurationField.getMillis(int)
public long org.joda.time.field.BaseDurationField.getMillis(long)
public long org.joda.time.field.DecoratedDurationField.getMillis(long,long)
public long org.joda.time.field.DecoratedDurationField.getMillis(int,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getMillis"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDurationField.getValueAsLong(long)
public long org.joda.time.field.DecoratedDurationField.getValueAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DecoratedDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DecoratedDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.DecoratedDurationField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getWrappedField"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DecoratedDurationField.add(long,long)
public long org.joda.time.field.DecoratedDurationField.add(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "add"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getDifference"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "toString"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.BaseDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "compareTo"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getName"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getValue(long)
public int org.joda.time.field.BaseDurationField.getValue(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getValue"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getType"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "isSupported"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "subtract"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "wait"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "equals"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "hashCode"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "getClass"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "notify"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DecoratedDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::DECORATEDDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField| "class"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField| "name"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField|
                       "precise"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField|
                       "supported"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField| "type"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DecoratedDurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField|
                       "unitMillis"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DECORATEDDURATIONFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.DecoratedDurationField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DecoratedDurationField|
                       "wrappedField"
                       '|org.joda.time.field|::DECORATEDDURATIONFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::DELEGATEDDATETIMEFIELD.
  '|org.joda.time.field|::|DelegatedDateTimeField|)
(DEFCLASS |org.joda.time.field|::DELEGATEDDATETIMEFIELD.
          (|org.joda.time|::DATETIMEFIELD. |java.io|::SERIALIZABLE.) NIL)
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.DelegatedDateTimeField(org.joda.time.DateTimeField)
public org.joda.time.field.DelegatedDateTimeField(org.joda.time.DateTimeField,org.joda.time.DateTimeFieldType)
public org.joda.time.field.DelegatedDateTimeField(org.joda.time.DateTimeField,org.joda.time.DurationField,org.joda.time.DateTimeFieldType)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|DelegatedDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|DelegatedDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::DELEGATEDDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(long)
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.DelegatedDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.add(long,long)
public long org.joda.time.field.DelegatedDateTimeField.add(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "add"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "get"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "toString"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getName"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getType"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.DelegatedDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "set"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "wait"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "equals"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "notify"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField| "class"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField| "name"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "supported"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField| "type"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::DELEGATEDDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::DELEGATEDDURATIONFIELD.
  '|org.joda.time.field|::|DelegatedDurationField|)
(DEFCLASS |org.joda.time.field|::DELEGATEDDURATIONFIELD.
          (|org.joda.time|::DURATIONFIELD. |java.io|::SERIALIZABLE.) NIL)
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDurationField.getMillis(long)
public long org.joda.time.field.DelegatedDurationField.getMillis(long,long)
public long org.joda.time.field.DelegatedDurationField.getMillis(int)
public long org.joda.time.field.DelegatedDurationField.getMillis(int,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getMillis"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDurationField.getValueAsLong(long,long)
public long org.joda.time.field.DelegatedDurationField.getValueAsLong(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getDifference"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.DelegatedDurationField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getWrappedField"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDurationField.add(long,long)
public long org.joda.time.field.DelegatedDurationField.add(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "add"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDurationField.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "equals"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "toString"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDurationField.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "hashCode"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.DelegatedDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "compareTo"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getName"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDurationField.getValue(long,long)
public int org.joda.time.field.DelegatedDurationField.getValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getValue"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType org.joda.time.field.DelegatedDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getType"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "isSupported"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "subtract"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "wait"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "getClass"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "notify"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DelegatedDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::DELEGATEDDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField| "class"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField| "name"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField|
                       "precise"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField|
                       "supported"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType org.joda.time.field.DelegatedDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField| "type"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField|
                       "unitMillis"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DELEGATEDDURATIONFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.DelegatedDurationField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DelegatedDurationField|
                       "wrappedField"
                       '|org.joda.time.field|::DELEGATEDDURATIONFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::DIVIDEDDATETIMEFIELD.
  '|org.joda.time.field|::|DividedDateTimeField|)
(DEFCLASS |org.joda.time.field|::DIVIDEDDATETIMEFIELD.
          (|org.joda.time.field|::DECORATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.DividedDateTimeField(org.joda.time.DateTimeField,org.joda.time.DurationField,org.joda.time.DateTimeFieldType,int)
public org.joda.time.field.DividedDateTimeField(org.joda.time.field.RemainderDateTimeField,org.joda.time.DateTimeFieldType)
public org.joda.time.field.DividedDateTimeField(org.joda.time.DateTimeField,org.joda.time.DateTimeFieldType,int)
public org.joda.time.field.DividedDateTimeField(org.joda.time.field.RemainderDateTimeField,org.joda.time.DurationField,org.joda.time.DateTimeFieldType)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|DividedDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|DividedDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::DIVIDEDDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DividedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DividedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DividedDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DividedDateTimeField.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DividedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.DividedDateTimeField.addWrapField(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DividedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DividedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIVISOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DividedDateTimeField.getDivisor()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getDivisor"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIVISOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.DividedDateTimeField.add(long,int)
public long org.joda.time.field.DividedDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField| "add"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DividedDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField| "get"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DividedDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField| "set"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DividedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "toString"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getName"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getType"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField| "wait"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "equals"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "notify"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|DividedDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField| "class"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.DIVISOR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DividedDateTimeField.getDivisor()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField| "divisor"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.DIVISOR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DividedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField| "lenient"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DividedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DividedDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField| "name"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DividedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "supported"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField| "type"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::DIVIDEDDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|DividedDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::DIVIDEDDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::FIELDUTILS.
  '|org.joda.time.field|::|FieldUtils|)
(DEFCLASS |org.joda.time.field|::FIELDUTILS. (|java.lang|::OBJECT.) NIL)
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-NEGATE (&REST FOIL::ARGS)
  "public static int org.joda.time.field.FieldUtils.safeNegate(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "safeNegate"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-NEGATE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-MULTIPLY (&REST FOIL::ARGS)
  "public static long org.joda.time.field.FieldUtils.safeMultiply(long,long)
public static int org.joda.time.field.FieldUtils.safeMultiply(int,int)
public static long org.joda.time.field.FieldUtils.safeMultiply(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "safeMultiply"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-MULTIPLY NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-ADD (&REST FOIL::ARGS)
  "public static long org.joda.time.field.FieldUtils.safeAdd(long,long)
public static int org.joda.time.field.FieldUtils.safeAdd(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "safeAdd"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-ADD NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-SUBTRACT (&REST FOIL::ARGS)
  "public static long org.joda.time.field.FieldUtils.safeSubtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "safeSubtract"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-SUBTRACT NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-TO-INT (&REST FOIL::ARGS)
  "public static int org.joda.time.field.FieldUtils.safeToInt(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "safeToInt"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-TO-INT NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-DIVIDE (&REST FOIL::ARGS)
  "public static long org.joda.time.field.FieldUtils.safeDivide(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "safeDivide"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-DIVIDE NIL
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.VERIFY-VALUE-BOUNDS (&REST FOIL::ARGS)
  "public static void org.joda.time.field.FieldUtils.verifyValueBounds(org.joda.time.DateTimeField,int,int,int)
public static void org.joda.time.field.FieldUtils.verifyValueBounds(org.joda.time.DateTimeFieldType,int,int,int)
public static void org.joda.time.field.FieldUtils.verifyValueBounds(java.lang.String,int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils|
                          "verifyValueBounds"
                          '|org.joda.time.field|::FIELDUTILS.VERIFY-VALUE-BOUNDS
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.GET-WRAPPED-VALUE (&REST FOIL::ARGS)
  "public static int org.joda.time.field.FieldUtils.getWrappedValue(int,int,int)
public static int org.joda.time.field.FieldUtils.getWrappedValue(int,int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils|
                          "getWrappedValue"
                          '|org.joda.time.field|::FIELDUTILS.GET-WRAPPED-VALUE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.SAFE-MULTIPLY-TO-INT
       (&REST FOIL::ARGS)
  "public static int org.joda.time.field.FieldUtils.safeMultiplyToInt(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils|
                          "safeMultiplyToInt"
                          '|org.joda.time.field|::FIELDUTILS.SAFE-MULTIPLY-TO-INT
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
public static boolean org.joda.time.field.FieldUtils.equals(java.lang.Object,java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "equals"
                          '|org.joda.time.field|::FIELDUTILS.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "wait"
                          '|org.joda.time.field|::FIELDUTILS.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "toString"
                          '|org.joda.time.field|::FIELDUTILS.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "hashCode"
                          '|org.joda.time.field|::FIELDUTILS.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "getClass"
                          '|org.joda.time.field|::FIELDUTILS.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "notify"
                          '|org.joda.time.field|::FIELDUTILS.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|FieldUtils| "notifyAll"
                          '|org.joda.time.field|::FIELDUTILS.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::FIELDUTILS.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|FieldUtils| "class"
                       '|org.joda.time.field|::FIELDUTILS.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::IMPRECISEDATETIMEFIELD.
  '|org.joda.time.field|::|ImpreciseDateTimeField|)
(DEFCLASS |org.joda.time.field|::IMPRECISEDATETIMEFIELD.
          (|org.joda.time.field|::BASEDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.ImpreciseDateTimeField(org.joda.time.DateTimeFieldType,long)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|ImpreciseDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|ImpreciseDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::IMPRECISEDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.field.ImpreciseDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.ImpreciseDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ImpreciseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ImpreciseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.ImpreciseDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public abstract long org.joda.time.field.ImpreciseDateTimeField.add(long,int)
public abstract long org.joda.time.field.ImpreciseDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "add"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.ImpreciseDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "get"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public abstract long org.joda.time.field.ImpreciseDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "set"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMinimumValue()
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public abstract int org.joda.time.field.BaseDateTimeField.getMaximumValue()
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "toString"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getName"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getType"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "wait"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "equals"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "notify"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ImpreciseDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField| "class"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.ImpreciseDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField| "name"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.ImpreciseDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField|
                       "supported"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ImpreciseDateTimeField| "type"
                       '|org.joda.time.field|::IMPRECISEDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.
  '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|)
(DEFCLASS |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.
          (|org.joda.time.field|::BASEDURATIONFIELD.) NIL)
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDurationField.getMillis(int)
public long org.joda.time.field.BaseDurationField.getMillis(long)
public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getMillis(int,long)
public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getMillis(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getMillis"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-MILLIS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDurationField.getValueAsLong(long)
public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getValueAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getValueAsLong"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-VALUE-AS-LONG
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getUnitMillis"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-UNIT-MILLIS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getDifference"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-DIFFERENCE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "isPrecise"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.IS-PRECISE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getDifferenceAsLong"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.add(long,long)
public long org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.add(long,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField| "add"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.ADD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getValue(long)
public int org.joda.time.field.ImpreciseDateTimeField$LinkedDurationField.getValue(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getValue"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "toString"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.TO-STRING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.BaseDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "compareTo"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.COMPARE-TO
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getName"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-NAME
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getType"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-TYPE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "isSupported"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.IS-SUPPORTED
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "subtract"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.SUBTRACT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField| "wait"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.WAIT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "equals"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.EQUALS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "hashCode"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.HASH-CODE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "getClass"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-CLASS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "notify"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NOTIFY
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "notifyAll"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NOTIFY-ALL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField| "class"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.CLASS-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField| "name"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NAME-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean org.joda.time.DurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "precise"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.PRECISE-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "supported"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.SUPPORTED-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField| "type"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.TYPE-GET
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract long org.joda.time.DurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET
   '|org.joda.time.field|::|ImpreciseDateTimeField$LinkedDurationField|
   "unitMillis"
   '|org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.UNITMILLIS-GET
   FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::LENIENTDATETIMEFIELD.
  '|org.joda.time.field|::|LenientDateTimeField|)
(DEFCLASS |org.joda.time.field|::LENIENTDATETIMEFIELD.
          (|org.joda.time.field|::DELEGATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.LenientDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-INSTANCE
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeField org.joda.time.field.LenientDateTimeField.getInstance(org.joda.time.DateTimeField,org.joda.time.Chronology)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getInstance"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-INSTANCE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.LenientDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField| "set"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(long)
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.DelegatedDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.add(long,long)
public long org.joda.time.field.DelegatedDateTimeField.add(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField| "add"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField| "get"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "toString"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getName"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getType"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField| "wait"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "equals"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "notify"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|LenientDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::LENIENTDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField| "class"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.LenientDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField| "lenient"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField| "name"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "supported"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField| "type"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::LENIENTDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|LenientDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::LENIENTDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::MILLISDURATIONFIELD.
  '|org.joda.time.field|::|MillisDurationField|)
(DEFCLASS |org.joda.time.field|::MILLISDURATIONFIELD.
          (|org.joda.time|::DURATIONFIELD. |java.io|::SERIALIZABLE.) NIL)
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.MillisDurationField.getMillis(int,long)
public long org.joda.time.field.MillisDurationField.getMillis(long,long)
public long org.joda.time.field.MillisDurationField.getMillis(int)
public long org.joda.time.field.MillisDurationField.getMillis(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getMillis"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.MillisDurationField.getValueAsLong(long,long)
public long org.joda.time.field.MillisDurationField.getValueAsLong(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.MillisDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.MillisDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getDifference"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.MillisDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.MillisDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.MillisDurationField.add(long,int)
public long org.joda.time.field.MillisDurationField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField| "add"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.MillisDurationField.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "equals"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.MillisDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "toString"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.MillisDurationField.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "hashCode"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.MillisDurationField.compareTo(org.joda.time.DurationField)
public int org.joda.time.field.MillisDurationField.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "compareTo"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.MillisDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getName"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.MillisDurationField.getValue(long,long)
public int org.joda.time.field.MillisDurationField.getValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getValue"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType org.joda.time.field.MillisDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getType"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.MillisDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "isSupported"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "subtract"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField| "wait"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "getClass"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "notify"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|MillisDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::MILLISDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.+INSTANCE+* ()
  "public static final org.joda.time.DurationField org.joda.time.field.MillisDurationField.INSTANCE"
  (FOIL::CALL-FIELD '|org.joda.time.field|::|MillisDurationField| "INSTANCE"
                    '|org.joda.time.field|::MILLISDURATIONFIELD.+INSTANCE+*
                    NIL))
(DEFUN (SETF |org.joda.time.field|::MILLISDURATIONFIELD.+INSTANCE+*)
       (FOIL::VAL)
  (FOIL::CALL-FIELD '|org.joda.time.field|::|MillisDurationField| "INSTANCE"
                    '|org.joda.time.field|::MILLISDURATIONFIELD.+INSTANCE+* NIL
                    FOIL::VAL))
(DEFINE-SYMBOL-MACRO |org.joda.time.field|::+MILLISDURATIONFIELD.+INSTANCE+*+
                     (|org.joda.time.field|::MILLISDURATIONFIELD.+INSTANCE+*))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|MillisDurationField| "class"
                       '|org.joda.time.field|::MILLISDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.MillisDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|MillisDurationField| "name"
                       '|org.joda.time.field|::MILLISDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.MillisDurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|MillisDurationField| "precise"
                       '|org.joda.time.field|::MILLISDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.MillisDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|MillisDurationField|
                       "supported"
                       '|org.joda.time.field|::MILLISDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationFieldType org.joda.time.field.MillisDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|MillisDurationField| "type"
                       '|org.joda.time.field|::MILLISDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::MILLISDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.MillisDurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|MillisDurationField|
                       "unitMillis"
                       '|org.joda.time.field|::MILLISDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::OFFSETDATETIMEFIELD.
  '|org.joda.time.field|::|OffsetDateTimeField|)
(DEFCLASS |org.joda.time.field|::OFFSETDATETIMEFIELD.
          (|org.joda.time.field|::DECORATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.OffsetDateTimeField(org.joda.time.DateTimeField,org.joda.time.DateTimeFieldType,int,int,int)
public org.joda.time.field.OffsetDateTimeField(org.joda.time.DateTimeField,org.joda.time.DateTimeFieldType,int)
public org.joda.time.field.OffsetDateTimeField(org.joda.time.DateTimeField,int)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|OffsetDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|OffsetDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::OFFSETDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.OffsetDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.OffsetDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.OffsetDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.OffsetDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.OffsetDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.OffsetDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.OffsetDateTimeField.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.OffsetDateTimeField.addWrapField(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.OffsetDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.OffsetDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.OffsetDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.OffsetDateTimeField.add(long,long)
public long org.joda.time.field.OffsetDateTimeField.add(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField| "add"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.OffsetDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField| "get"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.OffsetDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField| "set"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.OffsetDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-OFFSET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.OffsetDateTimeField.getOffset()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getOffset"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "toString"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getName"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getType"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField| "wait"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "equals"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "notify"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|OffsetDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::OFFSETDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField| "class"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.OffsetDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField| "lenient"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.OffsetDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.OffsetDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField| "name"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.OFFSET-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.OffsetDateTimeField.getOffset()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField| "offset"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.OFFSET-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "supported"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField| "type"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::OFFSETDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|OffsetDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::OFFSETDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::PRECISEDATETIMEFIELD.
  '|org.joda.time.field|::|PreciseDateTimeField|)
(DEFCLASS |org.joda.time.field|::PRECISEDATETIMEFIELD.
          (|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.PreciseDateTimeField(org.joda.time.DateTimeFieldType,org.joda.time.DurationField,org.joda.time.DurationField)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|PreciseDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|PreciseDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::PRECISEDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.PreciseDateTimeField.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.PreciseDateTimeField.addWrapField(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.PreciseDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-RANGE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDateTimeField.getRange()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getRange"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-RANGE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField| "get"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.PreciseDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField| "set"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.PreciseDurationDateTimeField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getUnitMillis"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.PreciseDurationDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.PreciseDurationDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.PreciseDurationDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.BaseDateTimeField.add(long,int)
public long org.joda.time.field.BaseDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField| "add"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "toString"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getName"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getType"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField| "wait"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "equals"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "notify"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::PRECISEDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField| "class"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.PreciseDurationDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.PreciseDurationDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField| "lenient"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDurationDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField| "name"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.RANGE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDateTimeField.getRange()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField| "range"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.RANGE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.PreciseDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "supported"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField| "type"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDATETIMEFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.PreciseDurationDateTimeField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDateTimeField|
                       "unitMillis"
                       '|org.joda.time.field|::PRECISEDATETIMEFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.
  '|org.joda.time.field|::|PreciseDurationDateTimeField|)
(DEFCLASS |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.
          (|org.joda.time.field|::BASEDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NEW
       (&REST FOIL::ARGS)
  "public org.joda.time.field.PreciseDurationDateTimeField(org.joda.time.DateTimeFieldType,org.joda.time.DurationField)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|PreciseDurationDateTimeField|
                   FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL
              (QUOTE |org.joda.time.field|::|PreciseDurationDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "roundFloor"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-FLOOR FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "roundCeiling"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-CEILING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.PreciseDurationDateTimeField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getUnitMillis"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-UNIT-MILLIS
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.PreciseDurationDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getDurationField"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.PreciseDurationDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getMinimumValue"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MINIMUM-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.PreciseDurationDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "isLenient"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-LENIENT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.PreciseDurationDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "set"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SET FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "remainder"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.REMAINDER FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "roundHalfFloor"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-FLOOR
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "roundHalfCeiling"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-CEILING
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "roundHalfEven"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-EVEN
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public abstract int org.joda.time.field.BaseDateTimeField.getMaximumValue()
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getMaximumValue"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-VALUE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getDifference"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DIFFERENCE
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getAsText"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-AS-TEXT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getAsShortText"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-AS-SHORT-TEXT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "addWrapPartial"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD-WRAP-PARTIAL
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "addWrapField"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD-WRAP-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getDifferenceAsLong"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField|
   "getRangeDurationField"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-RANGE-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "isLeap"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-LEAP FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getLeapAmount"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-LEAP-AMOUNT
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField|
   "getLeapDurationField"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-LEAP-DURATION-FIELD
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField|
   "getMaximumTextLength"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField|
   "getMaximumShortTextLength"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
   FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.BaseDateTimeField.add(long,int)
public long org.joda.time.field.BaseDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "add"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "get"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "toString"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.TO-STRING FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getName"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-NAME FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getType"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-TYPE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "isSupported"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-SUPPORTED FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "setExtended"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SET-EXTENDED FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "wait"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.WAIT FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "equals"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.EQUALS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "hashCode"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.HASH-CODE FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "getClass"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-CLASS FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "notify"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NOTIFY FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD
   '|org.joda.time.field|::|PreciseDurationDateTimeField| "notifyAll"
   '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NOTIFY-ALL FOIL::THIS
   FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "class"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.PreciseDurationDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.PreciseDurationDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int org.joda.time.field.BaseDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDurationDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "name"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "supported"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "type"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.PreciseDurationDateTimeField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationDateTimeField|
                       "unitMillis"
                       '|org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::PRECISEDURATIONFIELD.
  '|org.joda.time.field|::|PreciseDurationField|)
(DEFCLASS |org.joda.time.field|::PRECISEDURATIONFIELD.
          (|org.joda.time.field|::BASEDURATIONFIELD.) NIL)
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.PreciseDurationField(org.joda.time.DurationFieldType,long)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|PreciseDurationField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|PreciseDurationField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::PRECISEDURATIONFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDurationField.getMillis(int)
public long org.joda.time.field.BaseDurationField.getMillis(long)
public long org.joda.time.field.PreciseDurationField.getMillis(int,long)
public long org.joda.time.field.PreciseDurationField.getMillis(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getMillis"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDurationField.getValueAsLong(long)
public long org.joda.time.field.PreciseDurationField.getValueAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.PreciseDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.PreciseDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.PreciseDurationField.add(long,long)
public long org.joda.time.field.PreciseDurationField.add(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField| "add"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.PreciseDurationField.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "equals"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.PreciseDurationField.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "hashCode"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getDifference"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "toString"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.BaseDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "compareTo"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getName"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.getValue(long)
public int org.joda.time.field.BaseDurationField.getValue(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getValue"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getType"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "isSupported"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "subtract"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField| "wait"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "getClass"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "notify"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|PreciseDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::PRECISEDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationField| "class"
                       '|org.joda.time.field|::PRECISEDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationField| "name"
                       '|org.joda.time.field|::PRECISEDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.PreciseDurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationField| "precise"
                       '|org.joda.time.field|::PRECISEDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationField|
                       "supported"
                       '|org.joda.time.field|::PRECISEDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationField| "type"
                       '|org.joda.time.field|::PRECISEDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::PRECISEDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final long org.joda.time.field.PreciseDurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|PreciseDurationField|
                       "unitMillis"
                       '|org.joda.time.field|::PRECISEDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::REMAINDERDATETIMEFIELD.
  '|org.joda.time.field|::|RemainderDateTimeField|)
(DEFCLASS |org.joda.time.field|::REMAINDERDATETIMEFIELD.
          (|org.joda.time.field|::DECORATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.RemainderDateTimeField(org.joda.time.field.DividedDateTimeField,org.joda.time.DateTimeFieldType)
public org.joda.time.field.RemainderDateTimeField(org.joda.time.field.DividedDateTimeField)
public org.joda.time.field.RemainderDateTimeField(org.joda.time.DateTimeField,org.joda.time.DurationField,org.joda.time.DateTimeFieldType,int)
public org.joda.time.field.RemainderDateTimeField(org.joda.time.DateTimeField,org.joda.time.DateTimeFieldType,int)
public org.joda.time.field.RemainderDateTimeField(org.joda.time.field.DividedDateTimeField,org.joda.time.DurationField,org.joda.time.DateTimeFieldType)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|RemainderDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|RemainderDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::REMAINDERDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.RemainderDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.RemainderDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.RemainderDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.RemainderDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.RemainderDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.RemainderDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMinimumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.RemainderDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumValue(long)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.BaseDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.RemainderDateTimeField.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.RemainderDateTimeField.addWrapField(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.RemainderDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIVISOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.RemainderDateTimeField.getDivisor()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getDivisor"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIVISOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.RemainderDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "get"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.RemainderDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "set"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.RemainderDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.BaseDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.BaseDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.BaseDateTimeField.add(long,int)
public long org.joda.time.field.BaseDateTimeField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "add"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "toString"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getName"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getType"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "wait"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "equals"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "notify"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|RemainderDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::REMAINDERDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField| "class"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.DIVISOR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.RemainderDateTimeField.getDivisor()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "divisor"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.DIVISOR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.RemainderDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.BaseDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.RemainderDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.RemainderDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField| "name"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.RemainderDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "supported"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField| "type"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::REMAINDERDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|RemainderDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::REMAINDERDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::SCALEDDURATIONFIELD.
  '|org.joda.time.field|::|ScaledDurationField|)
(DEFCLASS |org.joda.time.field|::SCALEDDURATIONFIELD.
          (|org.joda.time.field|::DECORATEDDURATIONFIELD.) NIL)
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.ScaledDurationField(org.joda.time.DurationField,org.joda.time.DurationFieldType,int)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|ScaledDurationField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|ScaledDurationField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::SCALEDDURATIONFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ScaledDurationField.getMillis(long,long)
public long org.joda.time.field.ScaledDurationField.getMillis(int)
public long org.joda.time.field.ScaledDurationField.getMillis(long)
public long org.joda.time.field.ScaledDurationField.getMillis(int,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getMillis"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ScaledDurationField.getValueAsLong(long,long)
public long org.joda.time.field.ScaledDurationField.getValueAsLong(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ScaledDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ScaledDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getDifference"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ScaledDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-SCALAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ScaledDurationField.getScalar()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getScalar"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-SCALAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ScaledDurationField.add(long,int)
public long org.joda.time.field.ScaledDurationField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField| "add"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.ScaledDurationField.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "equals"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ScaledDurationField.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "hashCode"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ScaledDurationField.getValue(long,long)
public int org.joda.time.field.ScaledDurationField.getValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getValue"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.DecoratedDurationField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getWrappedField"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "toString"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.BaseDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "compareTo"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getName"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getType"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "isSupported"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "subtract"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField| "wait"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "getClass"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "notify"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ScaledDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::SCALEDDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField| "class"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField| "name"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField| "precise"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.SCALAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ScaledDurationField.getScalar()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField| "scalar"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.SCALAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField|
                       "supported"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.BaseDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField| "type"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ScaledDurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField|
                       "unitMillis"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SCALEDDURATIONFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationField org.joda.time.field.DecoratedDurationField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ScaledDurationField|
                       "wrappedField"
                       '|org.joda.time.field|::SCALEDDURATIONFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::SKIPDATETIMEFIELD.
  '|org.joda.time.field|::|SkipDateTimeField|)
(DEFCLASS |org.joda.time.field|::SKIPDATETIMEFIELD.
          (|org.joda.time.field|::DELEGATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.SkipDateTimeField(org.joda.time.Chronology,org.joda.time.DateTimeField)
public org.joda.time.field.SkipDateTimeField(org.joda.time.Chronology,org.joda.time.DateTimeField,int)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|SkipDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|SkipDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::SKIPDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(long)
public int org.joda.time.field.SkipDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.SkipDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "get"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.SkipDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "set"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(long)
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.DelegatedDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "isLeap"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.add(long,long)
public long org.joda.time.field.DelegatedDateTimeField.add(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "add"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "toString"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "getName"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "getType"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "wait"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "equals"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField| "notify"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::SKIPDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField| "class"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField| "lenient"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.SkipDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField| "name"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField| "supported"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField| "type"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::SKIPDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::SKIPUNDODATETIMEFIELD.
  '|org.joda.time.field|::|SkipUndoDateTimeField|)
(DEFCLASS |org.joda.time.field|::SKIPUNDODATETIMEFIELD.
          (|org.joda.time.field|::DELEGATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.SkipUndoDateTimeField(org.joda.time.Chronology,org.joda.time.DateTimeField)
public org.joda.time.field.SkipUndoDateTimeField(org.joda.time.Chronology,org.joda.time.DateTimeField,int)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|SkipUndoDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|SkipUndoDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::SKIPUNDODATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(long)
public int org.joda.time.field.SkipUndoDateTimeField.getMinimumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.SkipUndoDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField| "get"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.SkipUndoDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField| "set"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(long)
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.DelegatedDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.add(long,long)
public long org.joda.time.field.DelegatedDateTimeField.add(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField| "add"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "toString"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getName"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getType"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "wait"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "equals"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "notify"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|SkipUndoDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField| "class"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.SkipUndoDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField| "name"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "supported"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField| "type"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::SKIPUNDODATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|SkipUndoDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::SKIPUNDODATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::STRICTDATETIMEFIELD.
  '|org.joda.time.field|::|StrictDateTimeField|)
(DEFCLASS |org.joda.time.field|::STRICTDATETIMEFIELD.
          (|org.joda.time.field|::DELEGATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.StrictDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-INSTANCE
       (&REST FOIL::ARGS)
  "public static org.joda.time.DateTimeField org.joda.time.field.StrictDateTimeField.getInstance(org.joda.time.DateTimeField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getInstance"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-INSTANCE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.DelegatedDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public int[] org.joda.time.field.DelegatedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.StrictDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField| "set"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(long)
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long)
public java.lang.String org.joda.time.field.DelegatedDateTimeField.getAsShortText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.DelegatedDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.add(long,long)
public long org.joda.time.field.DelegatedDateTimeField.add(long,int)
public int[] org.joda.time.field.DelegatedDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField| "add"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField| "get"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "toString"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getName"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getType"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.DelegatedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField| "wait"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "equals"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "notify"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|StrictDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::STRICTDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField| "class"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.StrictDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField| "lenient"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.DelegatedDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.DelegatedDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField| "name"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DelegatedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DelegatedDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "supported"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.DelegatedDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField| "type"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::STRICTDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DelegatedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|StrictDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::STRICTDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.
  '|org.joda.time.field|::|UnsupportedDateTimeField|)
(DEFCLASS |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.
          (|org.joda.time|::DATETIMEFIELD. |java.io|::SERIALIZABLE.) NIL)
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.UnsupportedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getMinimumValue()
public int org.joda.time.field.UnsupportedDateTimeField.getMinimumValue(long)
public int org.joda.time.field.UnsupportedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.UnsupportedDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.UnsupportedDateTimeField.getMaximumValue(long)
public int org.joda.time.field.UnsupportedDateTimeField.getMaximumValue()
public int org.joda.time.field.UnsupportedDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsText(long)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsText(long,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsText(int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsShortText(int,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getAsShortText(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.UnsupportedDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.UnsupportedDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.UnsupportedDateTimeField.addWrapField(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.UnsupportedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.UnsupportedDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.add(long,int)
public long org.joda.time.field.UnsupportedDateTimeField.add(long,long)
public int[] org.joda.time.field.UnsupportedDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "add"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "get"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "toString"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getName"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-INSTANCE
       (&REST FOIL::ARGS)
  "public static synchronized org.joda.time.field.UnsupportedDateTimeField org.joda.time.field.UnsupportedDateTimeField.getInstance(org.joda.time.DateTimeFieldType,org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getInstance"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-INSTANCE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.UnsupportedDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getType"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.UnsupportedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.UnsupportedDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.UnsupportedDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.UnsupportedDateTimeField.set(long,int)
public long org.joda.time.field.UnsupportedDateTimeField.set(long,java.lang.String,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "set"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "wait"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "equals"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "notify"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "class"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.UnsupportedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.UnsupportedDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "name"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.UnsupportedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "supported"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DateTimeFieldType org.joda.time.field.UnsupportedDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDateTimeField|
                       "type"
                       '|org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.
  '|org.joda.time.field|::|UnsupportedDurationField|)
(DEFCLASS |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.
          (|org.joda.time|::DURATIONFIELD. |java.io|::SERIALIZABLE.) NIL)
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDurationField.getMillis(long,long)
public long org.joda.time.field.UnsupportedDurationField.getMillis(int,long)
public long org.joda.time.field.UnsupportedDurationField.getMillis(long)
public long org.joda.time.field.UnsupportedDurationField.getMillis(int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getMillis"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-VALUE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDurationField.getValueAsLong(long,long)
public long org.joda.time.field.UnsupportedDurationField.getValueAsLong(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getValueAsLong"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-VALUE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-UNIT-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDurationField.getUnitMillis()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getUnitMillis"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-UNIT-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDurationField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getDifference"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.IS-PRECISE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDurationField.isPrecise()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "isPrecise"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.IS-PRECISE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDurationField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDurationField.add(long,int)
public long org.joda.time.field.UnsupportedDurationField.add(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "add"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDurationField.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "equals"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDurationField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "toString"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDurationField.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "hashCode"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDurationField.compareTo(java.lang.Object)
public int org.joda.time.field.UnsupportedDurationField.compareTo(org.joda.time.DurationField)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "compareTo"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDurationField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getName"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.UnsupportedDurationField.getValue(long,long)
public int org.joda.time.field.UnsupportedDurationField.getValue(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getValue"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-INSTANCE
       (&REST FOIL::ARGS)
  "public static synchronized org.joda.time.field.UnsupportedDurationField org.joda.time.field.UnsupportedDurationField.getInstance(org.joda.time.DurationFieldType)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getInstance"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-INSTANCE
                          NIL FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.UnsupportedDurationField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getType"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDurationField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "isSupported"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.SUBTRACT
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DurationField.subtract(long,int)
public long org.joda.time.DurationField.subtract(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "subtract"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.SUBTRACT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "wait"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "getClass"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "notify"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|UnsupportedDurationField|
                          "notifyAll"
                          '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDurationField|
                       "class"
                       '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.UnsupportedDurationField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDurationField|
                       "name"
                       '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.PRECISE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDurationField.isPrecise()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDurationField|
                       "precise"
                       '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.PRECISE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.UnsupportedDurationField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDurationField|
                       "supported"
                       '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DurationFieldType org.joda.time.field.UnsupportedDurationField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDurationField|
                       "type"
                       '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.UNIT-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.UnsupportedDurationField.getUnitMillis()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|UnsupportedDurationField|
                       "unitMillis"
                       '|org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.UNITMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.
  '|org.joda.time.field|::|ZeroIsMaxDateTimeField|)
(DEFCLASS |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.
          (|org.joda.time.field|::DECORATEDDATETIMEFIELD.) NIL)
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NEW (&REST FOIL::ARGS)
  "public org.joda.time.field.ZeroIsMaxDateTimeField(org.joda.time.DateTimeField,org.joda.time.DateTimeFieldType)
"
  (FOIL::CALL-CTOR '|org.joda.time.field|::|ZeroIsMaxDateTimeField| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM
             (EQL (QUOTE |org.joda.time.field|::|ZeroIsMaxDateTimeField|)))
            &REST FOIL::ARGS)
  (APPLY #'|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NEW FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.roundFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "roundFloor"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.roundCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "roundCeiling"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-FLOOR
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.roundHalfFloor(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "roundHalfFloor"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-FLOOR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-CEILING
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.roundHalfCeiling(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "roundHalfCeiling"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-CEILING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-EVEN
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.roundHalfEven(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "roundHalfEven"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-EVEN
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MINIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.getMinimumValue(long)
public int org.joda.time.field.ZeroIsMaxDateTimeField.getMinimumValue()
public int org.joda.time.field.ZeroIsMaxDateTimeField.getMinimumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.ZeroIsMaxDateTimeField.getMinimumValue(org.joda.time.ReadablePartial)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getMinimumValue"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MINIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-VALUE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.getMaximumValue(org.joda.time.ReadablePartial,int[])
public int org.joda.time.field.ZeroIsMaxDateTimeField.getMaximumValue(long)
public int org.joda.time.field.ZeroIsMaxDateTimeField.getMaximumValue(org.joda.time.ReadablePartial)
public int org.joda.time.field.ZeroIsMaxDateTimeField.getMaximumValue()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getMaximumValue"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-VALUE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DIFFERENCE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.getDifference(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getDifference"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DIFFERENCE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD-WRAP-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.addWrapField(long,int)
public int[] org.joda.time.field.ZeroIsMaxDateTimeField.addWrapField(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "addWrapField"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD-WRAP-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.getDifferenceAsLong(long,long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getDifferenceAsLong"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-LEAP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.ZeroIsMaxDateTimeField.isLeap(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "isLeap"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-LEAP
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-LEAP-AMOUNT
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.getLeapAmount(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getLeapAmount"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-LEAP-AMOUNT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-LEAP-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.ZeroIsMaxDateTimeField.getLeapDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getLeapDurationField"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.add(org.joda.time.ReadablePartial,int,int[],int)
public long org.joda.time.field.ZeroIsMaxDateTimeField.add(long,long)
public long org.joda.time.field.ZeroIsMaxDateTimeField.add(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "add"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.get(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "get"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],int)
public final long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String)
public int[] org.joda.time.field.BaseDateTimeField.set(org.joda.time.ReadablePartial,int,int[],java.lang.String,java.util.Locale)
public long org.joda.time.field.BaseDateTimeField.set(long,java.lang.String,java.util.Locale)
public long org.joda.time.field.ZeroIsMaxDateTimeField.set(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "set"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.REMAINDER
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.field.ZeroIsMaxDateTimeField.remainder(long)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "remainder"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.REMAINDER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getDurationField"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-RANGE-DURATION-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getRangeDurationField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getRangeDurationField"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-WRAPPED-FIELD
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getWrappedField"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-WRAPPED-FIELD
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "isLenient"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-AS-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(org.joda.time.ReadablePartial,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsText(long,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getAsText"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-AS-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-AS-SHORT-TEXT
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,int,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long,java.util.Locale)
public java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(int,java.util.Locale)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(long)
public final java.lang.String org.joda.time.field.BaseDateTimeField.getAsShortText(org.joda.time.ReadablePartial,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getAsShortText"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-AS-SHORT-TEXT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD-WRAP-PARTIAL
       (FOIL::THIS &REST FOIL::ARGS)
  "public int[] org.joda.time.field.BaseDateTimeField.addWrapPartial(org.joda.time.ReadablePartial,int,int[],int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "addWrapPartial"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD-WRAP-PARTIAL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getMaximumTextLength"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.BaseDateTimeField.getMaximumShortTextLength(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getMaximumShortTextLength"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String org.joda.time.field.BaseDateTimeField.toString()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "toString"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getName"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getType"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "isSupported"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SET-EXTENDED
       (FOIL::THIS &REST FOIL::ARGS)
  "public long org.joda.time.DateTimeField.setExtended(long,int)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "setExtended"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SET-EXTENDED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "wait"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "equals"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "hashCode"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "getClass"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "notify"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                          "notifyAll"
                          '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField| "class"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "durationField"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.DURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.ZeroIsMaxDateTimeField.getLeapDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "leapDurationField"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.LEAPDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean org.joda.time.field.DecoratedDateTimeField.isLenient()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "lenient"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.MAXIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.getMaximumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "maximumValue"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.MAXIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.MINIMUM-VALUE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int org.joda.time.field.ZeroIsMaxDateTimeField.getMinimumValue()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "minimumValue"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.MINIMUMVALUE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String org.joda.time.field.BaseDateTimeField.getName()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField| "name"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NAME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public org.joda.time.DurationField org.joda.time.field.DecoratedDateTimeField.getRangeDurationField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "rangeDurationField"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.RANGEDURATIONFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean org.joda.time.field.BaseDateTimeField.isSupported()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "supported"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeFieldType org.joda.time.field.BaseDateTimeField.getType()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField| "type"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.TYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.WRAPPED-FIELD-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final org.joda.time.DateTimeField org.joda.time.field.DecoratedDateTimeField.getWrappedField()
"
  (FOIL::CALL-PROP-GET '|org.joda.time.field|::|ZeroIsMaxDateTimeField|
                       "wrappedField"
                       '|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.WRAPPEDFIELD-GET
                       FOIL::THIS FOIL::ARGS))
(eval-when (:load-toplevel)(export '(|org.joda.time.field|::ZEROISMAXDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.SET
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.NEW
                                     |org.joda.time.field|::ZEROISMAXDATETIMEFIELD.
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.WAIT
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-INSTANCE
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.ADD
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::UNSUPPORTEDDURATIONFIELD.
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.SET
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-INSTANCE
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::UNSUPPORTEDDATETIMEFIELD.
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ADD
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.SET
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.GET-INSTANCE
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::STRICTDATETIMEFIELD.
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.WAIT
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.SET
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.NEW
                                     |org.joda.time.field|::SKIPUNDODATETIMEFIELD.
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ADD
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.SET
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.NEW
                                     |org.joda.time.field|::SKIPDATETIMEFIELD.
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.SCALAR-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.WAIT
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.ADD
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-SCALAR
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.NEW
                                     |org.joda.time.field|::SCALEDDURATIONFIELD.
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.DIVISOR-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.SET
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DIVISOR
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.NEW
                                     |org.joda.time.field|::REMAINDERDATETIMEFIELD.
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.WAIT
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.ADD
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.NEW
                                     |org.joda.time.field|::PRECISEDURATIONFIELD.
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.SET
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.NEW
                                     |org.joda.time.field|::PRECISEDURATIONDATETIMEFIELD.
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.RANGE-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ADD
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.SET
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-RANGE
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.NEW
                                     |org.joda.time.field|::PRECISEDATETIMEFIELD.
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.OFFSET-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-OFFSET
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.SET
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ADD
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.NEW
                                     |org.joda.time.field|::OFFSETDATETIMEFIELD.
                                     |org.joda.time.field|::MILLISDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::MILLISDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::MILLISDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::MILLISDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::MILLISDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::MILLISDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::MILLISDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::+MILLISDURATIONFIELD.+INSTANCE+*+
                                     |org.joda.time.field|::MILLISDURATIONFIELD.+INSTANCE+*
                                     |org.joda.time.field|::MILLISDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::MILLISDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::MILLISDURATIONFIELD.WAIT
                                     |org.joda.time.field|::MILLISDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::MILLISDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::MILLISDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::MILLISDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::MILLISDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::MILLISDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::MILLISDURATIONFIELD.ADD
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::MILLISDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::MILLISDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::MILLISDURATIONFIELD.
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ADD
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.SET
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.GET-INSTANCE
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::LENIENTDATETIMEFIELD.
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.WAIT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.ADD
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD$LINKEDDURATIONFIELD.
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.SET
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ADD
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.NEW
                                     |org.joda.time.field|::IMPRECISEDATETIMEFIELD.
                                     |org.joda.time.field|::FIELDUTILS.CLASS-PROP
                                     |org.joda.time.field|::FIELDUTILS.NOTIFY-ALL
                                     |org.joda.time.field|::FIELDUTILS.NOTIFY
                                     |org.joda.time.field|::FIELDUTILS.GET-CLASS
                                     |org.joda.time.field|::FIELDUTILS.HASH-CODE
                                     |org.joda.time.field|::FIELDUTILS.TO-STRING
                                     |org.joda.time.field|::FIELDUTILS.WAIT
                                     |org.joda.time.field|::FIELDUTILS.EQUALS
                                     |org.joda.time.field|::FIELDUTILS.SAFE-MULTIPLY-TO-INT
                                     |org.joda.time.field|::FIELDUTILS.GET-WRAPPED-VALUE
                                     |org.joda.time.field|::FIELDUTILS.VERIFY-VALUE-BOUNDS
                                     |org.joda.time.field|::FIELDUTILS.SAFE-DIVIDE
                                     |org.joda.time.field|::FIELDUTILS.SAFE-TO-INT
                                     |org.joda.time.field|::FIELDUTILS.SAFE-SUBTRACT
                                     |org.joda.time.field|::FIELDUTILS.SAFE-ADD
                                     |org.joda.time.field|::FIELDUTILS.SAFE-MULTIPLY
                                     |org.joda.time.field|::FIELDUTILS.SAFE-NEGATE
                                     |org.joda.time.field|::FIELDUTILS.
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.DIVISOR-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.SET
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIVISOR
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.NEW
                                     |org.joda.time.field|::DIVIDEDDATETIMEFIELD.
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.WAIT
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.ADD
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::DELEGATEDDURATIONFIELD.
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.SET
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.NEW
                                     |org.joda.time.field|::DELEGATEDDATETIMEFIELD.
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.WAIT
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.ADD
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.NEW
                                     |org.joda.time.field|::DECORATEDDURATIONFIELD.
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.WRAPPED-FIELD-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.SET
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-WRAPPED-FIELD
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::DECORATEDDATETIMEFIELD.
                                     |org.joda.time.field|::BASEDURATIONFIELD.UNIT-MILLIS-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.TYPE-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.PRECISE-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.NAME-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.MILLIS-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.CLASS-PROP
                                     |org.joda.time.field|::BASEDURATIONFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::BASEDURATIONFIELD.NOTIFY
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-CLASS
                                     |org.joda.time.field|::BASEDURATIONFIELD.HASH-CODE
                                     |org.joda.time.field|::BASEDURATIONFIELD.EQUALS
                                     |org.joda.time.field|::BASEDURATIONFIELD.WAIT
                                     |org.joda.time.field|::BASEDURATIONFIELD.ADD
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::BASEDURATIONFIELD.IS-PRECISE
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-UNIT-MILLIS
                                     |org.joda.time.field|::BASEDURATIONFIELD.SUBTRACT
                                     |org.joda.time.field|::BASEDURATIONFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-TYPE
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-VALUE
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-NAME
                                     |org.joda.time.field|::BASEDURATIONFIELD.COMPARE-TO
                                     |org.joda.time.field|::BASEDURATIONFIELD.TO-STRING
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-VALUE-AS-LONG
                                     |org.joda.time.field|::BASEDURATIONFIELD.GET-MILLIS
                                     |org.joda.time.field|::BASEDURATIONFIELD.
                                     |org.joda.time.field|::BASEDATETIMEFIELD.TYPE-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.SUPPORTED-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.NAME-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.LENIENT-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.DURATION-FIELD-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.CLASS-PROP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.NOTIFY-ALL
                                     |org.joda.time.field|::BASEDATETIMEFIELD.NOTIFY
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-CLASS
                                     |org.joda.time.field|::BASEDATETIMEFIELD.HASH-CODE
                                     |org.joda.time.field|::BASEDATETIMEFIELD.EQUALS
                                     |org.joda.time.field|::BASEDATETIMEFIELD.WAIT
                                     |org.joda.time.field|::BASEDATETIMEFIELD.IS-LENIENT
                                     |org.joda.time.field|::BASEDATETIMEFIELD.SET-EXTENDED
                                     |org.joda.time.field|::BASEDATETIMEFIELD.IS-SUPPORTED
                                     |org.joda.time.field|::BASEDATETIMEFIELD.REMAINDER
                                     |org.joda.time.field|::BASEDATETIMEFIELD.SET
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-TYPE
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-NAME
                                     |org.joda.time.field|::BASEDATETIMEFIELD.TO-STRING
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ADD
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::BASEDATETIMEFIELD.IS-LEAP
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ADD-WRAP-FIELD
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ADD-WRAP-PARTIAL
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-AS-TEXT
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-DIFFERENCE
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::BASEDATETIMEFIELD.GET-DURATION-FIELD
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-EVEN
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-CEILING
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-HALF-FLOOR
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-CEILING
                                     |org.joda.time.field|::BASEDATETIMEFIELD.ROUND-FLOOR
                                     |org.joda.time.field|::BASEDATETIMEFIELD.
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NAME-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MINIMUM-VALUE-OVERALL-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MAXIMUM-VALUE-OVERALL-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-DURATION-FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-AMOUNT-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.LEAP-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.FIELD-TYPE-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.DURATION-FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.CLASS-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.AS-TEXT-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.AS-STRING-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.AS-SHORT-TEXT-PROP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NOTIFY-ALL
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NOTIFY
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-CLASS
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.WAIT
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.REMAINDER
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-FIELD-TYPE
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-FIELD
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-NAME
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.COMPARE-TO
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.HASH-CODE
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.TO-STRING
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.EQUALS
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MINIMUM-VALUE-OVERALL
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-STRING
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.TO-INTERVAL
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-LEAP-DURATION-FIELD
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-LEAP-AMOUNT
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.IS-LEAP
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DIFFERENCE-AS-LONG
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-AS-TEXT
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DIFFERENCE
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-DURATION-FIELD
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.GET-MAXIMUM-VALUE-OVERALL
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.NEW
                                     |org.joda.time.field|::ABSTRACTREADABLEINSTANTFIELDPROPERTY.
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.RANGE-DURATION-FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NAME-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MINIMUM-VALUE-OVERALL-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MINIMUM-VALUE-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MAXIMUM-VALUE-OVERALL-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.MAXIMUM-VALUE-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.FIELD-TYPE-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.DURATION-FIELD-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.CLASS-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.AS-TEXT-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.AS-STRING-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.AS-SHORT-TEXT-PROP
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NOTIFY-ALL
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.NOTIFY
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-CLASS
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.WAIT
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-FIELD-TYPE
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-FIELD
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-NAME
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.COMPARE-TO
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.HASH-CODE
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.TO-STRING
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.EQUALS
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MINIMUM-VALUE-OVERALL
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-STRING
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-SHORT-TEXT-LENGTH
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-TEXT-LENGTH
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-RANGE-DURATION-FIELD
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-SHORT-TEXT
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-AS-TEXT
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-VALUE
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MINIMUM-VALUE
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-DURATION-FIELD
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.GET-MAXIMUM-VALUE-OVERALL
                                     |org.joda.time.field|::ABSTRACTPARTIALFIELDPROPERTY.) "org.joda.time.field"))
