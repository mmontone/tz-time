(in-package :tz-time.joda)

(setf foil:*fvm* (make-instance 'foil:foreign-vm
								:stream (usocket:socket-stream
										 (usocket:socket-connect "localhost" 13579))))

(defparameter +joda-jar+ "/home/marian/src/lisp/jar/joda-time-2.9/joda-time-2.9.jar")


(defun generate-java-wrappers ()
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time-base.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time/base"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time-chrono.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time/chrono"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time-convert.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time/convert"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time-field.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time/field"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time-format.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time/format"))
  (foil:dump-wrapper-defs-to-file
   (asdf:system-relative-pathname :tz-time-joda "joda/java/joda-time-tz.lisp")
   (foil:get-library-classnames +joda-jar+  "org/joda/time/tz")))

(defstruct (tz-time::timezone (:print-object print-timezone))
  ref)

(defun timezone-name (timezone)
  (foil:to-string (timezone-ref timezone)))

(defun print-timezone (timezone stream)
  (format stream "<TZ ~A>" (timezone-name timezone)))

(defparameter tz-time::+utc-tz+ (make-timezone :ref (|org.joda.time|::datetimezone.for-+id+ "UTC")))

(defparameter tz-time::*timezone* +utc-tz+)

(defstruct (tz-time::tz-timestamp (:print-object print-tz-timestamp))
  ref
  tz)

(defun print-tz-timestamp (tz-timestamp stream)
  (write-string "<TZ-TIMESTAMP " stream)
  (tz-time::format-timestring stream tz-timestamp)
  (write-string ">" stream))

(defun tz-time::now (&key (tz tz-time::*timezone*))
  (make-tz-timestamp :ref
					 (|org.joda.time|::datetime.now
									 (timezone-ref tz))
					 :tz tz))

(defun tz-time::format-timestring (stream timestamp &optional (format))
  (format stream "~A @ ~A"
		  (foil:to-string (tz-timestamp-ref timestamp))
		  (timezone-name (tz-timestamp-tz timestamp))))

(defun tz-time::decode-tz-timestamp (timestamp)
  "Returns the decoded time as multiple values: nsec, ss, mm, hh, day, month, year, day-of-week, timezone"
  (check-type timestamp tz-time::tz-timestamp)
  (with-slots (ref tz) timestamp
	(values (|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.millis-of-second ref))
			(|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.second-of-minute ref))
			(|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.minute-of-hour ref))
			(|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.hour-of-day ref))
			(|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.month-of-year ref))
			(|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.year ref))
			(|org.joda.time|:datetime$property.get
							(|org.joda.time|:datetime.day-of-week ref))
			tz)))
