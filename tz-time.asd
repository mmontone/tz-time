(asdf:defsystem #:tz-time
  :description "Common Lisp library date and time library with support for time zones"
  :long-description "Common Lisp library date and time library with support for time zones"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "package")
               (:file "tz-time")))

(asdf:defsystem #:tz-time-joda
  :description "Java JodaTime implementation of tz-time"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :depends-on (:tz-time :foil)
  :components ((:module :joda
						:components
						((:file "package")
						 (:file "joda")
						 (:module :java
								  :components ((:file "joda-time")
											   (:file "joda-time-base")
											   (:file "joda-time-chrono")
											   (:file "joda-time-convert")
											   (:file "joda-time-field")
											   (:file "joda-time-format")
											   (:file "joda-time-tz")))))))

(asdf:defsystem #:tz-time-jsr310
  :description "Java JodaTime implementation of tz-time"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :depends-on (:tz-time :foil)
  :components ((:module "JSR-310"
						:components
						((:file "package")
						 (:file "jsr-310")
						 (:file "java-util")
						 (:file "java-time")
						 (:file "api")))))

(asdf:defsystem #:tz-time-jni
    :description "Java8 time datatype access via JNI"
    :license "MIT"
    :serial t
    :depends-on (:tz-time :cl+j)
    :components ((:module "JNI"
                          :components
                          ((:file "package")
                           (:file "api")))))       
